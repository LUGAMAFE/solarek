-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 30-08-2019 a las 16:47:16
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `solarek`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `id_banners` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_img_banner_home` int(255) UNSIGNED NOT NULL,
  `id_img_banner_home_movil` int(255) UNSIGNED NOT NULL,
  `id_img_banner_home_ingles` int(255) UNSIGNED NOT NULL,
  `id_img_banner_home_movil_ingles` int(255) UNSIGNED NOT NULL,
  `url_youtube_id_home_ingles` varchar(300) NOT NULL,
  `banner_home_activo` int(1) UNSIGNED NOT NULL,
  `url_youtube_id_home` varchar(300) NOT NULL,
  `banner_type_home` int(5) NOT NULL,
  PRIMARY KEY (`id_banners`),
  KEY `id_img_banner_home` (`id_img_banner_home`),
  KEY `id_img_banner_home_movil` (`id_img_banner_home_movil`),
  KEY `id_img_banner_home_ingles` (`id_img_banner_home_ingles`),
  KEY `id_img_banner_home_movil_ingles` (`id_img_banner_home_movil_ingles`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `banners`
--

INSERT INTO `banners` (`id_banners`, `id_img_banner_home`, `id_img_banner_home_movil`, `id_img_banner_home_ingles`, `id_img_banner_home_movil_ingles`, `url_youtube_id_home_ingles`, `banner_home_activo`, `url_youtube_id_home`, `banner_type_home`) VALUES
(1, 35, 52, 45, 53, 'https://www.youtube.com/watch?v=Bey4XXJAqS8', 0, 'https://www.youtube.com/watch?v=Bey4XXJAqS8', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `casos_exito`
--

DROP TABLE IF EXISTS `casos_exito`;
CREATE TABLE IF NOT EXISTS `casos_exito` (
  `id_caso` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titulo_caso` varchar(200) NOT NULL,
  `lugar_caso` varchar(200) NOT NULL,
  `capacidad_caso` int(40) NOT NULL,
  `produccion_caso` int(40) NOT NULL,
  `informacion_caso` varchar(600) NOT NULL,
  `titulo_ingles_caso` varchar(200) NOT NULL,
  `lugar_ingles_caso` varchar(200) NOT NULL,
  `informacion_ingles_caso` varchar(600) NOT NULL,
  `img_caso` int(255) UNSIGNED NOT NULL,
  `fecha_creacion_caso` date NOT NULL,
  `fecha_modificacion_caso` date NOT NULL,
  PRIMARY KEY (`id_caso`),
  UNIQUE KEY `titulo_caso_unique` (`titulo_caso`),
  KEY `img_caso` (`img_caso`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `casos_exito`
--

INSERT INTO `casos_exito` (`id_caso`, `titulo_caso`, `lugar_caso`, `capacidad_caso`, `produccion_caso`, `informacion_caso`, `titulo_ingles_caso`, `lugar_ingles_caso`, `informacion_ingles_caso`, `img_caso`, `fecha_creacion_caso`, `fecha_modificacion_caso`) VALUES
(1, 'fewff', 'fewfewf', 3, 4, 'fewfewfew', 'cvcxvcxv', 'vcxvcx', 'vcxvcxvc', 43, '2019-08-27', '2019-08-27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correos`
--

DROP TABLE IF EXISTS `correos`;
CREATE TABLE IF NOT EXISTS `correos` (
  `id_correo` int(255) NOT NULL AUTO_INCREMENT,
  `direccion_correo` varchar(300) NOT NULL,
  `fecha_creacion_correo` date NOT NULL,
  PRIMARY KEY (`id_correo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `correos`
--

INSERT INTO `correos` (`id_correo`, `direccion_correo`, `fecha_creacion_correo`) VALUES
(2, 'luisjavier004@hotmail.com', '2019-08-07'),
(3, 'efewfewf@fee.com', '2019-08-07'),
(4, 'dwdw@efef.com', '2019-08-07'),
(5, 'luisjavier00@hotmail.com', '2019-08-07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descargables`
--

DROP TABLE IF EXISTS `descargables`;
CREATE TABLE IF NOT EXISTS `descargables` (
  `id_descargable` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titulo_descargable` varchar(300) NOT NULL,
  `descripcion_descargable` varchar(500) NOT NULL,
  `id_archivo_descargable` int(255) UNSIGNED NOT NULL,
  `fecha_creacion_descargable` date NOT NULL,
  `fecha_modificacion_descargable` date NOT NULL,
  PRIMARY KEY (`id_descargable`),
  UNIQUE KEY `titulo_descargable` (`titulo_descargable`),
  KEY `id_archivo_descargable` (`id_archivo_descargable`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `descargables`
--

INSERT INTO `descargables` (`id_descargable`, `titulo_descargable`, `descripcion_descargable`, `id_archivo_descargable`, `fecha_creacion_descargable`, `fecha_modificacion_descargable`) VALUES
(1, 'DWQD', 'fewfewfewfewfewfew', 19, '2019-08-06', '2019-08-06'),
(2, 'fef', 'fefew', 20, '2019-08-06', '2019-08-06'),
(3, 'fefe', 'fefef', 21, '2019-08-06', '2019-08-06'),
(4, 'fefef', 'efef', 22, '2019-08-06', '2019-08-06'),
(5, 'feff', 'feWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW', 23, '2019-08-06', '2019-08-06'),
(6, 'grgtewgfe', 'feEFWF', 24, '2019-08-06', '2019-08-06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas_blog`
--

DROP TABLE IF EXISTS `entradas_blog`;
CREATE TABLE IF NOT EXISTS `entradas_blog` (
  `id_entrada` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre_entrada` varchar(100) NOT NULL,
  `autor_entrada` varchar(100) NOT NULL,
  `titulo_entrada` varchar(100) NOT NULL,
  `subtitulo_entrada` varchar(100) NOT NULL,
  `resumen_entrada` varchar(200) NOT NULL,
  `informacion_entrada` text NOT NULL,
  `titulo_ingles_entrada` varchar(100) NOT NULL,
  `subtitulo_ingles_entrada` varchar(100) NOT NULL,
  `resumen_ingles_entrada` varchar(200) NOT NULL,
  `informacion_ingles_entrada` text NOT NULL,
  `url_youtube_id_entrada` varchar(300) NOT NULL,
  `check_video_entrada` int(1) UNSIGNED NOT NULL,
  `imagen_entrada_big` int(255) UNSIGNED NOT NULL,
  `imagen_entrada_small` int(255) UNSIGNED NOT NULL,
  `fecha_creacion_entrada` datetime NOT NULL,
  `fecha_modificacion_entrada` datetime NOT NULL,
  PRIMARY KEY (`id_entrada`),
  UNIQUE KEY `nombre_entrada_unique` (`nombre_entrada`),
  UNIQUE KEY `titulo_entrada_unique` (`titulo_entrada`),
  KEY `imagen_entrada` (`imagen_entrada_big`),
  KEY `imagen_entrada_small` (`imagen_entrada_small`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `entradas_blog`
--

INSERT INTO `entradas_blog` (`id_entrada`, `nombre_entrada`, `autor_entrada`, `titulo_entrada`, `subtitulo_entrada`, `resumen_entrada`, `informacion_entrada`, `titulo_ingles_entrada`, `subtitulo_ingles_entrada`, `resumen_ingles_entrada`, `informacion_ingles_entrada`, `url_youtube_id_entrada`, `check_video_entrada`, `imagen_entrada_big`, `imagen_entrada_small`, `fecha_creacion_entrada`, `fecha_modificacion_entrada`) VALUES
(1, '4 Formas De Ahorrar Luz Y Dinero', 'José Luis', 'Descubre 4 Formas De Empezar A Pagar Menos Luz Y Ahorrar Dinero.', '¡La 4 te dejará impactado!', 'Bimestre tras bimestre no sabemos con que \"sorpresa\" nos llegará el recibo y cuanto tendremos que desembolsar...', '<p>Una de nuestras preocupaciones más importantes es el pago del recibo de luz. Bimestre tras bimestre no sabemos con que \\\"sorpresa\\\" nos llegará el recibo y cuanto tendremos que desembolsar para cubrir ese gasto.</p><p>Pero existen formas realistas de comenzar a pagar menos, algunas de ellas conllevan modificar nuestros hábitos en casa para reducir el consumo de energía:</p><h3>1. Reemplaza las bombillas por focos ahorradores.</h3><p>Éstos consumen 75% menos energía que las bombillas incandescentes tradicionales, y su vida útil es 10 veces mayor. Lámparas de espiral o de tubo se recomiendan para las recámaras, sala y comedor; las de globo, en cocinas y estudios.</p><h3>2.  Dale mantenimiento a tus instalaciones.</h3><p>Lo ideal es que cada cinco años un electricista calificado revise el sistema y aplique los servicios preventivos o correctivos necesarios con el fin de evitar incendios y accidentes, así como hacer más eficiente y reducir hasta 15% el consumo de luz por fugas eléctricas.</p><h3>3. Pon freno a los vampiros</h3><p>Desconecta los aparatos eléctricos, lámparas y herramientas que no utilices; aunque estén apagados, éstos son \\\"vampiros\\\" de energía que disparan hasta en 13% tu recibo de la luz.</p><h3>4. Apuesta por el uso de la tecnología de paneles solares</h3><p>En la actualidad los paneles solares se han abaratado drástica mente y son una de las mejores formas de ahorrar de manera contundente en tu recibo de luz.</p><p></p><p>Pero ¿Cómo funcionan? Los paneles solares foto-voltaicos aprovechan la luz del sol para convertirla en energía solar, que a su vez se transforma en electricidad. Generan energía por más de 25 años y en promedio, recuperas la inversión hecha en 5 años.</p><p>Por supuesto, al utilizar energía solar, reduces de manera drástica tu tarifa en el recibo de luz, hablamos de hasta un 98%. Por ejemplo, si pagas 4 mil 62 pesos llegarás  a pagar  la mínima cantidad de 121 pesos.</p><p>Con los paneles solares de alta tecnología que ofrece Solárek, empresa con años de experiencia en toda la península de Yucatán, reducirás rápidamente el pago de energía desde tu primer recibo. Lo anterior está garantizado por la compañía.</p><h3>5. Sácale brillo a tu casa</h3><p>Mantén abiertas las cortinas y persianas para aprovechar la luz solar. Pinta las paredes del hogar con colores claros, los cuales optimizan la iluminación natural y artificial. Lava, plancha y lleva a cabo el aseo del hogar durante el día.<br></p>', '', '', '', '', '', 0, 2, 1, '2019-07-25 18:09:06', '2019-07-25 18:09:06'),
(2, 'Para Tu Negocio, Un Proyecto De Ahorro Con Paneles Solares Es 100 % Deducible De Impuestos.', 'José Luis', 'Para Tu Negocio, Un Proyecto De Ahorro Con Paneles Solares Es 100 % Deducible De Impuestos.', 'Además ahorrarás dinero en cada recibo de luz', 'Bimestre tras bimestre no sabemos con que \"sorpresa\" nos llegará el recibo y cuanto tendremos que desembolsar...', '<p><span style=\\\"color: rgb(10, 10, 10); font-family: Raleway, Arial, Helvetica, sans-serif; font-size: 18.4px; background-color: rgb(254, 254, 254);\\\">En Yucatán como en todo el país, la Ley de Impuesto Sobre la Renta te permite una deducción de impuestos del 100% de toda la inversión que hagas en un sistema de paneles solares para tu negocio; se trata de un estimulo para que las empresas inviertan en energías renovables porque de esta forma están contribuyendo al cuidado del medio ambiente.</span></p><p><span style=\\\"color: rgb(10, 10, 10); font-family: Raleway, Arial, Helvetica, sans-serif; font-size: 18.4px; background-color: rgb(254, 254, 254);\\\">Puedes contactarme para más información, con gusto te compartiré los detalles solo escríbeme a&nbsp;</span><a href=\\\"mailto:joseluis@solarek.com.mx\\\" style=\\\"box-sizing: inherit; background-color: rgb(254, 254, 254); line-height: inherit; color: rgb(0, 17, 53); cursor: pointer; outline: none; font-family: Raleway, Arial, Helvetica, sans-serif; font-size: 18.4px;\\\">joseluis@solarek.com.mx</a></p><p><span style=\\\"color: rgb(10, 10, 10); font-family: Raleway, Arial, Helvetica, sans-serif; font-size: 18.4px; background-color: rgb(254, 254, 254);\\\">Pero, ¿Cómo podrás deducir impuestos? Te explicamos, al hacer las declaraciones anuales de tu negocio deberás incluir el gasto de la inversión de los paneles solares y de esta forma se podrás deducir el 100% de la inversión. Posteriormente, por los siguientes 5 años deberás incluir una declaración complementaria en tu declaración anual comprobando que el sistema fotovoltaico sigue operando.</span></p><p><span style=\\\"color: rgb(10, 10, 10); font-family: Raleway, Arial, Helvetica, sans-serif; font-size: 18.4px; background-color: rgb(254, 254, 254);\\\">En conclusión, esta es una opción viable y recomendable para todos les negocios que estén considerando adquirir paneles solares para sus inmuebles. De esta manera los múltiples beneficios de los paneles serán aprovechados a su máximo potencial. Incluyendo los beneficios de ahorro, medio ambiente, e incentivas fiscales.</span></p><p><span style=\\\"color: rgb(10, 10, 10); font-family: Raleway, Arial, Helvetica, sans-serif; font-size: 18.4px; background-color: rgb(254, 254, 254);\\\">Además, te garantizamos que ahorrarás hasta un 97 % en el pago de tu recibo de luz.</span></p><p><span style=\\\"color: rgb(10, 10, 10); font-family: Raleway, Arial, Helvetica, sans-serif; font-size: 18.4px; background-color: rgb(254, 254, 254);\\\">José Luis Azcorra Quijano. Director Comercial</span><br></p>', '', '', '', '', '', 0, 4, 3, '2019-07-25 18:11:20', '2019-07-25 18:11:20'),
(5, 'Dwqdqwd', 'Luis', 'DWQD', 'WDWQD', 'DWQDWQD', '<p>                                          DWQDWQDWQDWQDWQDWQDWQDFEWF</p><p>FEWFEWFEW</p><p>FEWFEW</p><p>FEWFEWF</p><p>EWFEWFEWFEW</p><p>                                        </p>', 'CXZCZX', 'CXZCXZC', 'CXZCXZ', 'CXZCXZCXZCXZCXZCX', 'https://www.youtube.com/watch?v=ZQNRkAiqJc8', 1, 41, 40, '2019-08-27 08:48:24', '2019-08-27 08:49:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

DROP TABLE IF EXISTS `imagenes`;
CREATE TABLE IF NOT EXISTS `imagenes` (
  `id_img` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `folder_img` varchar(300) NOT NULL,
  `uuid_img` varchar(100) NOT NULL,
  `name_img` varchar(150) NOT NULL,
  `ext_img` varchar(10) NOT NULL,
  `dir_img` varchar(200) NOT NULL,
  `full_route_img` varchar(400) NOT NULL,
  `fecha_creacion_img` datetime NOT NULL,
  `fecha_modificacion_img` datetime NOT NULL,
  PRIMARY KEY (`id_img`),
  UNIQUE KEY `uuid_img_unique` (`uuid_img`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `imagenes`
--

INSERT INTO `imagenes` (`id_img`, `folder_img`, `uuid_img`, `name_img`, `ext_img`, `dir_img`, `full_route_img`, `fecha_creacion_img`, `fecha_modificacion_img`) VALUES
(1, 'assets/img/entradas/', 'c9038edb-f143-4813-ac32-9af947119751', 'Banner1 small', 'png', 'assets/img/entradas//c9038edb-f143-4813-ac32-9af947119751/', 'assets/img/entradas//c9038edb-f143-4813-ac32-9af947119751/Banner1 small.png', '2019-07-25 18:09:06', '2019-07-25 18:09:06'),
(2, 'assets/img/entradas/', '652159a7-fcb6-424d-8b7c-b06d8b0ef63a', 'Banner1', 'png', 'assets/img/entradas//652159a7-fcb6-424d-8b7c-b06d8b0ef63a/', 'assets/img/entradas//652159a7-fcb6-424d-8b7c-b06d8b0ef63a/Banner1.png', '2019-07-25 18:09:06', '2019-07-25 18:09:06'),
(3, 'assets/img/entradas/', '2cbf46b5-4e18-4745-9d4b-da2b1329ef84', 'Banner2 small', 'png', 'assets/img/entradas//2cbf46b5-4e18-4745-9d4b-da2b1329ef84/', 'assets/img/entradas//2cbf46b5-4e18-4745-9d4b-da2b1329ef84/Banner2 small.png', '2019-07-25 18:11:20', '2019-07-25 18:11:20'),
(4, 'assets/img/entradas/', 'd3762fc5-09b9-47c6-930b-5594b8886d85', 'Banner2', 'png', 'assets/img/entradas//d3762fc5-09b9-47c6-930b-5594b8886d85/', 'assets/img/entradas//d3762fc5-09b9-47c6-930b-5594b8886d85/Banner2.png', '2019-07-25 18:11:20', '2019-07-25 18:11:20'),
(14, 'assets/img/casos/', 'ddaa774e-c5b7-4953-a0ea-6fe40dfe2a87', 'NTE', 'png', 'assets/img/casos//ddaa774e-c5b7-4953-a0ea-6fe40dfe2a87/', 'assets/img/casos//ddaa774e-c5b7-4953-a0ea-6fe40dfe2a87/NTE.png', '2019-08-05 17:58:57', '2019-08-05 17:58:57'),
(16, 'assets/img/casos/', '81c1416e-11c5-46a3-9bce-b8a9d783b6e4', 'DRAGONES', 'png', 'assets/img/casos//81c1416e-11c5-46a3-9bce-b8a9d783b6e4/', 'assets/img/casos//81c1416e-11c5-46a3-9bce-b8a9d783b6e4/DRAGONES.png', '2019-08-05 18:09:30', '2019-08-05 18:09:30'),
(19, 'assets/archivos/descargables/', '5bdfeedf-fb77-4ffa-bd3f-6607dc63a2e9', 'Home', 'php', 'assets/archivos/descargables//5bdfeedf-fb77-4ffa-bd3f-6607dc63a2e9/', 'assets/archivos/descargables//5bdfeedf-fb77-4ffa-bd3f-6607dc63a2e9/Home.php', '2019-08-06 17:23:54', '2019-08-06 17:23:54'),
(20, 'assets/archivos/descargables/', '7df6d252-ace8-4767-ae6e-8f45f7107179', 'imagenes', 'sql', 'assets/archivos/descargables//7df6d252-ace8-4767-ae6e-8f45f7107179/', 'assets/archivos/descargables//7df6d252-ace8-4767-ae6e-8f45f7107179/imagenes.sql', '2019-08-06 17:35:47', '2019-08-06 17:35:47'),
(21, 'assets/archivos/descargables/', '5ce1f207-cb0b-40b1-b196-7699eb30af01', 'imagenes', 'sql', 'assets/archivos/descargables//5ce1f207-cb0b-40b1-b196-7699eb30af01/', 'assets/archivos/descargables//5ce1f207-cb0b-40b1-b196-7699eb30af01/imagenes.sql', '2019-08-06 17:35:55', '2019-08-06 17:35:55'),
(22, 'assets/archivos/descargables/', '8ae75b97-ccdd-4a00-96cf-e28c130c1d3b', 'imagenes', 'sql', 'assets/archivos/descargables//8ae75b97-ccdd-4a00-96cf-e28c130c1d3b/', 'assets/archivos/descargables//8ae75b97-ccdd-4a00-96cf-e28c130c1d3b/imagenes.sql', '2019-08-06 17:36:03', '2019-08-06 17:36:03'),
(23, 'assets/archivos/descargables/', '752cfa19-83dd-4239-a97e-a86b840e8f29', 'routes (1)', 'php', 'assets/archivos/descargables//752cfa19-83dd-4239-a97e-a86b840e8f29/', 'assets/archivos/descargables//752cfa19-83dd-4239-a97e-a86b840e8f29/routes (1).php', '2019-08-06 17:36:14', '2019-08-06 17:36:14'),
(24, 'assets/archivos/descargables/', 'b98150a3-4300-4f45-809a-b2b987b09e7d', 'joseluissolarek', 'jpeg', 'assets/archivos/descargables//b98150a3-4300-4f45-809a-b2b987b09e7d/', 'assets/archivos/descargables//b98150a3-4300-4f45-809a-b2b987b09e7d/joseluissolarek.jpeg', '2019-08-06 17:36:36', '2019-08-06 17:36:36'),
(35, 'assets/archivos/banners/', '111fc900-90d7-4320-845d-b9fac23ffece', 'A8F6ED70', 'png', 'assets/archivos/banners//111fc900-90d7-4320-845d-b9fac23ffece/', 'assets/archivos/banners//111fc900-90d7-4320-845d-b9fac23ffece/A8F6ED70.png', '2019-08-20 09:34:29', '2019-08-20 09:34:29'),
(40, 'assets/img/entradas/', '15bd6a6a-9d80-4c23-850b-e3809d709a5e', 'Captura', 'PNG', 'assets/img/entradas//15bd6a6a-9d80-4c23-850b-e3809d709a5e/', 'assets/img/entradas//15bd6a6a-9d80-4c23-850b-e3809d709a5e/Captura.PNG', '2019-08-27 08:48:24', '2019-08-27 08:48:24'),
(41, 'assets/img/entradas/', 'b0ed73d6-2e5f-4da6-87ea-82a8f9a413ee', 'Captura de pantalla (1)', 'png', 'assets/img/entradas//b0ed73d6-2e5f-4da6-87ea-82a8f9a413ee/', 'assets/img/entradas//b0ed73d6-2e5f-4da6-87ea-82a8f9a413ee/Captura de pantalla (1).png', '2019-08-27 08:48:24', '2019-08-27 08:48:24'),
(42, 'assets/img/casos/', 'f614ada8-026b-4809-b286-1631c16884cc', 'banner-experiencia', 'jpg', 'assets/img/casos//f614ada8-026b-4809-b286-1631c16884cc/', 'assets/img/casos//f614ada8-026b-4809-b286-1631c16884cc/banner-experiencia.jpg', '2019-08-27 09:20:46', '2019-08-27 09:20:46'),
(43, 'assets/img/casos/', '70376295-1f01-4805-9db1-c34eef270216', 'fondo-degradado-home', 'jpg', 'assets/img/casos//70376295-1f01-4805-9db1-c34eef270216/', 'assets/img/casos//70376295-1f01-4805-9db1-c34eef270216/fondo-degradado-home.jpg', '2019-08-27 09:22:46', '2019-08-27 09:22:46'),
(45, 'assets/archivos/banners/', '78869c30-72aa-4e5d-8367-3225d9348c8c', 'prueba', 'jpg', 'assets/archivos/banners//78869c30-72aa-4e5d-8367-3225d9348c8c/', 'assets/archivos/banners//78869c30-72aa-4e5d-8367-3225d9348c8c/prueba.jpg', '2019-08-27 13:35:51', '2019-08-27 13:35:51'),
(50, 'assets/img/testimonios/', '83966830-6817-439f-961d-993a0bdf8db0', 'perfil-profesional', 'png', 'assets/img/testimonios//83966830-6817-439f-961d-993a0bdf8db0/', 'assets/img/testimonios//83966830-6817-439f-961d-993a0bdf8db0/perfil-profesional.png', '2019-08-28 08:49:25', '2019-08-28 08:49:25'),
(51, 'assets/img/testimonios/', '8bd0860d-178b-4c94-afb2-66460349a48f', 'perfil-profesional', 'png', 'assets/img/testimonios//8bd0860d-178b-4c94-afb2-66460349a48f/', 'assets/img/testimonios//8bd0860d-178b-4c94-afb2-66460349a48f/perfil-profesional.png', '2019-08-28 08:50:57', '2019-08-28 08:50:57'),
(52, 'assets/archivos/banners/', '2026b3b6-826e-4e95-8329-ad5126875894', '10', 'jpg', 'assets/archivos/banners//2026b3b6-826e-4e95-8329-ad5126875894/', 'assets/archivos/banners//2026b3b6-826e-4e95-8329-ad5126875894/10.jpg', '2019-08-29 11:36:28', '2019-08-29 11:36:28'),
(53, 'assets/archivos/banners/', 'd4f2b112-b9a5-4289-8c7b-a67f4bfd48f6', '4', 'jpg', 'assets/archivos/banners//d4f2b112-b9a5-4289-8c7b-a67f4bfd48f6/', 'assets/archivos/banners//d4f2b112-b9a5-4289-8c7b-a67f4bfd48f6/4.jpg', '2019-08-29 11:36:28', '2019-08-29 11:36:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `testimonios`
--

DROP TABLE IF EXISTS `testimonios`;
CREATE TABLE IF NOT EXISTS `testimonios` (
  `id_testimonio` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cliente_testimonio` varchar(50) NOT NULL,
  `img_testimonio` int(255) UNSIGNED NOT NULL,
  `descripcion_testimonio` varchar(300) NOT NULL,
  `descripcion_ingles_testimonio` varchar(300) NOT NULL,
  `fecha_creacion_testimonio` date NOT NULL,
  `fecha_modificacion_testimonio` date NOT NULL,
  PRIMARY KEY (`id_testimonio`),
  KEY `img_testimonio` (`img_testimonio`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `testimonios`
--

INSERT INTO `testimonios` (`id_testimonio`, `cliente_testimonio`, `img_testimonio`, `descripcion_testimonio`, `descripcion_ingles_testimonio`, `fecha_creacion_testimonio`, `fecha_modificacion_testimonio`) VALUES
(1, 'luis', 51, 'dwdwqdwqdwdwdwqd\r\nwqdwqdwqd', 'bbvbxcbvxcbvcxb\r\nvbvxbvxbvxbvxbvxcvxb', '2019-08-28', '2019-08-28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `contrasena_usuario` varchar(300) NOT NULL,
  `nombre_usuario` varchar(300) NOT NULL,
  `correo_usuario` varchar(300) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `unique_nombre_usuario` (`nombre_usuario`),
  UNIQUE KEY `unique_correo_usuario` (`correo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `contrasena_usuario`, `nombre_usuario`, `correo_usuario`) VALUES
(1, '123456', 'lugamafe', 'luisjavier004@hotmail.com'),
(2, 'SolAdmin19', 'Admin', '');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `banners`
--
ALTER TABLE `banners`
  ADD CONSTRAINT `banners_ibfk_1` FOREIGN KEY (`id_img_banner_home`) REFERENCES `imagenes` (`id_img`);

--
-- Filtros para la tabla `casos_exito`
--
ALTER TABLE `casos_exito`
  ADD CONSTRAINT `casos_exito_ibfk_1` FOREIGN KEY (`img_caso`) REFERENCES `imagenes` (`id_img`);

--
-- Filtros para la tabla `descargables`
--
ALTER TABLE `descargables`
  ADD CONSTRAINT `descargables_ibfk_1` FOREIGN KEY (`id_archivo_descargable`) REFERENCES `imagenes` (`id_img`);

--
-- Filtros para la tabla `entradas_blog`
--
ALTER TABLE `entradas_blog`
  ADD CONSTRAINT `entradas_blog_ibfk_1` FOREIGN KEY (`imagen_entrada_big`) REFERENCES `imagenes` (`id_img`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
