<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
  
    public function __construct()
    {
        $dotenv = new Dotenv\Dotenv(BASEPATH .'/../');
        $dotenv->load();
        parent::__construct();

        $idiom = 'spanish';
        if($this->session->userdata('language')){
            $idiom = $this->session->userdata('language');
        }else{
            $this->session->set_userdata($idiom);
        }
        if($idiom != 'spanish'){
            setlocale(LC_ALL,null);
        }
        $this->lang->load('site', $idiom);

        /*$fb = new Facebook\Facebook([
            'app_id' => '251852438960949', // Replace {app-id} with your app id
            'app_secret' => '93a26bc9dc28d4ab723117344a2719f9',
            'default_graph_version' => 'v2.12',
        ]);
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email', 'public_profile', 'publish_actions']; // Optional permissions
        $this->loginUrl = $helper->getLoginUrl(site_url('clientes/callback'), $permissions);
        /* datos repetidos */
        /*$this->load->model('categorias/categoria');
        $this->load->model('comentarios/comentario');
        $this->listacategoria = $this->categoria->order_by('orden_categoria', 'ASC')->get();*/
        //$this->listacomentarios = $this->comentario->get();
    }
    
    public function setFlashData($nombre, $datos){
        $this->session->set_flashdata($nombre, $datos);
    }

    private function alertSession($type, $message, $description = null, $title = null){
        $datos = [
            "title" => $title,
            "type" => $type,
            "message" =>  $message,
            "description" => $description,
        ];  
        $this->setFlashData("admin-message", $datos);
    }    

    public function alertSuccess($message, $description = null, $title = "Exito!"){
        $this->alertSession("success", $message, $description, $title);
    }

    public function alertError($message, $description = null, $title = "Error!" ){
        $this->alertSession("error", $message, $description, $title);
    }

    public function alertInfo($message,  $description = null, $title = "Info" ){
        $this->alertSession("info", $message, $description, $title);
    }

    public function alertWarning($message,  $description = null, $title = "Alerta!" ){
        $this->alertSession("warning", $message, $description, $title);
    }   

    public function alertQuestion($message,  $description = null, $title = "¿?" ){
        $this->alertSession("question", $message, $description, $title);
    }
}

class CLI_Controller extends MY_Controller {
  
    public function __construct()
    {
        parent::__construct();
    }
}

class MY_Admin extends MY_Controller {

    private   $publicas   = array('administracion/login', 'administracion/logout', 'administracion/recuperar', 'administracion/verificar');
    protected $permitidos = array(0);

    public function __construct(){
        parent::__construct();
        $this->template->set_template('admin');
        /* Verificamos si está permitido el acceso */
        $this->verificar();
    }

    private function verificar(){
        $actual  = $this->uri->segment(1, '') . '/' . $this->uri->segment(2, '');
        $publica = in_array($actual, $this->publicas);

        if( !$publica AND !$this->session->userdata('logged_in') ) {
            redirect('administracion/login');
        } 
    }

    protected function loadDataTables(){
		// Datatables
		$this->template->asset_css('datatables/datatables.min.css');
		$this->template->asset_js('datatables/datatables.min.js');
		$this->template->asset_js('datatables-config.js');
	}

	protected function loadTemplatesComunes($datos){
		$this->template->write('bodyclasses', 'sidebar-mini layout-fixed');
		$this->template->asset_js('../dist/js/personalizacion.js');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
	}
}
