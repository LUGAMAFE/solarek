<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/**
	 * CodeIgniter MY_Model Class
	 * 
	 * Base model with CRUD functions for CodeIgniter Framework.
	 * Bassed on Jamie Rumbelow model base class. 
	 * 
	 * @package 	CodeIgniter
	 * @subpackage 	Libraries
	 * @category 	Library
	 * @author 		Henry Silva Bautista
	 * @author 		Juan Pablo Zamora Veraza <jupazave@gmail.com>
	 * @version 	0.6
	 */

	class MY_Model extends CI_Model {

		private $iv = '25452267885WOPTKTMF86652855SA5NJ5NVBN5X6D9TR5U6FG56asd6df5f9dg5d6fg56df5gdf9h4'; //Serial 
		private $key1 = 'j9u53zXLOP25PT0Elht09665naXCX45B'; //Llave para encriptar
			
		protected $_id;
		protected $_table;
		protected $total_results;
		protected $field_crypt = array();
		protected $file_fields = array();
		protected $field_names = array();
		protected $pre_insert = array();
		protected $pos_insert = array();
		protected $pre_update = array();
		protected $pos_update = array();
		protected $pre_delete = array();
		protected $pos_delete = array();
		protected $pre_get	  = array();
		protected $pos_get	  = array();
		
		protected $callback_params = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('inflector');
		$this->set_table();
	}

	public function getAttributes()
	{
		return $this->field_names;
	}

	public function getTableName()
	{
		return $this->_table;
	}

	
	/**
	 * Encriptar - Solo se van a poder consultar por medio de esta clase
	 * 
	 * @param  string 	$text Texto a encriptar
	 * @param  numeric 	$salt Saltos para encriptar
	 */
    protected function encrypt($text, $salt = NULL)
    {
        $salt = (is_null($salt)) ? $this->key1 : $salt ;

        if(function_exists('openssl_encrypt')) 
        {
            return base64_encode(openssl_encrypt($text, 'aes-256-cbc', $salt, true, substr($this->iv, 32,16)));
        }
        else
        {
            return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_CBC, pack('H*', $this->iv))));
        }
    }

    /**
	 * Desencriptar - Solo se van a poder consultar por medio de esta clase
	 *
	 * @param  string 	$text Texto a desenciptar 
	 * @param  numeric 	$salt Saltos para desencriptar 
	 */
    protected function decrypt($text, $salt = NULL)
    {
        $salt = (is_null($salt)) ? $this->key1 : $salt ;

        if(function_exists('openssl_decrypt')) 
        {
            return openssl_decrypt(base64_decode($text), 'aes-256-cbc', $salt, true, substr($this->iv, 32,16));
        }
        else
        {
            return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_CBC, pack('H*',$this->iv)));
        }    
    }

    /**
	 * Encriptar - Solo se van a poder consultar por medio de esta clase - Solo que el campo de la base de datos debe ser 
	 * blob por que los datos se convierten a vinarios 
	 *
	 * @param  string 	$text Texto a enciptar
	 * 
	 */
    protected function aes_encrypt($texto, $campo)
    {
    	$this->db->set($campo, "AES_ENCRYPT('".$texto."','".$this->config->item('aes_encryption_key')."')", FALSE);
    }

	/* --------------------------------------------------------------
	 * INTERNAL METHODS
	 * ------------------------------------------------------------ */
	
	/**
	 * Sets the table to use with the model. If not provided with $this->_table
	 * var, the function try to guess the name using the plural name of the
	 * model.
	 */
	
	protected function set_table()
	{
		if (empty($this->_table) || $this->_table == NULL) {
			$this->_table = plural(preg_replace('/(_m|_model)?$/', '', strtolower(get_class($this))));
		}
	}

	/**
	 * Trigger a method before or after a CRUD function
	 * 
	 * @param  string 	$event event to trigger
	 * @param  mixed 	$data  could be an array of data to process
	 * @return mixed 	$data  array processed or FALSE if it is not provided
	 */
	
	protected function trigger($event, $data = FALSE)
	{
		if (isset($this->$event) && is_array($this->$event)) {
			foreach ($this->$event as $method) {
				if (strpos($method, '(')) {
					preg_match('/([a-zA-Z0-9\_\-]+)(\(([a-zA-Z0-9\_\-\., ]+)\))?/', $method, $matches);
					$method = $matches[1];
					$this->callback_params = explode(',', $matches[3]);
				}
				$data = call_user_func_array(array($this, $method), array($data));
			}
		}
		return $data;
	}

	/**
	 * Serialises data for you automatically, allowing you to pass
	 * through objects and let it handle the serialisation in the background
	 * 
	 * @param  array $data data for process before or after an operation
	 * @return array $data serialized data
	 */
	
	public function serialize($data)
	{
		foreach ($this->callback_params as $field) {
			$data[$field] = serialize($data[$field]);
		}

		return $data;
	}

	/**
	 * Unserialises data for you automatically, allowing you to pass
	 * through objects and let it handle the unserialisation in the background
	 * 
	 * @param  array $data data for process
	 * @return array $data unserialized data
	 */
	
	public function unserialize($data)
	{
		foreach ($this->callback_params as $field) {
			if (is_array($data)) {
				$data[$field] = unserialize($data[$field]);
			} else {
				$data->$field = unserialize($data->$field);
			}
		}

		return $data;
	}

	/* --------------------------------------------------------------
	 * CRUD METHODS
	 * ------------------------------------------------------------ */

	/**
	 * Select tabla desencriptada.
	 * 
	 * @param  string 	$data info to insert
	 * @param  Bool    returns the inserted id on sucess or FALSE
	 */
	
	protected function select_crypt($data, $opcion = FALSE)
	{
		if (!empty($data)) {
			$data = explode(',', $data);
			foreach ($data as $k_1 => $v_1) {
				foreach ($this->field_crypt as $k_2 => $v_2) {
					$data[$k_1] = str_replace($v_2, "AES_DECRYPT(".$v_2.", '".$this->config->item('aes_encryption_key')."')", $v_1);
				}
				$data[$k_1] = ($k_1 < (count($data) - 1)) ? $data[$k_1].',' : $data[$k_1];
			}
			$this->db->select(implode($data), $opcion);
		} else {
			$this->db->select('SQL_CALC_FOUND_ROWS *', $opcion);
		}
	}

	protected function like_crypt($fields, $search, $side = '', $escape = false)
	{
		if(!empty($fields))
		{
			$fields = explode(',', $fields);
			foreach ($fields as $k_1 => $v_1) {
				foreach ($this->field_crypt as $k_2 => $v_2) {
					$fields[$k_1] = str_replace($v_2, "AES_DECRYPT(".$v_2.", '".$this->config->item('aes_encryption_key')."')", $v_1);
				}
				$fields[$k_1] = ($k_1 < (count($fields) - 1)) ? $fields[$k_1].',' : $fields[$k_1];
			}
			$this->db->like(implode($fields), $search, $side, $escape);
		} 
	}

	protected function order_by_crypt($fields, $opcion)
	{
		if(!empty($fields))
		{
			$fields = explode(',', $fields);
			foreach ($fields as $k_1 => $v_1) {
				foreach ($this->field_crypt as $k_2 => $v_2) {
					$fields[$k_1] = str_replace($v_2, "AES_DECRYPT(".$v_2.", '".$this->config->item('aes_encryption_key')."')", $v_1);
				}
				$fields[$k_1] = ($k_1 < (count($fields) - 1)) ? $fields[$k_1].',' : $fields[$k_1];
			}
			$this->db->order_by(implode($fields), $opcion);
		} 
	}


	/* --------------------------------------------------------------
	 * CRUD METHODS
	 * ------------------------------------------------------------ */

	/**
	 * Inserts a new row on the table.
	 * 
	 * @param  array $data info to insert
	 * @return mixed       returns the inserted id on sucess or FALSE
	 */
	
	public function insert($data)
	{
		if (is_array($data) && count($data) > 0) {
			$data = $this->trigger('pre_insert', $data);
			$this->db->insert($this->_table, $data);
			$id = $this->db->insert_id();
			$this->trigger('pos_insert', $id);
			return $id;
		} else {
			return FALSE;
		}
	}

	/**
	 * Update table data. It can be chainable with 'where' query helper.
	 * 
	 * @param  array $data 	info to be updated
	 * @param  int 	 $id 	primary key of the row to be updated (optional)
	 * @return mixed 		returns an integer on sucess or FALSE
	 */
	
	public function update($data, $id = '', $row = '')
	{
		if (is_array($data) && count($data) > 0) {
			$data = $this->trigger('pre_update', $data);
			
			if (!empty($id)) {
				if(!empty($row)) {
					$this->db->where($row, $id);
				} else {
					$this->db->where($this->_id, $id);
				}
			}
			$this->db->update($this->_table, $data);
			$rows = $this->db->affected_rows();
			$this->trigger('pos_update', array($data, $rows));
			return $rows;
		} else {
			return FALSE;
		}
	}

	public function insert_update($data, $id = '', $row = '')
	{
		if($this->exists($id, $row)) {
			return $this->update($data, $id, $row);
		} else {
			return $this->insert($data);
		}
	}

	/**
	 * Delete rows from table using primary key as reference
	 * 
	 * @param  mixed $id 	a single value or array of primary keys (optional)
	 * @return mixed      	returns a integer number of affected rows.
	 */
	
	public function delete($id = '', $rel = '')
	{
		$id = $this->trigger('pre_delete', $id);

		if (!empty($id)) {			
			if (!is_array($id)) {
				$id = array($id);
			}
			if(empty($rel)) {
				$this->db->where_in($this->_id, $id);
			} else {
				$this->db->where_in($rel, $id);
			}
		}

		$this->db->delete($this->_table);
		$rows = $this->db->affected_rows();
		$this->trigger('pos_delete', array($id, $rows));

		return $rows;
	}

	public function last_id()
	{
		$this->db->order_by($this->_id, 'DESC');
		$this->db->limit(1);
		return $this->db->get($this->_table)->row();
	}

	/**
	 * Delete rows from table using primary key as reference
	 * 
	 * @param  mixed $id 	a single value or array of primary keys (optional)
	 * @return mixed      	returns a integer number of affected rows.
	 */
	
	public function safe_delete($id = '', $rel = '')
	{
		$id = $this->trigger('pre_delete', $id);

		if (!empty($id)) {			
			if (!is_array($id)) {
				$id = array($id);
			}
			if(empty($rel)) {
				$this->db->where_in($this->_id, $id);
			} else {
				$this->db->where_in($rel, $id);
			}
		}

		$this->db->delete($this->_table);
		$rows = $this->db->affected_rows();
		$this->trigger('pos_delete', array($id, $rows));

		return $rows;
	}

	/**
	 * Get result set from table. If $id value are provided, it find only
	 * rows with specified primary key values. This function also can be
	 * chainable with query hepelrs like 'where', 'where in', etc.
	 * 
	 * @param  mixed  $id 		can be a single value or array (optional)
	 * @return object $result   data result from table
	 */
	
	public function get($id = '', $activo = FALSE, $order_by = '')
	{
		$id = $this->trigger('pre_get', $id);
		if (!empty($id)) {
			if (is_array($id)) {
				$this->db->where_in($this->_id, $id);
			} else {
				$this->db->where($this->_id, $id);
				$this->db->limit(1);
			}
		}
		($activo == TRUE) ? $this->db->where($this->_table. '.estatus'.$this->_prefix, '1') : '';
		if(is_array($order_by)) {
			foreach ($order_by as $key => $value) {
				$this->db->order_by($key, $value);
			}
		} 
		$result = $this->db->get($this->_table);
		$result = $this->trigger('pos_get', $result);

		return $result;
	}

	/* busqueda por relaccion de tablas */

	public function get_rel($id = '', $idBusqueda, $activo = FALSE, $order_by = '')
	{
		$id = $this->trigger('pre_get', $id);
		if (!empty($id)) {
			if (is_array($id)) {
				$this->db->where_in($idBusqueda, $id);
			} else {
				$this->db->where($idBusqueda, $id);
			}
		}
		($activo == TRUE) ? $this->db->where('estatus'.$this->_prefix, '1') : '';
		if(is_array($order_by)) {
			foreach ($order_by as $key => $value) {
				$this->db->order_by($key, $value);
			}
		}
		$result = $this->db->get($this->_table);
		$result = $this->trigger('pos_get', $result);

		return $result;
	}

	/* busqueda por relaccion de tablas enviando un array  key = al campo de busqueda, value = al valor de busqueda */

	public function get_send_array($array = array(), $activo = FALSE)
	{
		//$id = $this->trigger('pre_get', $id);
		if (!empty($array)) {
			foreach ($array as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		($activo == TRUE) ? $this->db->where($this->_table. '.estatus', '1') : '';
		$result = $this->db->get($this->_table);
		$result = $this->trigger('pos_get', $result);

		return $result;
	}

	/* busqueda de un resultado pero con los datos de tu seleccion */

	public function get_data($select = '', $id = '')
	{
		$this->db->select($select, FALSE);
		$id = $this->trigger('pre_get', $id);
		if (!empty($id)) {
			if (is_array($id)) {
				$this->db->where_in($this->_id, $id);
			} else {
				$this->db->where($this->_id, $id);
				$this->db->limit(1);
			}
		}

		$result = $this->db->get($this->_table);
		$result = $this->trigger('pos_get', $result);

		return $result;
	}

	/* Devuelve un array de las consultas de la base de datos encriptadas */

	public function result_decrypt($data) 
	{
		if($data->num_rows() > 0) {
			$datos = $data->result_array();
			for ($i = 0, $c = $data->num_rows(); $i < $c; $i++) { 
				foreach ($this->field_crypt as $key => $val) {
					if(array_key_exists($val, $datos[$i]))
						$datos[$i][$val] = $this->decrypt($datos[$i][$val]);
				}
				$datos[$i] = (object) $datos[$i];
			}
			$data->result_array = array_replace($data->result_array, $datos);
		}
		return $data;
	}

	/* busqueda tabla para devolucion de select o img */
	
	public function get_where($id = '', $where = '', $order = '', $distinct = '', $activo = FALSE)
	{
		$id = $this->trigger('pre_get', $id);
		if (!empty($id)) {
			if(!empty($distinct)) 
			{
				$this->db->select('distinct('.$distinct.'), '.$this->_id);;
			}
			if (is_array($id)) {
				$this->db->where_in($where, $id);
			} else {
				$this->db->where($where, $id);
			}
		}
		($activo == TRUE) ? $this->db->where($this->_table. '.estatus', '1') : '';
		if (!empty($order))
		{
			$this->db->order_by($order, 'ASC');
		}

		$result = $this->db->get($this->_table);
		$result = $this->trigger('pos_get', $result);

		return $result;
	}

	/* --------------------------------------------------------------
	 * QUERY HELPERS || ::EXPERIMENTAL::
	 * ------------------------------------------------------------ */

	/**
	 * Magic Method __call allows to execute DB methods like a wrapper. Also it allows
	 * chaining method for some active record query helpers.
	 * 
	 * @param  string $method 	method name
	 * @param  array  $params 	parameters to pass at the method
	 * @return mixed  			returns $this object or FALSE if method doesn't exists
	 */
	
	public function __call($method, $params)
	{
		if (method_exists($this->db, $method)) {
			if (in_array($method, array('count_all', 'empty_table', 'truncate'))) {
				return call_user_func_array(array($this->db, $method), array($this->_table));
			} else if (in_array($method, array('query', 'simple_query', 'insert_id', 'plattform', 'version', 'last_query'))) {
				return call_user_func_array(array($this->db, $method), $params);
			} else {
				call_user_func_array(array($this->db, $method), $params);
				return $this;
			}
		} else {
			return FALSE;
		}		
	}

	/**
	 * Count all rows of a result that a query produces
	 * 
	 * @return int rows
	 */
	
	public function count_all_results()
	{
		$this->total_results = $this->db->count_all_results($this->_table);
		return $this->total_results;
	}

	/**
	 * Count all rows of a query using MySQL SQL_CALC_FOUND_ROWS
	 * 
	 * @return int rows
	 */
	
	public function found_rows()
	{
		$query = $this->db->query('SELECT FOUND_ROWS() AS total');
		$data  = $query->row();
		return $data->total;
	}

	/* --------------------------------------------------------------
	 * UTILITIES
	 * ------------------------------------------------------------ */

	/**
	 * Static method that generate a stamp string. Is useful to rename 
	 * files uploaded and avoid overwriting.
	 * 
	 * @return string stamp
	 */
	
	public static function calculate_id()
	{
		return date("Ymds") . sprintf("%02d", rand(0,99));
	}

	/**
	 * Get the table name from the model
	 * 
	 * @return string table name
	 */
	
	public function table()
	{
		return $this->_table;
	}

	/**
	 * Returns an array containing the column names of the table
	 * 
	 * @return array
	 */
	
	public function table_columns()
	{
		if (count($this->field_names) > 0) {
			return $this->field_names;
		} else {
			$query = $this->db->query("SHOW COLUMNS FROM ".$this->_table);
			foreach ($query->result() as $row) {
				$this->field_names[] = $row->Field;
			}
			return $this->field_names;
		}
	}

	/**
	 * Generates a initial set array of values to be passed a form view. If a
	 * $data array is provided (could be post values), this data will be merged
	 * Devuelve tambien datos desencriptados
	 * 
	 * @param  array  $data array data set
	 * @return array        data initialized
	 */
	
	public function prepare_data($data = '', $return = '')
	{
		if (! is_array($return)) {
			$return = array();

			foreach ($this->field_names as $field) {
				$return[$field] = '';
			}
		}

		if(is_array($data)){
			if(is_array($this->field_crypt)) {
				$datos = array();
				foreach ($this->field_crypt as $key => $val) {
					$datos[$val] = $this->decrypt($data[$val]);
				}
				$data = array_replace_recursive($data, $datos);
			}
			$return = array_merge($return, $data);
		}

		return $return;
	}

	/**
	 * Search a row with a field that contains a specified value. If
	 * a row is finded, then returns TRUE. 
	 *  
	 * @param  string $value value to search in the column
	 * @param  string $field column name to search
	 * @return bool
	 */
	
	public function exists($value, $field = '')
	{
		if(!is_array($value) && !is_array($field)) {
			if(empty($field)){
				$field = $this->_id;
			}

			$num = $this->db->where($field, $value)->count_all_results($this->_table);
			return ($num > 0) ? TRUE : FALSE;
		} else {
			$num = $this->db->where($value)->count_all_results($this->_table);
			return ($num > 0) ? TRUE : FALSE;
		}
		
	}

	/**
	 * Produces a array that can be used with form helper. This also can be chained
	 * with other query helpers like: 
	 * $arr = $this->model->where('field', $search)->order_by($label, 'ASC')->dropdown($value, $label);
	 * 
	 * @param  string $value field that will be used as value
	 * @param  string $label field that will be used as label. If not provided, value field is used.
	 * @return array         option array
	 */
	
	public function dropdown($value, $label = '')
	{
		if (!empty($value)) {
			$query = $this->db->get($this->_table);
			$opt   = array(); 
			foreach ($query->result() as $row) {
				$opt[$row->$value] = (!empty($label)) ? $row->$label : $row->$value;
			}

			return $opt;
		} else {
			return array();
		}
	}

	/**
	 * Produces option tags for dropdowns. It uses dropdown function and is chainable
	 * with query helpers.
	 * 
	 * @param  string $value    field for values
	 * @param  string $label    field for labels, if not provided, value field is used (optional)
	 * @param  string $selected preselected value (optional)
	 * @return string           string option tags
	 */
	
	public function dropdown_opt($value, $label = '', $selected = '')
	{
		$opt = $this->dropdown($value, $label);
		$str = '';

		foreach ($opt as $key => $val) {
			$attr = ($key == $selected) ? ' selected' : '';
			$str .= '<option value="'. $key . '"' . $attr . '>' . $val . "</option>\n";
		}

		return $str;
	}

	public function datetime_creado($datos)
	{
		date_default_timezone_set('America/Mexico_City');
		$datos['creado'.$this->_prefix] = date('Y-m-d H:i:s');
		return $datos;
	}

	public function datetime_modificado($datos)
	{
		date_default_timezone_set('America/Mexico_City');
		$datos['actualizado'.$this->_prefix] = date('Y-m-d H:i:s');
		return $datos;
	}

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */