<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/';
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Site - 404 Page Not Found</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="<?php echo $base_url.'assets/img/icon.png'; ?>" rel="shortcut icon" />
<link href="<?php echo $base_url.'assets/css/app.css'; ?>" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="container">
		<div class="row">
			<div class="header_error">
				<div class="large-12 columns">
					<img src="<?php echo $base_url.'assets/img/logo.png'?>" class="text-center">
				</div>
				
			</div>
			<div class="body_error">
				<div class="large-12 columns">
					<h1 class="text-center"><?php echo $heading; ?></h1>
					<div class="text-center">
						<?php echo $message; ?>
					</div>
				</div>
				<div class="large-12 columns text-center">
					<a href="<?php echo $base_url?>" title="" class="button"><i class="fa fa-undo" aria-hidden="true"></i> Back to site</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>