<div class="row expanded home">
	<div class="row beneficios">
		<div class="large-24 columns titulo-beneficios" data-magellan>
			<h1 class="titulo"><?= $this->lang->line('casos_exito')['titulo']; ?></h1>
			<a href="#modalContacto" class="button hollow btn-beneficio btn">
				<span><?= $this->lang->line('casos_exito')['btn_casos_exito']; ?></span>
			</a>
		</div>

		<div class="row small-up-2 medium-up-3 large-up-4 galeria">
			<?php 
			//$fileList = glob('assets/img/galeria/*'); 
			for($cont = 0; $cont < count($casos); $cont++):
				$caso = $casos[$cont];
			?>
				<div class="column column-block img">
				<a data-fancybox="gallery" data-animation-duration="500" data-src="#galeriaModal<?= $cont?>" href="javascript:;"><div><img src="<?= site_url('assets/img/fondo-galeria.png');?>" style="background-image:url('<?= site_url($caso["big_dir_img"].$caso["big_name_img"]." (large).".$caso["big_ext_img"]);?>');" alt=""></div></a>
				<div style="display: none;" id="galeriaModal<?= $cont?>" class="animated-modal gal-modal">
					<div class="cont-modal">
						<div class="img-modal">
							<div class="adornos">
								<div class="cinta"><img src="<?= site_url('assets/img/Cuadrado4.svg?v=2');?>" alt=""><div class="rectangulo"></div></div>
								<div class="cinta"><div class="rectangulo"></div><img src="<?= site_url('assets/img/Cuadrado3.svg?v=2');?>" alt=""></div>
							</div>
							<img src="<?= site_url('assets/img/fondo-galeria.png');?>" style="background-image: url('<?= site_url($caso["big_full_route_img"]);?>');" alt="">
						</div>
						<div class="inf-modal">
							<div class="texto">
								<h3 class="titulo-item">
									<?= ($this->session->userdata("language") == 'spanish') ? $caso["titulo_caso"] : $caso["titulo_ingles_caso"]; ?>
								</h3>
								<span><?= ($this->session->userdata("language") == 'spanish') ? $caso["lugar_caso"] : $caso["lugar_ingles_caso"];?></span>
								<div class="cont-info">
									<img src="<?= site_url('assets/img/Cuadrado.svg?v=2');?>" alt=""><p><img src="<?= site_url('assets/img/iconos/casos-exito/bateria.svg');?>"><?= $this->lang->line('casos_exito')['capacidad']; ?> <?= $caso["capacidad_caso"];?> Wp</p>
									<img src="<?= site_url('assets/img/Cuadrado.svg?v=2');?>" alt=""><p><img src="<?= site_url('assets/img/iconos/casos-exito/thunder.svg');?>"><?= $this->lang->line('casos_exito')['produccion']; ?> <?= $caso["produccion_caso"];?> kWh</p>
								</div>
							</div>
							<div class="texto-secundario">
								<p><?= ($this->session->userdata("language") == 'spanish') ? $caso["informacion_caso"] : $caso["informacion_ingles_caso"];?></p>
								<div class="botonesFlechas">
									<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left"><</button>
									<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right">></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endfor;?>
		</div>

		<!--<div class="medium-24 columns text-center">
			<a class="button success btn-vermas btn">
				<span>VER MÁS</span>
			</a>
		</div> -->

	</div>
</div>


<?php if(count($testimonios) > 0): ?>
<div class="row expanded home">
	<div class="row testimonios">
		<h1 class="titulo"><?= $this->lang->line('testimonios')['titulo']; ?></h1>
		<div class="row texto-testimonio">
			<div>
				<?php foreach ($testimonios as $testimonio): ?>
				<p>
				<?= ($this->session->userdata("language") == 'spanish') ? $testimonio["descripcion_testimonio"] : $testimonio["descripcion_ingles_testimonio"];?>
				</p>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="imagenes">
			<div class="img-testimonios">
				<?php foreach ($testimonios as $testimonio): ?>
				<div class="img">
					<img src="<?= site_url('assets/img/fondo-perfil-testimonio.png');?>" style="background-image: url('<?= site_url($testimonio["full_route_img"]);?>');" alt="<?= $testimonio["cliente_testimonio"] ?>">
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>

<script>
	$(document)
		.on('ready', function(){
			// $('.slider-casos').slick({
			// 	dots: false,
			// 	infinite: true,
			// 	speed: 500,
			// 	fade: true,
			// 	autoplay: true,
  			// 	autoplaySpeed: 5000,
			// });

			$('.texto-testimonio').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				asNavFor: '.img-testimonios'
			});

			$('.img-testimonios').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?= site_url('assets/img/left.png');?>" alt=""></button>',
                nextArrow: '<button type="button" class="slick-next"><img src="<?= site_url('assets/img/right.png');?>" alt=""></button>',
				slidesToShow: 1,
				slidesToScroll: 1,
				asNavFor: '.texto-testimonio',
				dots: false,
				centerMode: false,
				focusOnSelect: false
			});
		});
</script>
<?php endif; ?>


