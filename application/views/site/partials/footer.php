<?php
		date_default_timezone_set('Mexico/General');
		$date = new DateTime('NOW');
		$dateTime = $date->format('Y');
?>
<footer id="foot">
	<div class="row">
		<h2 class="titulo"><?= $this->lang->line('footer')['titulo']; ?></h2>
	</div>
	<div class="fila-contacto">
		<div class="columna">
			<form action="correo-contacto" class="footer-form" method="POST">
				<div class="fila formul">
					<div class="columns">
						<label>
							<input type="text" placeholder="<?= $this->lang->line('footer')['form']['nombre']; ?>" name="nombre" required>
						</label>
					</div>
					<div class="columns">
						<label>
							<input type="email" placeholder="<?= $this->lang->line('footer')['form']['email']; ?>" name="correo" required>
						</label>
					</div>
					<div class="columns">
						<label>
							<input type="text" placeholder="<?= $this->lang->line('footer')['form']['telefono']; ?>" name="telefono" required>
						</label>
					</div>
					<div class="columns">
						<label>
							<input type="text" placeholder="<?= $this->lang->line('footer')['form']['cuanto_paga']; ?>" name="pago-bimestre" required>
						</label>
					</div>
				</div>	
				<div class="row">	
					<div class="small-24 columns">
						<label>
							<textarea name="mensaje" required placeholder="<?= $this->lang->line('footer')['form']['mensaje']; ?>" id="" rows="4"></textarea>
						</label>
					</div>
				</div>	
				<div class="boton">
					<button type="submit" class="button success btn-enviar btn">
						<span><?= $this->lang->line('footer')['form']['enviar']; ?></span>
					</button>
				</div>
				
			</form>
		</div>
		<div class="columna footer-info">
			<ul class="menu vertical">
				<li>
					<a href="">
						<div class="img home">
							<img src="<?php echo site_url('assets/img/iconos/contacto/home.svg');?>" alt="">
						</div>
						<div class="texto">
							<span>
								<?= $this->lang->line('footer')['calle']; ?> 49ᴬ 415,Francisco de Montejo II, <br>
								Mérida, Yuc., Francisco de Montejo
							</span>
						</div>
					</a>
				</li>
				<li>
					<div class="telefono cels">
						<div class="img">
							<img src="<?php echo site_url('assets/img/iconos/contacto/phone.svg');?>" alt="">
						</div>
						<div class="texto">
							<span><?= $this->lang->line('footer')['oficina']; ?></span>
							<ul class="menu vertical">
								<li>
									<a href="tel:+529999812876">Tel: (999) 9 81 28 76</a>
								</li>
								<li>
									<a href="tel:+529993573355">Cel: 999 357 3355</a>
								</li>
								<li>
									<a href="tel:+529999009869">Cel: 9999 009869</a>
								</li>
							</ul>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="row info-copy">
		<p>© Copyright <?php echo $dateTime ?> <?= $this->lang->line('footer')['aviso']; ?></p>
	</div>
</footer>