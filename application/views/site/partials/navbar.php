<header class="header-site" data-sticky-container>
	<div class="header-sticky sticky" id="stick" data-sticky data-margin-top="0">
		<div class="row expanded">
			<a href="#" class="medium-3 medium-offset-1 columns logo">
				<img src="<?= site_url('assets/img/fondo-logo.png');?>" alt="Solárek">
			</a>
			<div class="medium-18 large-20 columns menu-header" data-magellan data-offset="50">
				<ul class="menu align-right">
					<li><a href="<?= site_url();?>" <?= active('inicio', $active); ?>><?= $this->lang->line('header')['inicio']; ?></a></li>
					<?php if($active == "inicio"): ?>
						<li><a href="#beneficios" ><?= $this->lang->line('header')['beneficios']; ?></a></li>
					<?php else: ?>
						<li><a href="<?= site_url();?>beneficios" ><?= $this->lang->line('header')['beneficios']; ?></a></li>
					<?php endif; ?>
					<li><a href="<?= site_url('casos-exito');?>" <?= active('casos-exito', $active); ?>><?= $this->lang->line('header')['casos_de_exito']; ?></a></li>
					<li><a href="<?= site_url('financiamiento');?>" <?= active('credito', $active); ?>><?= $this->lang->line('header')['credito']; ?></a></li>
					<li><a href="<?= site_url('blog');?>" <?= active('blog', $active); ?>><?= $this->lang->line('header')['blog']; ?></a></li>
					<li><a href="<?= site_url('contacto');?>" <?= active('contacto', $active); ?>><?= $this->lang->line('header')['contacto']; ?></a></li>
					<li class="iconos-nav">
						<a href="https://www.facebook.com/solarekmerida/" target="_blank" class="facebook">
							<img class="svg" src="assets/img/iconos/facebook-logo.svg" alt="">
						</a>
						<a href="https://wa.me/5219992976094" target="_blank" class="whatsapp">
							<img class="svg" src="assets/img/iconos/whatsapp-logo.svg" alt="">
						</a>
						<a href="https://www.instagram.com/solarek.merida/" target="_blank" class="instagram">
							<img class="svg" src="assets/img/iconos/instagram-logo.svg" alt="">
						</a>
					</li>
					<li class="banderas">
						<a class="<?= ($this->session->userdata("language") == 'spanish') ? "active": "";?>" href="<?php echo site_url('idioma/es');?>">
							ESP
						</a>
						/
						<a class="<?= ($this->session->userdata("language") == 'english') ? "active": "";?>" href="<?php echo site_url('idioma/en');?>">
							ENG
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</header>


<header class="header-small" data-sticky-container>
	<nav id="header-bar" class="small-header" data-sticky  data-sticky-on="small" data-options="marginTop:0;"> 
		<div class="title-bar" data-responsive-toggle="top-bar" data-hide-for="medium">
			<button class="menu-icon" type="button" data-curtain-menu-button></button>
			<div class="title-bar-title" data-curtain-menu-button>
				Menu
			</div>
			<a href="#" class="medium-3 medium-offset-1 columns logo">
				<img src="<?= site_url('assets/img/fondo-logo.png');?>" class="logo btn-logo">
			</a>
			<a href="tel:+529993044922" title="" class="phone">
				<i class="fas fa-phone"></i>
			</a>
		</div> 
	</nav>
</header>

<script>
	$('[data-curtain-menu-button]').click(function(){
		$('body').toggleClass('curtain-menu-open');
	});
</script>

<div class="curtain-menu">
	<div class="curtain"></div>
	<div class="curtain"></div>
	<div class="curtain"></div>
	<div class="curtain-menu-wrapper">
		<ul class="curtain-menu-list menu vertical" data-dropdown-menu data-deep-linking="false" data-disable-hover="true">
			<li class="iconos-nav">
				<a href="https://www.facebook.com/solarekmerida/" target="_blank" class="facebook">
					<img class="svg" src="assets/img/iconos/facebook-logo.svg" alt="">
				</a>
				<a href="https://wa.me/5219992976094" target="_blank" class="whatsapp">
					<img class="svg" src="assets/img/iconos/whatsapp-logo.svg" alt="">
				</a>
				<a href="https://www.instagram.com/solarek.merida/" target="_blank" class="instagram">
					<img class="svg" src="assets/img/iconos/instagram-logo.svg" alt="">
				</a>
			</li>
			<li><a href="<?= site_url();?>" <?= active('inicio', $active); ?>><?= $this->lang->line('header')['inicio']; ?></a></li>
			<?php if($active == "inicio"): ?>
						<li><a href="#beneficios" ><?= $this->lang->line('header')['beneficios']; ?></a></li>
					<?php else: ?>
						<li><a href="<?= site_url();?>beneficios" ><?= $this->lang->line('header')['beneficios']; ?></a></li>
					<?php endif; ?>
			<li><a href="<?= site_url('casos-exito');?>" <?= active('casos-exito', $active); ?>><?= $this->lang->line('header')['casos_de_exito']; ?></a></li>
			<li><a href="<?= site_url('financiamiento');?>" <?= active('credito', $active); ?>><?= $this->lang->line('header')['credito']; ?></a></li>
			<li><a href="<?= site_url('blog');?>" <?= active('blog', $active); ?>><?= $this->lang->line('header')['blog']; ?></a></li>
			<li><a href="<?= site_url('contacto');?>" <?= active('contacto', $active); ?>><?= $this->lang->line('header')['contacto']; ?></a></li>
			<li class="banderas">
				<a class="<?= ($this->session->userdata("language") == 'spanish') ? "active": "";?>" href="<?php echo site_url('idioma/es');?>">
					ESP
				</a>
				/
				<a class="<?= ($this->session->userdata("language") == 'english') ? "active": "";?>" href="<?php echo site_url('idioma/en');?>">
					ENG
				</a>
			</li>
		</ul>
	</div>
</div>



