

<?php if(isset($banners)): ?>
  <?php if($datosBanner["banner_type_home"] == "1"): ?>
    <div class="row expanded banner-home" id="banner">
        <div class="imgFondo">
            <img id="fondo">
        </div>
        <script>
            $(document).ready(cambiarBanners);

            $(window).on('resize', checkNeedToChange);

            let showingBigBanner;
            let breakpoint = 622;

            function checkNeedToChange(){
              if($(window).innerWidth() <= breakpoint) {
                if(showingBigBanner){
                  cambiarBanners();
                }
              }else{
                if(!showingBigBanner){
                  cambiarBanners();
                }
              }
            }


            function cambiarBanners() {
              let $banner = $("#fondo");
              let len = "<?= $this->session->userdata("language") ?>";
              let pathBig = "<?=$banners["img_banner_home"]["full_route_img"]?>",
              pathBigIng = "<?=$banners["img_banner_home_ingles"]["full_route_img"]?>",
              pathMov = "<?=$banners["img_banner_home_movil"]["full_route_img"]?>",
              pathMovIng = "<?=$banners["img_banner_home_movil_ingles"]["full_route_img"]?>",
              ingles = false,
              dir = pathBig;

              if(len == "english"){
                ingles = true;
                dir = pathBigIng;
              }

              if($(window).innerWidth() <= breakpoint) {
                dir = pathMov;
                if(ingles){
                  dir = pathMovIng;
                }
                showingBigBanner = false;
              }else{
                showingBigBanner = true;
              }

              $banner.attr("src", dir);
            }
        </script>
    </div>
  <?php elseif($datosBanner["banner_type_home"] == "2"): ?>   
    <?php preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", ($this->session->userdata("language") == 'spanish') ? $datosBanner["url_youtube_id_home"] : $datosBanner["url_youtube_id_home_ingles"], $matches);?>
    <div class="tv" style="position: relative;">
       <div class="screen" id="video"></div>
       <div class="taparVideo" style="z-index: 2; position: absolute; width: 100%; height: 100%; top: 0; left: 0; background: linear-gradient(to bottom, #000, transparent, transparent, transparent); "></div>
    </div>
    
    <script>
      $(document).on("ready", function(){
        let tv = $('#video').YTPlayer({
          fitToBackground: false,
          videoId: '<?= $matches[1] ?>',
          repeat: true,
          playerVars: {
            modestbranding: 0,
            autoplay: 1,
            autohide: 1,
            controls: 0,
            showinfo: 0,
            branding: 0,
            disablekb: 1, 
            enablejsapi: 0, 
            iv_load_policy: 3,
            autohide: 0,
            rel: 0,
            showinfo: 0, 
            autohide: 0
          }
        });

        function vidRescale(){
          var w = $(window).width()+200,
            h = $(window).height()+200;

          if (w/h > 16/9){
            tv.setSize(w, w/16*9);
            $('.tv .screen').css({'left': '0px'});
          } else {
            tv.setSize(h/9*16, h);
            $('.tv .screen').css({'left': -($('.tv .screen').outerWidth()-w)/2});
          }
        }

        $(window).on('load resize', function(){
          vidRescale();
        });
      });
    </script>
  <?php endif; ?> 
<?php else: ?>
<div class="row expanded banner-home" id="banner">
    <div class="imgFondo">
        <a href="#modalContacto" class="btn-banner btn">
          <span><?= $this->lang->line('banners')['btn-contacto']; ?></span>
        </a>
        <img id="fondo" src="" alt="">
        <div class="imgTilt js-tilt">
          <img id="panel" class="first" src="" alt="">
          <img id="letras" class="second" src="" alt="">
      </div>
    </div>
</div>

<script>

// console.log($(document).innerWidth());
// //alert($(window).width());

// $(window).resize(function() {
//     console.log($(document).innerWidth());
// });


$(document).ready(cambiarBanners);

$(document).ready(toggleClassBotonBanner);

function toggleClassBotonBanner(){
  let active = "<?= $active ?>";
  let newClass;
  switch (active){
    case 'inicio':
      newClass = "bnr-home";
      break;
    case 'casos-exito':
      newClass = "bnr-casos";
      break;
    case 'credito':  
      newClass = "bnr-credito";
      break;
  }
  $(".btn-banner").addClass(newClass);
}

$(window).on('resize', checkNeedToChange);

let showingBigBanner;
let breakpoint = 622;

function checkNeedToChange(){
  if($(window).innerWidth() <= breakpoint) {
    if(showingBigBanner){
      cambiarBanners();
    }
  }else{
    if(!showingBigBanner){
      cambiarBanners();
    }
  }
}


function cambiarBanners() {
  let banner = $("#banner");
  let active = "<?= $active ?>";
  let len = "<?= $this->session->userdata("language") ?>";
  let fondo = $(banner).find("#fondo");
  let panel = $(banner).find("#panel");
  let info = $(banner).find("#letras");
  let path = "<?=site_url('assets/banners')?>"+"/";
  let dirFondo, dirPanel, dirinfo;

  switch (active){
    case 'inicio':
      dirFondo = dirPanel = dirinfo = "Home/";
      break;
    case 'casos-exito':
      dirFondo = dirPanel = dirinfo = "Casos/";
      break;
    case 'credito':  
      dirFondo = dirPanel = dirinfo = "Financiamiento/";
      break;
    default:
      dirFondo = dirPanel = dirinfo = "Home/";
      break;
  }

  if(len == "english"){
    dirFondo += "ingles/";
    dirPanel += "ingles/";
    dirinfo +="ingles/";

  }

  let endFondo;
  let endPanel;
  let endinfo;

  if($(window).innerWidth() <= breakpoint) {
    endFondo = "mobil/Energiaa.svg";
    endPanel = "mobil/Panel.svg";
    endinfo = "mobil/Info.svg";
    showingBigBanner = false;
  }else{
    endFondo = "Energiaa.svg";
    endPanel = "Panel.svg";
    endinfo = "Info.svg";
    showingBigBanner = true;
  }

  let srcFondo = path + dirFondo + endFondo;
  let srcPanel = path + dirPanel + endPanel;
  let srcinfo = path + dirinfo + endinfo;

  $(fondo).attr("src", srcFondo);
  $(panel).attr("src", srcPanel);
  $(info).attr("src", srcinfo);
}
</script>

<?php endif ?>
