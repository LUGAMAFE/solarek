<?php
		date_default_timezone_set('Mexico/General');
		$date = new DateTime('NOW');
		$dateTime = $date->format('Y');
?>
<div class="row expanded banner-contacto" style="background-image: url('<?= site_url('assets/banners/Blog/main.jpg');?>');">
    <div class="filtro filtro-blog"></div>
    <div class="texto">
        <span><?= $this->lang->line('blog')['titulo']; ?></span>
    </div>
</div>

<section class="entradas-template">
    <h2 class="title"><?= $this->lang->line('blog')['entradas_recientes']; ?></h2>

    <div class="layout-contenido">
        <div class="contenido">
            <div class="img-entrada">
                <div class="adornos">
                    <p class="cinta"><img src="<?= site_url('assets/img/Cuadrado.svg?v=2');?>" alt=""> <span class="rectangulo"><?= formatDateToString($entradaBlog["fecha_modificacion_entrada"], $this->session->userdata("language"));?> - <?= $entradaBlog["autor_entrada"];?></span> </p>
                </div>
                <img src="<?= site_url($entradaBlog["big_full_route_img"]);?>">
            </div>
            <div class="info-entrada">
                <h2> <?= ($this->session->userdata("language") == 'spanish') ? $entradaBlog["titulo_entrada"] : $entradaBlog["titulo_ingles_entrada"];?></h2>
                <h3 class="subtitulo"><?= ($this->session->userdata("language") == 'spanish') ? $entradaBlog["subtitulo_entrada"] : $entradaBlog["subtitulo_ingles_entrada"];?></h3>
                <?= ($this->session->userdata("language") == 'spanish') ? $entradaBlog["informacion_entrada"] : $entradaBlog["informacion_ingles_entrada"];?>
                <?php if($entradaBlog["check_video_entrada"]):
                    preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $entradaBlog["url_youtube_id_entrada"], $matches);
                ?>
                    <div class="videoWrapper">
                        <!-- Copy & Pasted from YouTube -->
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/<?=$matches[1]?>?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="left-bar">
            <div class="recomendados">
                <div class="title"><?= $this->lang->line('blog')['recomendados']; ?></div>
                <ul class="links">
                    <?php foreach ($entradas as $entrada): ?>
                        <li>
                            <a href="<?= site_url('blog/'.create_slug($entrada["titulo_entrada"])."/".$entrada["id_entrada"]);?>">
                                <?= ($this->session->userdata("language") == 'spanish') ? $entrada["titulo_entrada"] : $entrada["titulo_ingles_entrada"];?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    <button class="down downlinks"><img src="<?= site_url('assets/img/iconos/blog/right.svg');?>" alt=""></button>
                </ul>
            </div>
            <a href="descargables" class="descargables">
                <img src="<?= site_url('assets/banners/Blog/archivos.png');?>">
                <h4><?= $this->lang->line('blog')['descargables']; ?></h4>
            </a>
            <script>
                $(document).ready(function(){
                    $(".downlinks").on("click", function(){
                        $(".recomendados .links").toggleClass("show");
                        showLinks();
                    });
                    function showLinks(){
                        let $links = $(".recomendados .links li");
                        if($(".recomendados .links").hasClass("show")){
                            $links.fadeIn();
                        }else{
                            let cont = 0;
                            $links.each(function( index, elemnt){
                                if(cont >= 3){
                                    $(elemnt).fadeOut();
                                }
                                cont++;
                            });
                        }
                    }
                    showLinks();
                });
            </script> 
        </div>

        <div class="bottom-entrada">
            <div class="fila">
                <div class="redes">
                    <p>Compartir</p>
                    <div class="iconos">
                        <!-- Your share button code -->
                        <a href=""><img src="<?= site_url('assets/img/iconos/blog/share.png');?>" alt=""></a>
                        <div class="fb-share-button" 
                            data-href=<?= current_url() ?> 
                            data-layout="button">
                        </div>
                        <a href="http://twitter.com/share?text=<?= str_replace("%", "", html_entity_decode(($this->session->userdata("language") == 'spanish') ? $entradaBlog["subtitulo_entrada"] : $entradaBlog["subtitulo_ingles_entrada"], ENT_QUOTES, "UTF-8"));?>&url=<?= current_url() ?>"><img src="<?= site_url('assets/img/iconos/blog/twitter.png');?>" alt=""></a>
                    </div>
                </div>

                <!-- <div class="paginador">
                    <button class="left"><img src="<¿= site_url('assets/img/iconos/blog/right.svg');?>" alt=""></button>
                    <button class="selected">1</button>
                    <button>2</button>
                    <button>3</button>
                    <button class="right"><img src="<¿= site_url('assets/img/iconos/blog/right.svg');?>" alt=""></button>
                </div> -->
            </div>
        </div>
    </div>

</section>

<section class="banner-ahorrar">
    <div class="imagen" style="background-image: url('<?= site_url('assets/banners/Blog/ahorrar.jpg');?>');">
        <p class="texto">
            El momento de ahorrar es ahora
        </p>
    </div>
    <div class="borde">    
        <div class="boton">
            <a href="#modalContacto" class="button btn btn-agendar">
                <span>Agendar Cita</span>
            </a>
        </div>
    </div>
</section>

<section class="copy">
    <p >© Copyright <?= $dateTime ?> | SOLÁREK Energía Alternativas | TODOS LOS DERECHOS RESERVADOS | Términos y condiciones | Aviso de privacidad</p>
</section>


<script>
    $(document).ready(quitarStick);

    function quitarStick() {
        let header = $("#stick");
        $(header).addClass("activo");
    }
</script>    

