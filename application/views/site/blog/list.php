<?php
		date_default_timezone_set('Mexico/General');
		$date = new DateTime('NOW');
		$dateTime = $date->format('Y');
?>
<div class="row expanded banner-contacto" style="background-image: url('<?= site_url('assets/banners/Blog/main.jpg');?>');">
    <div class="filtro filtro-blog"></div>
    <div class="texto">
        <span><?= $this->lang->line('blog')['titulo']; ?></span>
    </div>
</div>

<section class="entradas-template">
    <h2 class="title"></h2>

    <div class="layout-contenido">
        <div class="contenido entradas">
            <?php foreach ($entradas as $entrada): ?>
                <div class="entrada"> 
                    <div class="contenido">
                        <div class="imagen-entrada" style="background-image: url('<?= site_url($entrada["ruta_small"]);?>">
                        </div>
                        <div class="info">
                            <h2>
                                <?= ($this->session->userdata("language") == 'spanish') ? $entrada["titulo_entrada"] : $entrada["titulo_ingles_entrada"];?>
                            </h2>
                            <h3>
                                <?= ($this->session->userdata("language") == 'spanish') ? $entrada["subtitulo_entrada"] : $entrada["subtitulo_ingles_entrada"];?>
                            </h3>
                            <p>
                                <?= ($this->session->userdata("language") == 'spanish') ? $entrada["resumen_entrada"] : $entrada["resumen_ingles_entrada"];?>
                            </p>
                        </div>
                        <a href="<?= site_url('blog/'.create_slug($entrada["titulo_entrada"])."/".$entrada["id_entrada"]);?>" class="button btn vermas"><?= $this->lang->line('blog')['btn_ver_mas']; ?></a>
                    </div> 
                </div>
            <?php endforeach; ?>
        </div>

        <div class="left-bar">
            <div class="recomendados">
                <div class="title"><?= $this->lang->line('blog')['recomendados']; ?></div>
                <ul class="links">
                    <?php foreach ($entradasRec as $entradaRec): ?>
                        <li>
                            <a href="<?= site_url('blog/'.create_slug($entradaRec["titulo_entrada"])."/".$entradaRec["id_entrada"]);?>">
                                <?= ($this->session->userdata("language") == 'spanish') ? $entradaRec["titulo_entrada"] : $entradaRec["titulo_ingles_entrada"];?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    <button class="down downlinks"><img src="<?= site_url('assets/img/iconos/blog/right.svg');?>" alt=""></button>
                </ul>
            </div>
            <a href="descargables" class="descargables">
                <img src="<?= site_url('assets/banners/Blog/archivos.png');?>">
                <h4><?= $this->lang->line('blog')['descargables']; ?></h4>
            </a>
            <script>
                $(document).ready(function(){
                    $(".downlinks").on("click", function(){
                        $(".recomendados .links").toggleClass("show");
                        showLinks();
                    });
                    function showLinks(){
                        let $links = $(".recomendados .links li");
                        if($(".recomendados .links").hasClass("show")){
                            $links.fadeIn();
                        }else{
                            let cont = 0;
                            $links.each(function( index, elemnt){
                                if(cont >= 3){
                                    $(elemnt).fadeOut();
                                }
                                cont++;
                            });
                        }
                    }
                    showLinks();
                });
            </script>    

            <div class="paginador">
                
            </div>
        </div>
    </div>

</section>


<section class="banner-ahorrar">
    <div class="imagen" style="background-image: url('<?= site_url('assets/banners/Blog/ahorrar.jpg');?>');">
        <p class="texto">
            <?= $this->lang->line('blog')['ahorro']; ?>
        </p>
    </div>
    <div class="borde">     
        <div class="boton">
            <a href="#modalContacto" class="button btn btn-agendar">
                <span><?= $this->lang->line('blog')['btn_agendar']; ?></span>
            </a>
        </div>
    </div>
</section>

<section class="copy">
    <p >© Copyright <?= $dateTime ?> <?= $this->lang->line('footer')['aviso']; ?></p>
</section>



<script>
    $(document).ready(quitarStick);

    function quitarStick() {
        let header = $("#stick");
        $(header).addClass("activo");
    }
    const totalPaginas = <?= $totalPaginas; ?>
</script>    


