<div class="row expanded home" id="beneficios">
	<div class="row beneficios">
		<div class="medium-8 columns titulo-beneficios" data-magellan>
			<h1 class="titulo"><?= $this->lang->line('beneficios')['titulo']; ?></h1>
			<a href="#modalContacto" class="button hollow btn-beneficio btn">
				<span><?= $this->lang->line('beneficios')['subtitulo']; ?></span>
			</a>
		</div>
		<div class="medium-16 columns">
			<div class="row small-up-1 medium-up-2 large-up-2 list-beneficios">

				<div class="column column-block">
					<div class="texto">
						<div class="img">
							<?= file_get_contents('assets/img/iconos/beneficios/contract.svg');?>
						</div>
						<h6 class="titulo"><?= $this->lang->line('beneficios')['lista']['titulo1']; ?></h6>
						<p>
							<?= $this->lang->line('beneficios')['lista']['texto1']; ?>
						</p>
					</div>
				</div>

				<div class="column column-block">
					<div class="texto">
						<div class="img">
							<?= file_get_contents('assets/img/iconos/beneficios/settings.svg');?>
						</div>
						<h6 class="titulo"><?= $this->lang->line('beneficios')['lista']['titulo2']; ?></h6>
						<p>
							<?= $this->lang->line('beneficios')['lista']['texto2']; ?>
						</p>
					</div>
				</div>

				<div class="column column-block">
					<div class="texto">
						<div class="img">
							<?= file_get_contents('assets/img/iconos/beneficios/tv.svg');?>
						</div>
						<h6 class="titulo"><?= $this->lang->line('beneficios')['lista']['titulo3']; ?></h6>
						<p>
							<?= $this->lang->line('beneficios')['lista']['texto3']; ?>
						</p>
					</div>
				</div>

				<div class="column column-block">
					<div class="texto">
						<div class="img">
							<?= file_get_contents('assets/img/iconos/beneficios/global.svg');?>
						</div>
						<h6 class="titulo"><?= $this->lang->line('beneficios')['lista']['titulo4']; ?></h6>
						<p>
							<?= $this->lang->line('beneficios')['lista']['texto4']; ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row expanded banner experiencia">
	<div class="img">
		<img src="<?= site_url('assets/img/banner-experiencia.jpg');?>" alt="">
		<div class="texto full">
			<div class="center">
				<h1 class="titulo-experiencia">
					<?= $this->lang->line('experiencia')['titulo']; ?>
				</h1>
				<div class="row text-center btn-option" data-magellan data-offset="0">
					<a href="#modalContacto" class="button success btn-experiencia btn">
						<span><?= $this->lang->line('experiencia')['texto']; ?></span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row expanded numeros">
		<div class="row small-up-1 medium-up-3 large-up-3">
			<div class="column column-block">
				<div class="texto">
					<h1 class="numero">
						<span class="numscroller" data-min='1' data-max='200' data-delay='5' data-increment='10'>0</span>
					</h1>
					<h4 class="titulo"><?= $this->lang->line('experiencia')['instalaciones']; ?></h4>
				</div>
			</div>
			<div class="column column-block">
				<div class="texto">
					<h1 class="numero">
						<sup>+</sup>
						<span class="numscroller" data-min='1' data-max='5' data-delay='1' data-increment='2'>0</span>
					</h1>
					<h4 class="titulo"><?= $this->lang->line('experiencia')['anios']; ?></h4>
				</div>
			</div>
			<div class="column column-block">
				<div class="texto">
					<h1 class="numero">
						<span class="numscroller" data-min='1' data-max='1000' data-delay='5' data-increment='10'>0</span>
					</h1>
					<h4 class="titulo"><?= $this->lang->line('experiencia')['paneles']; ?></h4>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row expanded home">
	<div class="row financiamiento">
		<h1 class="titulo">
			<?= $this->lang->line('financiamiento_home')['titulo']; ?>
		</h1>
		<div class="row small-up-1 medium-up-2 large-up-4 list-financiamiento">
			<div class="column column-block">
				<div class="texto">
					<img src="<?= site_url('assets/img/logos/asi-programa-ahorro-sistematico-integral.png');?>" alt="">
					<p>
						<?= $this->lang->line('financiamiento_home')['asi']; ?>
					</p>
				</div>
			</div>

			<div class="column column-block">
				<div class="texto">
					<img src="<?= site_url('assets/img/logos/fide.png');?>" alt="">
					<p>
						<?= $this->lang->line('financiamiento_home')['fide']; ?>
					</p>
				</div>
			</div>

			<div class="column column-block">
				<div class="texto">
					<img src="<?= site_url('assets/img/logos/ci-banco.png');?>" alt="">
					<p>
						<?= $this->lang->line('financiamiento_home')['ci_banco']; ?>
					</p>
				</div>
			</div>

			<div class="column column-block">
				<div class="texto">
					<img src="<?= site_url('assets/img/logos/infonavit.png');?>" alt="">
					<p>
						<?= $this->lang->line('financiamiento_home')['infonavit']; ?>
					</p>
				</div>
			</div>
		</div>
		<div class="medium-24 columns text-center" data-magellan>
			<a href="<?= site_url('financiamiento');?>" class="button success btn-financiamiento btn">
				<span><?= $this->lang->line('financiamiento_home')['btn_financiamiento']; ?></span>
			</a>
		</div>
	</div>
</div>

<div class="row expanded home">
	<div class="row consumo-basico">
		<h1 class="titulo">
			<?= $this->lang->line('consumo_basico')['titulo']; ?>
		</h1>
		<div class="row small-up-1 medium-up-3 large-up-3 list-consumo">
			<div class="column column-block tilt2" >
				<div class="texto text-center">
					<div class="img">
						<?= file_get_contents('assets/img/iconos/conoce-tu-consumo/fridge.svg');?>
					</div>
					<p>
						<?= $this->lang->line('consumo_basico')['refrigerador']; ?>
					</p>
				</div>
			</div>

			<div class="column column-block tilt2">
				<div class="texto text-center">
					<div class="img">
						<?= file_get_contents('assets/img/iconos/conoce-tu-consumo/tv.svg');?>
					</div>
					<p>
						<?= $this->lang->line('consumo_basico')['television']; ?>
					</p>
				</div>
			</div>

			<div class="column column-block tilt2">
				<div class="texto text-center">
					<div class="img">
						<?= file_get_contents('assets/img/iconos/conoce-tu-consumo/lamp.svg');?>
					</div>
					<p>
						<?= $this->lang->line('consumo_basico')['iluminacion']; ?>
					</p>
				</div>
			</div>

			<div class="column column-block tilt2">
				<div class="texto text-center">
					<div class="img">
						<?= file_get_contents('assets/img/iconos/conoce-tu-consumo/washing_machine.svg');?>
					</div>
					<p>
						<?= $this->lang->line('consumo_basico')['lavadora']; ?>
					</p>
				</div>
			</div>

			<div class="column column-block tilt2">
				<div class="texto text-center">
					<div class="img">
						<?= file_get_contents('assets/img/iconos/conoce-tu-consumo/laptop.svg');?>
					</div>
					<p>
						<?= $this->lang->line('consumo_basico')['laptop']; ?>
					</p>
				</div>
			</div>

			<div class="column column-block tilt2">
				<div class="texto text-center">
					<div class="img">
						<?= file_get_contents('assets/img/iconos/conoce-tu-consumo/air_conditioning.svg');?>
					</div>
					<p>
						<?= $this->lang->line('consumo_basico')['aire']; ?>
					</p>
				</div>
			</div>
		</div>
		<div class="medium-24 columns text-center" data-magellan>
			<a href="#modalContacto" class="button success btn-consumo btn">
				<span><?= $this->lang->line('consumo_basico')['btn_consumo']; ?></span>
			</a>
		</div>
	</div>
</div>

<div class="row expanded banner invierte">
	<div class="img">
		<img src="<?= site_url('assets/img/banner-invertir.jpg');?>" alt="">
		<div class="texto full">
			<div class="centrado">
				<h1 class="titulo-invierte">
					<?= $this->lang->line('banner_invierte')['titulo']; ?>
				</h1>
			</div>
			<div class="texto-banner centrado" data-magellan>
				<p>
					<?= $this->lang->line('banner_invierte')['texto']; ?>
				</p>
				<a href="#modalContacto" class="button success btn-invierte btn"><?= $this->lang->line('banner_invierte')['btn_banner']; ?></a>
			</div>
		</div>
	</div>
</div>


<div class="row expanded home">
	<div class="casos-exito">
		<div class="texto" data-magellan>
			<h1 class="titulo">
				<?= $this->lang->line('casos_exito_home')['titulo']; ?>
			</h1>
			<a href="<?= site_url('casos-exito');?>" class="button success btn-exito btn">
				<span><?= $this->lang->line('casos_exito_home')['btn_caso_exito']; ?></span>
			</a>
		</div>
		<div class="slide">
			<div class="slider-casos">
				<div class="item">
					<div class="img">
						<img src="<?= site_url('assets/img/fondo-casos-exito.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/prueba.jpg');?>');" alt="">
					</div>
					<div class="texto">
						<h3 class="titulo-item">
							Casa Nuevo Yucatan
						</h3>
						<span>Cancún, Q. Roo</span>
						<div class="cont-info">
							<img src="<?= site_url('assets/img/Cuadrado.svg?v=2');?>" alt=""><p>Capacidad instalada: 4.55Wp</p>
							<img src="<?= site_url('assets/img/Cuadrado.svg?v=2');?>" alt=""><p >Producción anual: 7,368 kWh</p>
						</div>
						
					</div>
				</div>
				<div class="item">
					<div class="img">
						<img src="<?= site_url('assets/img/fondo-casos-exito.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/prueba-caso-exito.png');?>');" alt="">
					</div>
					<div class="texto">
						<h3 class="titulo-item">
							Casa Nuevo Yucatan
						</h3>
						<span>Cancún, Q. Roo</span>
						<div class="cont-info">
							<img src="<?= site_url('assets/img/Cuadrado.svg');?>" alt=""><p>Capacidad instalada: 4.55Wp</p>
							<img src="<?= site_url('assets/img/Cuadrado.svg');?>" alt=""><p >Producción anual: 7,368 kWh</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="img">
						<img src="<?= site_url('assets/img/fondo-casos-exito.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/prueba.jpg');?>');" alt="">
					</div>
					<div class="texto">
						<h3 class="titulo-item">
							Casa Nuevo Mexico
						</h3>
						<span>Cancún, Q. Roo</span>
						<div class="cont-info">
							<img src="<?= site_url('assets/img/Cuadrado.svg');?>" alt=""><p>Capacidad instalada: 4.55Wp</p>
							<img src="<?= site_url('assets/img/Cuadrado.svg');?>" alt=""><p >Producción anual: 7,368 kWh</p>
						</div>
						
					</div>
				</div>
				<div class="item">
					<div class="img">
						<img src="<?= site_url('assets/img/fondo-casos-exito.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/prueba-caso-exito.png');?>');" alt="">
					</div>
					<div class="texto">
						<h3 class="titulo-item">
							Casa Nuevo Mexico
						</h3>
						<span>Cancún, Q. Roo</span>
						<div class="cont-info">
							<img src="<?= site_url('assets/img/Cuadrado.svg');?>" alt=""><p>Capacidad instalada: 4.55Wp</p>
							<img src="<?= site_url('assets/img/Cuadrado.svg');?>" alt=""><p >Producción anual: 7,368 kWh</p>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="row expanded home">
	<div class="row decide-invertir">
		<div class="medium-12 columns">
			<h1 class="titulo">
				<?= $this->lang->line('decide_invertir')['titulo']; ?>
			</h1>
		</div>
		<div class="medium-12 columns">
			<ul class="menu vertical">
				<li>
					<img src="<?= site_url('assets/img/menu.svg');?>" alt="">
					<?= $this->lang->line('decide_invertir')['soporte']; ?>
				</li>
				<li>
					<img src="<?= site_url('assets/img/menu.svg');?>" alt="">
					<?= $this->lang->line('decide_invertir')['deducible']; ?>
				</li>
				<li>
					<img src="<?= site_url('assets/img/menu.svg');?>" alt="">
					<?= $this->lang->line('decide_invertir')['cfe']; ?>
				</li>
			</ul>
		</div>
	</div>
</div>

<?php if(count($testimonios) > 0):?>
<div class="row expanded home">
	<div class="row testimonios">
		<h1 class="titulo"><?= $this->lang->line('testimonios')['titulo']; ?></h1>
		<div class="row texto-testimonio">
			<div>
				<?php foreach ($testimonios as $testimonio): ?>
				<p>
				<?= ($this->session->userdata("language") == 'spanish') ? $testimonio["descripcion_testimonio"] : $testimonio["descripcion_ingles_testimonio"];?>
				</p>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="imagenes">
			<div class="img-testimonios">
				<?php foreach ($testimonios as $testimonio): ?>
				<div class="img">
					<img src="<?= site_url('assets/img/fondo-perfil-testimonio.png');?>" style="background-image: url('<?= site_url($testimonio["full_route_img"]);?>');" alt="<?= $testimonio["cliente_testimonio"] ?>">
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>

<script>
	$(document)
		.on('ready', function(){
			// $('.slider-casos').slick({
			// 	dots: false,
			// 	infinite: true,
			// 	speed: 500,
			// 	fade: true,
			// 	autoplay: true,
  			// 	autoplaySpeed: 5000,
			// });

			$('.texto-testimonio').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				asNavFor: '.img-testimonios'
			});

			$('.img-testimonios').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?= site_url('assets/img/left.png');?>" alt=""></button>',
                nextArrow: '<button type="button" class="slick-next"><img src="<?= site_url('assets/img/right.png');?>" alt=""></button>',
				slidesToShow: 1,
				slidesToScroll: 1,
				asNavFor: '.texto-testimonio',
				dots: false,
				centerMode: false,
				focusOnSelect: false
			});
		});
</script>
<?php endif; ?>

<div class="row expanded banner colaboracion">
	<div class="row certificaciones">
		<ul class="menu">
			<li class="texto">
				<a><?= $this->lang->line('colaboracion')['titulo']; ?></a>
			</li>
			<li>
				<a class="img">
					<img src="<?= site_url('assets/img/logos/banner/CMIC.png');?>" alt="">
				</a>
			</li>
			<li>
				<a class="img">
					<img src="<?= site_url('assets/img/logos/banner/EC0586.01.png');?>" alt="">
				</a>
			</li>
			<li>
				<a class="img">
					<img src="<?= site_url('assets/img/logos/banner/ICIC.png');?>" alt="">
				</a>
			</li>
			<li>
				<a class="img">
					<img src="<?= site_url('assets/img/logos/banner/innotec.png');?>" alt="">
				</a>
			</li>
		</ul>
	</div>
	<div class="img contimagen">
		<img class="imagen-colaboracion" src="<?= site_url('assets/img/banner-colaboracion.jpg');?>" alt="">
		<div class="texto full">
			<div class="colaboraciones">
				<h1 class="titulo-colaboracion">
					<?= $this->lang->line('colaboracion')['titulo_colaboracion']; ?>
				</h1>
				<div class="menu-banner">
					<ul class="menu expanded">
						<li>
							<a>
								<img src="<?= site_url('assets/img/logos/banner/ris.png');?>" alt="">
							</a>
						</li>
						<li>
							<a>
								<img src="<?= site_url('assets/img/logos/banner/solaredge.png');?>" alt="">
							</a>
						</li>
						<li>
							<a>
								<img src="<?= site_url('assets/img/logos/banner/perlight_solar.png');?>" alt="">
							</a>
						</li>
						<li>
							<a>
								<img src="<?= site_url('assets/img/logos/banner/trina_solar.png');?>" alt="">
							</a>
						</li>
						<li>
							<a>
								<img src="<?= site_url('assets/img/logos/banner/tigo.png');?>" alt="">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="row expanded forma-pago">
		<ul class="menu expanded">
			<li>
				<a class="texto"><?= $this->lang->line('colaboracion')['forma_pago']; ?></a>
			</li>
			<li>
				<a>
					<img src="<?= site_url('assets/img/iconos/pago/visa.svg');?>" alt="">
				</a>
			</li>
			<li>
				<a>
					<img src="<?= site_url('assets/img/iconos/pago/mastercard.svg');?>" alt="">
				</a>
			</li>
			<li>
				<a>
					<img src="<?= site_url('assets/img/iconos/pago/american_express.svg');?>" alt="">
				</a>
			</li>
			<li>
				<a>
					<img src="<?= site_url('assets/img/iconos/pago/fide.png');?>" alt="">
				</a>
			</li>
			<li>
				<a>
					<img src="<?= site_url('assets/img/iconos/pago/infonavit.png');?>" alt="">
				</a>
			</li>
		</ul>
	</div>
</div>

<?php if($datito == 1): ?>
<script>
	$( document ).ready(function() {
		$("html,body").animate({scrollTop:($("#beneficios").offset().top - 50)}, 1000, 'swing');
	});
</script>
<?php endif; ?>
