<?php
	date_default_timezone_set('Mexico/General');
	$date = new DateTime('NOW');
	$dateTime = $date->format('Y');
?>

<div class="row expanded banner-contacto" style="background-image: url('<?= site_url('assets/banners/banner-contacto.jpg');?>');">
    <div class="filtro"></div>
    <div class="texto">
        <?= $this->lang->line('contacto')['titulo']; ?>
    </div>
</div>

<div class="row expanded formulario">
    <div class="cotizacion">
        <div class="titulo">
            <h2><?= $this->lang->line('contacto')['subtitulo']; ?></h2>
        </div>    

        <form action="correo-contacto" class="formul" method="POST">
            <div class="fila campos">
                <label>
                    <input type="text" placeholder="<?= $this->lang->line('contacto')['form']['nombre']; ?>" name="nombre" required>
                </label>
                <label>
                    <input type="email" placeholder="<?= $this->lang->line('contacto')['form']['email']; ?>" name="correo" required>
                </label>
                <label>
                    <input type="tel" placeholder="<?= $this->lang->line('contacto')['form']['telefono']; ?>" name="telefono" required>
                </label>
                <label>
                    <input type="text" placeholder="<?= $this->lang->line('contacto')['form']['cuanto_paga']; ?>" name="pago-bimestre" required>
                </label>
                <label class="two-columns">
                    <select name="residencia" required>
                        <option disabled selected value=""><?= $this->lang->line('contacto')['form']['residencia']['vacio']; ?></option>
                        <option value="residencia"><?= $this->lang->line('contacto')['form']['residencia']['residencia']; ?></option>
                        <option value="negocio"><?= $this->lang->line('contacto')['form']['residencia']['negocio']; ?></option>
                    </select>
                </label>
                <label class="two-columns">
                    <textarea name="mensaje" required placeholder="<?= $this->lang->line('contacto')['form']['mensaje']; ?>" id="" rows="4"></textarea>
                </label>
            </div>	
  
            <div class="fila boton">
                <button type="submit" class="button success btn-enviar btn">
                    <span><?= $this->lang->line('contacto')['form']['enviar']; ?></span>
                </button>
            </div>
        </form>  

        <div class="info-contacto">
            <div class="oficinas">
                <div class="img">
                    <img src="<?= site_url('assets/img/iconos/contacto/phone.svg');?>" alt="">
                </div>
                <div class="texto">
                    <span><?= $this->lang->line('footer')['oficina']; ?></span>
                    <ul class="menu vertical">
                        <li>
                            <a href="tel:+529999812876">Tel: (999) 9 81 28 76</a>
                        </li>
                        <li>
                            <a href="tel:+529993573355">Cel: 999 357 3355</a>
                        </li>
                        <li>
                            <a href="tel:+529999009869">Cel: 9999 009869</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="direccion">
                <div class="img home">
                    <img src="<?= site_url('assets/img/iconos/contacto/home.svg');?>" alt="">
                </div>
                <div class="texto">
                    <span>
                        <?= $this->lang->line('footer')['calle']; ?> 49ᴬ 415,Francisco de Montejo II, <br>
                        Mérida, Yuc., Francisco de Montejo
                    </span>
                </div>
            </div>
        </div>

        <div class="row info-copy">
		    <p>© Copyright <?= $dateTime ?> <?= $this->lang->line('footer')['aviso']; ?></p>
	    </div>
    </div>
    <div class="patterns">
        <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" >
            <defs>
                <style>.cls-1{fill:#ec5125;}</style>d
                <pattern id="polka" x="0" y="0" width="500" height="500" patternUnits="userSpaceOnUse" patternTransform="scale(0.075 0.075)">
                    
                    <path class="cls-1" d="M101.12,285.15c22-42,39.34-75.14,127-63.17,42.11,5.72,72.76,2,95.85-7.18,40.41-16.08,57.66-49,72.55-77.48a21.3,21.3,0,0,0-8.61-28.42A20.28,20.28,0,0,0,360,117.25c-22,42-39.33,75.13-127,63.18-115.82-15.77-145,39.91-168.39,84.65a21.31,21.31,0,0,0,8.62,28.42,20.28,20.28,0,0,0,27.87-8.35Z"/><path class="cls-1" d="M398.88,214.85c-22,42-39.33,75.13-127,63.18-115.81-15.78-145,39.9-168.38,84.64a21.3,21.3,0,0,0,8.61,28.42A20.27,20.27,0,0,0,140,382.75c22-42,39.35-75.14,127-63.18,42.11,5.72,72.76,2,95.86-7.17,40.4-16.08,57.65-49,72.55-77.46a21.33,21.33,0,0,0-8.62-28.44,20.27,20.27,0,0,0-27.87,8.35Z"/>
                    
                </pattern>
            </defs>
            
            <rect x="0" y="0" width="100%" height="100%" fill="url(#polka)"></rect>
        </svg>
    </div>

</div>

<script>
    $(document).ready(quitarStick);

    function quitarStick() {
        let header = $("#stick");
        $(header).addClass("activo");
    }
</script>    