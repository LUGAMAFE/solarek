<!-- Emails use the XHTML Strict doctype -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- The character set should be utf-8 -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <!-- Link to the email's CSS, which will be inlined into the email -->
    <?php echo link_tag(array('href' => 'assets/img/icon.png', 'rel' => 'shortcut icon')); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <?php $this->load->view('site/template/correo_include'); ?>
  <!-- Wrapper for the body of the email -->
    <table align="center" class="container">
        <tbody>
            <tr>
               <td class="float-center" align="center" valign="top">
                    <table class="row collapse">
                        <tbody>
                            <tr>
                                <th class="small-12 large-12 columns">
                                    <table>
                                        <tr>
                                            <th class="logo">
                                                <img src="<?php echo site_url('assets/img/logo.png');?>" alt="" style="width: 200px;">
                                            </th>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                    <table class="row">
                        <tbody>
                            <tr>
                                <th class="small-12 large-12 columns">
                                    <table>
                                        <tr>
                                            <td class="info-reserva">
                                                <h4 class="titulo">
                                                    Correo de contacto
                                                </h4>
                                                <?php if(isset($nombre)): ?>
                                                <p>
                                                    Nombre: <b><?= $nombre ?></b> 
                                                </p>
                                                <?php endif; ?>
                                                
                                                <?php if(isset($correo)): ?>
                                                <p>
                                                    Correo: <b><?= $correo ?></b> 
                                                </p>
                                                <?php endif; ?>

                                                <?php if(isset($telefono)): ?>
                                                <p>
                                                    Telefono: <b><?= $telefono ?></b> 
                                                </p>
                                                <?php endif; ?>

                                                <?php if(isset($pago)): ?>
                                                <p>
                                                    Pago: <b><?= $pago ?></b> 
                                                </p>
                                                <?php endif; ?>

                                                <?php if(isset($residencia)): ?>
                                                <p>
                                                    Residencia: <b><?= $residencia ?></b> 
                                                </p>
                                                <?php endif; ?>

                                                <?php if(isset($mensaje)): ?>
                                                <p>
                                                    Mensaje: <b><?= $mensaje ?></b> 
                                                </p>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <!-- <tr>
                                            <th class="gracias">
                                                <p>
                                                    Correo de contacto
                                                </p>
                                            </th>
                                        </tr> -->
                                    </table>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </td> 
            </tr>
            <tr>
                <td>
                    <table class="row" id="footer">
                        <tbody>
                            <tr>
                                <th class="small-12 large-8 columns first">
                                    <table>
                                        <tr>
                                            <th>
                                                <img src="<?php echo site_url('assets/img/logo.png');?>" id="logo-footer" style="width: 200px;">
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>
                                                <p id="derechos"><span>© <?= $this->config->item('name_site') ?> |</span> TODOS LOS DERECHOS RESERVADOS</p>
                                            </th>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>