<?= doctype('html5'); ?> 
<html class="no-js" lang="es" itemscope itemtype="https://schema.org/LocalBusiness" ng-app="myApp"> 		<!--<![endif]-->
	<head <?php if(isset($prefix)) { echo $prefix; } ?>>
		<?= meta('charset', 'utf-8'); ?>
		<?= meta('viewport', 'width=device-width, initial-scale=1.0, user-scalable=no'); ?>
		<?= meta('x-ua-compatible', 'ie=edge', 'equiv'); ?>
		
		<meta property="og:title" content="<?= isset($description) ?  $description : "Expertos en energia solar" ;?>">
		<meta property="og:type" content="website">
		<meta property="og:image" content="<?= isset($imagePrev) ?  $imagePrev : site_url("assets/img/logo.png") ;?>">
		<div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v4.0"></script>
		
		<meta name="description" content="Expertos en energia solar">
   		<meta name="author" content="gant.mx">
   		<base href="<?= base_url();?>">
		<title><?= $this->config->item('name_site');?> - <?= $title ?></title>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
		<?= link_tag(array('href' => 'assets/img/icon.png', 'rel' => 'shortcut icon')); ?>
		<?= link_tag('assets/css/app.css?v=11'); ?>
		<script src="<?= site_url('assets/js/modernizr.js'); ?>"></script>
        <?= link_tag('assets/plugins/jquerymodal/jquery.modal.min.css'); ?>
		<?= $_styles ?>
		<!--[if lt IE 9]>
		<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
		<script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
		<script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
		<![endif]-->
		<script src="<?= site_url('assets/js/jquery.min.js')?>" type="text/javascript" charset="utf-8"></script>
		<script> const siteURL = "<?= current_url();?>/", 
                    baseURL = "<?= base_url();?>"; </script>
        <script src="<?= site_url('assets/plugins/jquerymodal/jquery.modal.min.js'); ?>"></script>
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            
            fbq('init', '381266799217639');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1"
            src="https://www.facebook.com/tr?id=381266799217639&ev=PageView
            &noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

	</head>
	<body>
		<div class="off-canvas-wrapper">
			<div class="off-canvas position-left" id="offCanvas" data-off-canvas>
				<!-- Your menu or Off-canvas content goes here -->
			</div>
			<?php //echo $this->load->view('site/partials/angular', '', true);?>
			<div class="off-canvas-content" data-off-canvas-content>
				<?= $navbar; ?>	
				<?= $banner; ?>		
				<?= $content; ?>
				<?= $footer; ?>
			</div>
		</div>	

		 <!-- Modal HTML embedded directly into document -->
		 <div id="modalContacto" class="modal" style="display:none;">
            <a href="#" rel="modal:close">x</a>
            <div class="contenido">
                <div class="formularioModal">
                    <div class="logo">
                        <img src="<?= site_url('assets/img/logo.png');?>" alt="Solárek">
                    </div>
                    <form action="correo-contacto" class="formula" method="POST">
                        <div class="fila campos">
                            <label>
                                <input type="text" placeholder="<?= $this->lang->line('footer')['form']['nombre']; ?>" name="nombre" required>
                            </label>
                            <label>
                                <input type="email" placeholder="<?= $this->lang->line('footer')['form']['email']; ?>" name="correo" required>
                            </label>
                            <label>
                                <input type="text" placeholder="<?= $this->lang->line('footer')['form']['telefono']; ?>" name="telefono" required>
                            </label>
                            <label>
                                <input type="text" placeholder="<?= $this->lang->line('footer')['form']['cuanto_paga']; ?>" name="pago-bimestre" required>
                            </label>
                            <label class="two-columns">
                                <textarea placeholder="<?= $this->lang->line('footer')['form']['mensaje']; ?>" id="" rows="4" name="mensaje" required></textarea>
                            </label>
                        </div>	
            
                        <div class="fila boton">
                            <button type="submit" class="button success btn-enviar btn">
                                <span><?= $this->lang->line('footer')['form']['enviar']; ?></span>
                            </button>
                        </div>
                    </form>  
                </div>
                <div class="derecha">
                    <div class="patterns">
                        <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" >
                            <defs>
                                <style>.cls-1{fill:#ec5125;}</style>
                                <pattern id="polka-dots" x="0" y="0" width="500" height="500" patternUnits="userSpaceOnUse" patternTransform="scale(0.05 0.05)">
                                    
                                    <path class="cls-1" d="M101.12,285.15c22-42,39.34-75.14,127-63.17,42.11,5.72,72.76,2,95.85-7.18,40.41-16.08,57.66-49,72.55-77.48a21.3,21.3,0,0,0-8.61-28.42A20.28,20.28,0,0,0,360,117.25c-22,42-39.33,75.13-127,63.18-115.82-15.77-145,39.91-168.39,84.65a21.31,21.31,0,0,0,8.62,28.42,20.28,20.28,0,0,0,27.87-8.35Z"/><path class="cls-1" d="M398.88,214.85c-22,42-39.33,75.13-127,63.18-115.81-15.78-145,39.9-168.38,84.64a21.3,21.3,0,0,0,8.61,28.42A20.27,20.27,0,0,0,140,382.75c22-42,39.35-75.14,127-63.18,42.11,5.72,72.76,2,95.86-7.17,40.4-16.08,57.65-49,72.55-77.46a21.33,21.33,0,0,0-8.62-28.44,20.27,20.27,0,0,0-27.87,8.35Z"/>
                                    
                                </pattern>
                            </defs>
                            
                            <rect x="0" y="0" width="100%" height="100%" fill="url(#polka-dots)"></rect>
                        </svg>
                    </div>
                    <div class="info-contacto">
                        <div class="oficinas">
                            <div class="img">
                                <img src="<?= site_url('assets/img/iconos/contacto/phone.svg');?>" alt="">
                            </div>
                            <div class="texto">
                                <span><?= $this->lang->line('footer')['oficina']; ?></span>
                                <ul class="menu vertical">
                                    <li>
                                        <a href="tel:+529999812876">Tel: (999) 9 81 28 76</a>
                                    </li>
                                    <li>
                                        <a href="tel:+529993573355">Cel: 999 357 3355</a>
                                    </li>
                                    <li>
                                        <a href="tel:+529999009869">Cel: 9999 009869</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="direccion">
                            <div class="img home">
                                <img src="<?= site_url('assets/img/iconos/contacto/home.svg');?>" alt="">
                            </div>
                            <div class="texto">
                                <span>
                                    <?= $this->lang->line('footer')['calle']; ?> 49ᴬ 415,Francisco de Montejo II, <br>
                                    Mérida, Yuc., Francisco de Montejo
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<?= $_scripts; ?>
		<script src="<?= site_url('assets/js/utilities.js')?>" type="text/javascript" charset="utf-8"></script>
		<script src="<?= site_url('assets/js/foundation.js')?>" type="text/javascript" charset="utf-8"></script>
		<script src="<?= site_url('assets/js/what-input.js')?>" type="text/javascript" charset="utf-8"></script>
		<script src="<?= site_url('assets/js/app.js')?>" type="text/javascript" charset="utf-8"></script>
        <script src="<?= site_url('assets/js/modalContacto.js')?>" type="text/javascript" charset="utf-8"></script>
        <script src="<?= site_url('assets/js/svg.js')?>" type="text/javascript" charset="utf-8"></script>
	</body>
</html>
