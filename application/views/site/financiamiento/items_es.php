<div class="financiamiento-empresa left-empresa row">
    <div class="texto" data-magellan>
        <div class="principal">
            <h4 class="sub-titulo">FIDEICOMISO PARA EL AHORRO DE ENERGÍA
            ELÉCTRICA EN CASAS CON TARIFA RESIDENCIAL.</h4>
            <p class="cont">Si eres dueño de una propiedad tienes la oportunidad
            de acceder a este crédito, con cargo en tu recibo de luz.</p>
            <p class="cont">El monto máximo para otorgar en este programa
            es de $350,000 mil pesos</p>
        </div>
        <div class="secundario">
            <p class="sub-titulo">REQUISITOS GENERALES</p>
            <ul class="list">
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Identificación oficial con fotografía</p> 
                </div> 
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Ser propietario de la vivienda, o tener el crédito hipotecario a tu nombre.</p> 
                </div> 
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Antigüedad mínima de 3 años viviendo en la propiedad.</p> 
                </div> 
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Dos referencias personales</p> 
				</div> 
            </ul>
        </div>
        <a href="#modalContacto" class="contacta btn">CONTACTAR A UN ASESOR</a>
    </div>
    <div class=" imagen">
        <img src=<?= site_url("assets/img/logos/asi-programa-ahorro-sistematico-integral.png")?>>
    </div>
</div>
<div class="financiamiento-empresa right-empresa row">
    <div class=" imagen">
        <img src=<?= site_url("assets/img/logos/fide.png")?>>
    </div>
    <div class=" texto" data-magellan>
        <div class="principal">
            <h4 class="sub-titulo">FIDEICOMISO PARA EL AHORRO DE ENERGIA
            ELÉCTRICA</h4>
            <p class="cont">Si eres una empresa o persona física con actividad
            empresarial en tarifas 2,3 u OM, el FIDE te ofrece
            financiamiento.</p>
        </div>
        <div class="secundario">
            <p class="sub-titulo">REQUISITOS GENERALES</p>
            <ul class="list">
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Subsidio que va del 10 al 14%</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Financian el resto del proyecto con plazos hasta los 4 años.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Cobro de mensualidad en tu recibo de luz.</p> 
                </div> 
            </ul>
        </div>
        <a href="#modalContacto" class="contacta btn">CONTACTAR A UN ASESOR</a>
    </div>
</div>
<div class="financiamiento-empresa left-empresa row">
    <div class=" texto" data-magellan>
        <div class="principal">
            <h4 class="sub-titulo">CIBANCO PANEL</h4>
            <p class="cont">Es el crédito destinado para la adquisición de
            paneles solares para Negocio Propio (No aplica
            en consorcios) o para casa habitación propia, con
            un crédito hipotecario a su nombre, o de algún
            familiar en línea directa, con Tarifa Doméstica de
            Alto Consumo (DAC) o 02.</p>
        </div>
        <div class="secundario">
            <p class="sub-titulo">CARACTERISTICAS</p>
            <ul class="list">
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Elija el plazo que se ajuste a sus necesidades
                    de hasta 84 meses.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Atractiva tasa de interés anual fija desde 17.00%.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Comisión por apertura de 2.00% más IVA,
                sobre el monto a financiar</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Enganche desde 15% del valor total de la factura.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Podrá realizar pagos anticipados sin penalización.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Monto mínimo de crédito de $60,000 pesos
                y máximo de $500,000.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Condiciones sujetas a la aprobación de crédito.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Condiciones sujetas a cambios sin previo aviso.</p> 
                </div>	
            </ul>
        </div>
        <a href="#modalContacto" class="contacta btn">CONTACTAR A UN ASESOR</a>
    </div>
    <div class=" imagen">
        <img src=<?= site_url("assets/img/logos/ci-banco.png")?>>
    </div>
</div>
<div class="financiamiento-empresa right-empresa row">
    <div class=" imagen" >
        <img src=<?= site_url("assets/img/logos/infonavit.png")?>>
    </div>
    <div class=" texto" data-magellan>
        <div class="principal">
            <h4 class="sub-titulo">INFONAVIT</h4>
            <p class="cont">Utiliza el saldo en tu Subcuenta de Vivienda del
            INFONAVIT para tu proyecto de Energía Solar y
            ahorra sin invertir.</p>

            <p class="cont"> El saldo de la Subcuenta de Vivienda es tu dinero,
            es el resultado de las aportaciones que tu patrón
            hace a tu nombre en el Infonavit y la mejor manera
            de aprovechar esos recursos es a través del
            programa que el Infonavit creó para impulsar el
            uso de energía solar.</p>
        </div>
        <a href="#modalContacto" class="contacta btn">CONTACTAR A UN ASESOR</a>
    </div>
</div>