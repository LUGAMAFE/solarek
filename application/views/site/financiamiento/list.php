<div class="row expanded home">
	<div class="row financiamiento">
		<h1 class="titulo">
			<?= $this->lang->line('financiamiento')['titulo']; ?>
		</h1>
    </div>
    <div class="row empresas">
        <?php 
            if($this->session->userdata('language') == 'english') {
                echo $this->load->view('site/financiamiento/items_en', '', true);
            } else {
                echo $this->load->view('site/financiamiento/items_es', '', true);
            }
        ?>
    </div>
</div>