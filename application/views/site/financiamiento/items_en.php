<div class="financiamiento-empresa left-empresa row">
    <div class="texto" data-magellan>
        <div class="principal">
            <h4 class="sub-titulo">FINANCING FOR HOMES AND BUSINESS COMPANIES</h4>
            <p class="cont">Escrow for the saving of electrical energy in houses with residential rate. If you own a property you have the opportunity of accesing to this credit with charge to your light bill. </p>
            <p class="cont">The max amount to grant in this financing program is $350,000 M.X.N.</p>
        </div>
        <div class="secundario">
            <p class="sub-titulo">GENERAL REQUIREMENTS</p>
            <ul class="list">
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Official ID with photo.</p> 
                </div> 
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Being an ownership home or have the mortgage credit at your name.</p> 
                </div> 
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Minimum of 3 years living in the property.</p> 
                </div> 
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Two personal references.</p> 
				</div> 
            </ul>
        </div>
        <a href="#modalContacto" class="contacta btn">Contact an energy advisor</a>
    </div>
    <div class=" imagen">
        <img src=<?= site_url("assets/img/logos/asi-programa-ahorro-sistematico-integral.png")?>>
    </div>
</div>
<div class="financiamiento-empresa right-empresa row">
    <div class=" imagen">
        <img src=<?= site_url("assets/img/logos/fide.png")?>>
    </div>
    <div class=" texto" data-magellan>
        <div class="principal">
            <h4 class="sub-titulo">ESCROW FOR THE SAVING OF ELECTRICAL ENERGY</h4>
            <p class="cont">If you have a company or you are a person with business activities in rates 2.3 u OM, the FIDE offers you finincing programms.</p>
        </div>
        <div class="secundario">
            <p class="sub-titulo">ADVANTAGES</p>
            <ul class="list">
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Subsudy rangin from 10% to 14%.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>They finance the rest of the project with terms up to 4 years.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Monthly payment on your electricity bill. </p> 
                </div> 
            </ul>
        </div>
        <a href="#modalContacto" class="contacta btn">Contact an energy advisor</a>
    </div>
</div>
<div class="financiamiento-empresa left-empresa row">
    <div class=" texto" data-magellan>
        <div class="principal">
            <h4 class="sub-titulo">CI BANCO</h4>
            <p class="cont">It’s the credit for the acquisition of solar panels for own business (It does not apply in consortiums)  or for particular homes with a mortgage credit at your name or of a family member in direct line with domestic rate of high consumption DAC or 02.</p>
        </div>
        <div class="secundario">
            <p class="sub-titulo">CHARACTERISTICS</p>
            <ul class="list">
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Choose the terms that adjusts your needs for up 84 months.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Attractive annual interest rate fired from 17%.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Commision for account opening of 2% plus VAT on the amount to be financed.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Engagement 15% of the total invoice value.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>You could make advance payments without penalty.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Minimun credit amount of $60,000 M.X.N and maximum of $500,000 M.X.N</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Terms and conditions subject to creddit aproval.</p> 
                </div>
                <div class="li">
                    <img class="img" src="<?= site_url('assets/img/iconos/financiamiento/list.svg');?>" alt="">
                    <p>Conditions subject to changes without previous notice.</p> 
                </div>	
            </ul>
        </div>
        <a href="#modalContacto" class="contacta btn">Contact an energy advisor</a>
    </div>
    <div class=" imagen">
        <img src=<?= site_url("assets/img/logos/ci-banco.png")?>>
    </div>
</div>
<div class="financiamiento-empresa right-empresa row">
    <div class=" imagen" >
        <img src=<?= site_url("assets/img/logos/infonavit.png")?>>
    </div>
    <div class=" texto" data-magellan>
        <div class="principal">
            <h4 class="sub-titulo">INFONAVIT</h4>
            <p class="cont">Use the balance in your INFONAVIT housing sub-account for your solar energy project and start saving without investing.</p>

            <p class="cont">The balance of the housing sub-account is your money, it is the result of the contributions that your employer makes at your name in the INFONAVIT and the best way to take advantage of these resources is through the program that INFONAVIT created to boost the use of solar energy.</p>
        </div>
        <a href="#modalContacto" class="contacta btn">Contact an energy advisor</a>
    </div>
</div>