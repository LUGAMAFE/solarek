<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-warning navbar-light border-bottom ">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= site_url("administracion") ?>" class="nav-link">Home</a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="<¡= site_url("administracion/blog") ?>" class="nav-link">Blog</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<¡= site_url("administracion/casosExito") ?>" class="nav-link">Casos Exito</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<¡= site_url("administracion/descargables") ?>" class="nav-link">Descargables</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<¡= site_url("administracion/correos") ?>" class="nav-link">Correos</a>
      </li> -->
    </ul>

    <!-- SEARCH FORM -->
    <!-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> -->

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item">
        <a class="nav-link cerrar-sesion" style="cursor: pointer;">
          <i class="fas fa-sign-out-alt"></i>
          Cerrar Sesión
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
          <i class="fas fa-th-large"></i>
          Personalizar
        </a>
      </li>

    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-warning elevation-4">
    <!-- Brand Logo -->
    <a href="#" target="_blank" class="brand-link navbar-light d-flex justify-content-center align-items-center">
      <img class="" id="logo" src="assets/img/logo.png">
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-center">
        <div class="image d-flex justify-content-center align-items-center">
          <i class="fas fa-user"></i>
        </div>
        <!-- <li class="nav-item">
            <i class="fas fa-user"></i>
        </li> -->
        <div class="info">
          <a class="d-block text-uppercase"><?= $this->session->userdata("username"); ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">Categorias Administracion</li>
          <li class="nav-item">
            <a href="<?= site_url("administracion/banners") ?>" class="nav-link">
            <i class="nav-icon far fa-image"></i>
              <p>Banners</p> 
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url("administracion/testimonios") ?>" class="nav-link">
            <i class="nav-icon fas fa-user-check"></i>
              <p>Testimonios</p> 
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url("administracion/blog") ?>" class="nav-link">
            <i class="nav-icon fas fa-blog"></i>
              <p>Blog</p> 
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url("administracion/casosExito") ?>" class="nav-link">
              <i class="nav-icon fas fa-images"></i>
              <p>Casos Exito</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url("administracion/descargables") ?>" class="nav-link">
              <i class="nav-icon fas fa-file-download"></i>
              <p>Descargables</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url("administracion/correos") ?>" class="nav-link">
              <i class="nav-icon fas fa-envelope"></i>
              <p>Correos</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>