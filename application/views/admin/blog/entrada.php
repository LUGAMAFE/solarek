<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Admin Entrada Blog <i class="nav-icon fas fa-blog"></i>  <?= $accion ?> </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/blog") ?>">Blog Admin</a></li>
            <li class="breadcrumb-item active">Admin Entrada Blog <?= $accion ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-md-11">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Control Blog</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form-entrada" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form"> 
              <div class="card-body">
                <div class="row justify-content-center">

                  <div class="col-12">
                      <div class="form-group">
                          <label>Autor</label>
                          <input type="text" class="form-control" name="autor" placeholder="Autor.." required value="<?= isset($datos_entrada["autor_entrada"]) ? $datos_entrada["autor_entrada"] : ""; ?>">
                      </div>
                      <div class="form-group">
                          <label>Nombre Entrada</label>
                          <input type="text" class="form-control" name="nombre" placeholder="Nombre Entrada.." required autocomplete="off" value="<?= isset($datos_entrada["nombre_entrada"]) ? $datos_entrada["nombre_entrada"] : ""; ?>">
                      </div>
                      <div class="form-group">
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="check-video" class="custom-control-input" id="customCheck1" <?= (isset($datos_entrada["check_video_entrada"]) && $datos_entrada["check_video_entrada"] == 1) ? "checked": ""; ?> >
                            <label class="custom-control-label" for="customCheck1">Activar Video Youtube</label>
                          </div>
                          <input type="url" class="form-control mt-2" name="video-youtube" placeholder="Url Video Youtube.." autocomplete="off" value="<?= isset($datos_entrada["url_youtube_id_entrada"]) ? $datos_entrada["url_youtube_id_entrada"] : ""; ?>">
                      </div>
                  </div>  
                  <div class="col-6">
                    <div class="form-group">
                        <label>Imagen de la entrada Pequeña</label>
                        <div class="upload-area requerido" id="imagen-entrada-small"></div>
                        <input type="hidden" name="imagen-entrada-small-info">
                    </div>
                  </div>  
                  <div class="col-6">
                    <div class="form-group">
                        <label>Imagen de la entrada GRANDE</label>
                        <div class="upload-area requerido" id="imagen-entrada-big"></div>
                        <input type="hidden" name="imagen-entrada-big-info">
                    </div>
                  </div> 

                  <div class="col-12">
                    <div class="form-group ">
                      <label class="text-primary">Idioma a modificar:  <span class="text-danger act-text">Español</span> </label>
                      <ul class="nav nav-tabs" id="tabIdioma">
                        <li class="nav-item">
                          <a id="esp-tab" data-toggle="tab" href="#esp-option" role="tab" aria-controls="esp-option" class="nav-link active bg-light" aria-selected="true" > <i class="text-success mr-1 seleccionado fas fa-check"></i> Español</a>
                        </li>
                        <li class="nav-item">
                          <a id="eng-tab" data-toggle="tab" href="#eng-option" role="tab" aria-controls="eng-option" class="nav-link" aria-selected="false" > <i style="display: none;" class="mr-1 text-success seleccionado fas fa-check"></i> Inglés</a>
                        </li>
                      </ul>
                      <div class="tab-content pt-2 bg-light" id="myTabContent">
                        <div class="tab-pane fade show active" id="esp-option" role="tabpanel" aria-labelledby="esp-option">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Titulo</label>
                                        <input type="text" class="form-control" name="titulo" placeholder="Titulo.." required autocomplete="off" value="<?= isset($datos_entrada["titulo_entrada"]) ? $datos_entrada["titulo_entrada"] : ""; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Subtitulo Pequeño</label>
                                        <input type="text" class="form-control" name="subtitulo" placeholder="Subtitulo.." required autocomplete="off" value="<?= isset($datos_entrada["subtitulo_entrada"]) ? $datos_entrada["subtitulo_entrada"] : ""; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Resumen Informacion</label>
                                        <input type="text" class="form-control" name="resumen" placeholder="Resumen.." required autocomplete="off" value="<?= isset($datos_entrada["resumen_entrada"]) ? $datos_entrada["resumen_entrada"] : ""; ?>">
                                    </div>
              
                                    <div class="form-group bg-light requerido">
                                        <label>Información Entrada</label>
                                        <textarea style="height: 500px; font-family: 'Raleway'; " class="summer espanol" name="informacion" placeholder="Informacion Caso...">
                                          <?= isset($datos_entrada["informacion_entrada"]) ? $datos_entrada["informacion_entrada"] : ""; ?>
                                        </textarea>
                                    </div>
                                </div> 
                            </div> 
                        </div>
                        <div class="tab-pane fade" id="eng-option" role="tabpanel" aria-labelledby="eng-option">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Titulo Inglés</label>
                                        <input type="text" class="form-control" name="titulo-ingles" placeholder="Titulo Inglés.." required autocomplete="off" value="<?= isset($datos_entrada["titulo_ingles_entrada"]) ? $datos_entrada["titulo_ingles_entrada"] : ""; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Subtitulo Pequeño Inglés</label>
                                        <input type="text" class="form-control" name="subtitulo-ingles" placeholder="Subtitulo Inglés.." required autocomplete="off" value="<?= isset($datos_entrada["subtitulo_ingles_entrada"]) ? $datos_entrada["subtitulo_ingles_entrada"] : ""; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Resumen Informacion Inglés</label>
                                        <input type="text" class="form-control" name="resumen-ingles" placeholder="Resumen Inglés.." required autocomplete="off" value="<?= isset($datos_entrada["resumen_ingles_entrada"]) ? $datos_entrada["resumen_ingles_entrada"] : ""; ?>">
                                    </div>
              
                                    <div class="form-group bg-light requerido">
                                        <label>Información Entrada Inglés</label>
                                        <textarea style="height: 500px; font-family: 'Raleway'; " class="summer ingles" name="informacion-ingles" placeholder="Informacion Caso Inglés...">
                                          <?= isset($datos_entrada["informacion_ingles_entrada"]) ? $datos_entrada["informacion_ingles_entrada"] : ""; ?>
                                        </textarea>
                                    </div>
                                </div> 
                            </div> 
                        </div>
                      </div>
                    </div>

                  </div>
                </div>   
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <a href="<?= site_url("administracion/blog") ?>"  class="btn btn-danger btn-cancelar">Cancelar</a>
                <div id="submit-form" class="btn btn-info float-right">Guardar Entrada</div>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>
