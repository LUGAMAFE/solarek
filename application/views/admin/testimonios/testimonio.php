<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Testimonios Admin <i class="nav-icon fas fa-user-check"></i> <?= $accion ?> </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/testimonios") ?>">Testimonios Admin</a></li>
            <li class="breadcrumb-item active">Testimonios Admin <?= $accion ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-md-11">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Control Blog</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form-entrada" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form"> 
              <div class="card-body">

                <div class="row justify-content-center">
                  <div class="col-12">
                      <div class="form-group">
                          <label>Cliente</label>
                          <input type="text" class="form-control" name="cliente" placeholder="Cliente.." required value="<?= isset($datos["cliente_testimonio"]) ? $datos["cliente_testimonio"] : ""; ?>">
                      </div>
   
                      <div class="form-group">
                          <label>Imagen del Testimonio</label>
                          <div class="upload-area requerido" id="img-testimonio"></div>
                          <input type="hidden" name="img-testimonio-info">
                      </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group ">
                      <label class="text-primary">Idioma a modificar: <span class="text-danger act-text">Español</span> </label>
                      <ul class="nav nav-tabs" id="tabIdioma">
                        <li class="nav-item">
                          <a id="esp-tab" data-toggle="tab" href="#esp-option" role="tab" aria-controls="esp-option" class="nav-link active bg-light" aria-selected="true" > <i class="text-success mr-1 seleccionado fas fa-check"></i> Español</a>
                        </li>
                        <li class="nav-item">
                          <a id="eng-tab" data-toggle="tab" href="#eng-option" role="tab" aria-controls="eng-option" class="nav-link" aria-selected="false" > <i style="display: none;" class="mr-1 text-success seleccionado fas fa-check"></i> Inglés</a>
                        </li>
                      </ul>
                      <div class="tab-content pt-2 bg-light" id="myTabContent">
                        <div class="tab-pane fade show active" id="esp-option" role="tabpanel" aria-labelledby="esp-option">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                  <div class="form-group">
                                      <label>Descripción</label>
                                      <textarea class="form-control" rows="3" name="descripcion" placeholder="Descripción Testimonio..." required><?= isset($datos["descripcion_testimonio"]) ? $datos["descripcion_testimonio"] : ""; ?></textarea>
                                  </div>
                                </div> 
                            </div> 
                        </div>
                        <div class="tab-pane fade" id="eng-option" role="tabpanel" aria-labelledby="eng-option">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                  <div class="form-group">
                                      <label>Descripción Inglés</label>
                                      <textarea class="form-control" rows="3" name="descripcion-ingles" placeholder="Descripción Inglés Testimonio..." required><?= isset($datos["descripcion_ingles_testimonio"]) ? $datos["descripcion_ingles_testimonio"] : ""; ?></textarea>
                                  </div>
                                </div> 
                            </div> 
                        </div>
                      </div>
                    </div>

                  </div>
                </div>      
                
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <a href="<?= site_url("administracion/testimonios") ?>"  class="btn btn-danger btn-cancelar">Cancelar</a>
                <div id="submit-form" class="btn btn-info float-right">Guardar Testimonios</div>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>