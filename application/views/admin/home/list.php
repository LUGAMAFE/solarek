<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Dashboard Home</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row d-flex justify-content-center justify-content-md-start">
        <div class="col-10 col-lg-4">
          <!-- small box -->
          <a href="<?= site_url("administracion/banners") ?>" class="small-box bg-secondary">
            <div class="inner">
              <h3>Banners</h3>

              <p>Admin</p>
            </div>
            <div class="icon">
              <i class="far fa-image"></i>
            </div>
            <div class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></div>
          </a>
        </div>
        <div class="col-10 col-lg-4">
          <!-- small box -->
          <a href="<?= site_url("administracion/testimonios") ?>" class="small-box bg-secondary">
            <div class="inner">
              <h3>Testimonios</h3>

              <p>Admin</p>
            </div>
            <div class="icon">
              <i class="fas fa-user-check"></i>
            </div>
            <div class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></div>
          </a>
        </div>
        <div class="col-10 col-lg-4">
          <!-- small box -->
          <a href="<?= site_url("administracion/blog") ?>" class="small-box bg-secondary">
            <div class="inner">
              <h3>Blog</h3>

              <p>Admin</p>
            </div>
            <div class="icon">
              <i class="fas fa-blog"></i>
            </div>
            <div class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></div>
          </a>
        </div>
        <!-- ./col -->
        <div class="col-10 col-lg-4">
          <!-- small box -->
          <a href="<?= site_url("administracion/casosExito") ?>" class="small-box bg-secondary">
            <div class="inner">
              <h3>Casos Exito</h3>

              <p>Admin</p>
            </div>
            <div class="icon">
              <i class="fas fa-images"></i>
            </div>
            <div class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></div>
          </a>
        </div>
        <!-- ./col -->
        <div class="col-10 col-lg-4">
          <!-- small box -->
          <a href="<?= site_url("administracion/descargables") ?>" class="small-box bg-secondary">
            <div class="inner">
              <h3>Descargables</h3>

              <p>Admin</p>
            </div>
            <div class="icon">
              <i class="fas fa-file-download"></i>
            </div>
            <div class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></div>
          </a>
        </div>
        <!-- ./col -->
        <div class="col-10 col-lg-4">
          <!-- small box -->
          <a href="<?= site_url("administracion/correos") ?>" class="small-box bg-secondary">
            <div class="inner">
              <h3>Correos</h3>

              <p>Admin</p>
            </div>
            <div class="icon">
              <i class="fas fa-envelope"></i>
            </div>
            <div class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></div>
          </a>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?php echo $this->load->view('admin/utils/sweetAlerts', '', true); ?>