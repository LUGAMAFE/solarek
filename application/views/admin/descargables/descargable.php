<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Descargable <i class="nav-icon fas fa-file-download"></i> <?= $accion ?> </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/descargables") ?>">Descargables Admin</a></li>
            <li class="breadcrumb-item active">Descargable <?= $accion ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-md-11">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Control Blog</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form-entrada" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form"> 
              <div class="card-body">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Titulo</label>
                            <input type="text" class="form-control" name="titulo" placeholder="Titulo.." required value="<?= isset($datos["titulo_descargable"]) ? $datos["titulo_descargable"] : ""; ?>">
                        </div>
                        <div class="form-group">
                            <label>Descripcion</label>
                            <input type="text" class="form-control" name="descripcion" placeholder="Descripcion.." required value="<?= isset($datos["descripcion_descargable"]) ? $datos["descripcion_descargable"] : ""; ?>">
                        </div>
                        <div class="form-group">
                            <label>Archivo descargable</label>
                            <div class="upload-area requerido" id="archivo-descargable"></div>
                            <input type="hidden" name="archivo-descargable-info">
                        </div>
                    </div>  
                </div>   
                
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <a href="<?= site_url("administracion/descargables") ?>"  class="btn btn-danger btn-cancelar">Cancelar</a>
                <div id="submit-form" class="btn btn-info float-right">Guardar Descargable</div>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>