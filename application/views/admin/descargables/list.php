<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0 text-dark">Descargables Admin <i class="nav-icon fas fa-file-download"></i></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item active">Descargables Admin</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
        <div class="card card-secondary">
          <div class="card-header" style="marginbottom: 1rem;">
            <h3 class="card-title">Tabla Descargables</h3>
          </div>
          <!-- /.card-header -->

          <div class="card-body">
            <div class="row justify-content-end">
              <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-2 d-flex justify-content-end">
                <a href="<?= site_url("administracion/descargables/crear") ?>" class="btn btn-info btn-block btn-md text-white"><i class="fas fa-plus mr-1"></i>Crear Descargable</a>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <table id="tabla" class="table table-bordered table-striped dt-responsive">
                  <thead>
                  <tr>
                    <th>Titulo</th>
                    <th>Lugar</th>
                    <th>Fecha Creación</th>
                    <th>Fecha Modificación</th>
                    <th>Editar</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($descargables as $descargable):?>
                        <tr>  
                            <td> <?= $descargable["id_descargable"];?>                  </td>
                            <td> <?= $descargable["titulo_descargable"];?>              </td>
                            <td> <?= $descargable["fecha_creacion_descargable"];?>      </td>
                            <td> <?= $descargable["fecha_modificacion_descargable"];?>  </td>
                            <td> 
                                <div class="row">
                                  <div class="col-6 d-flex justify-content-center">
                                    <a href="<?= site_url('administracion/descargables/').$descargable["id_descargable"];?>" class="btn btn-info d-flex justify-content-center align-items-center p-1 p-lg-2"><i style="padding-left: 3px;" class="fas fa-edit pr-xl-1"></i> <span class="d-none d-xl-block">Editar</span></a>
                                  </div>
                                  <div class="col-6 d-flex justify-content-center">
                                    <a href="<?= site_url('administracion/descargables/eliminar/').$descargable["id_descargable"];?>" class="btn btn-danger d-flex justify-content-center align-items-center p-1 p-lg-2"><i style="padding-left: 3px;" class="far fa-trash-alt pr-xl-1"></i> <span class="d-none d-xl-block">Eliminar</span></a>
                                  </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach;?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Titulo</th>
                    <th>Lugar</th>
                    <th>Fecha Creación</th>
                    <th>Fecha Modificación</th>
                    <th>Editar</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row --> 
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script>
  $(function () {
    $('#tabla').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      columnDefs: [
          { orderable: false, width: "20%", responsivePriority: 1, targets: -1},
      ]
    });
  });
</script>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>
