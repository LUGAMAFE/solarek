<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Correos Admin <i class="nav-icon fas fa-envelope"></i></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item active">Correos Admin</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
        <div class="card card-secondary">
          <div class="card-header" style="marginbottom: 1rem;">
            <h3 class="card-title">Tabla Correos Clientes Web</h3>
          </div>
          <!-- /.card-header -->

          <div class="card-body">
            <div class="row">
              <div class="col-12">
                <table id="tabla-entradas" class="table table-bordered table-striped dt-responsive">
                  <thead>
                  <tr>
                    <th>ID Correo</th>
                    <th>Direccion Correo</th>
                    <th>Fecha Creación</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($correos as $correo):?>
                        <tr>  
                            <td> <?= $correo["id_correo"];?>                  </td>
                            <td> <?= $correo["direccion_correo"];?>              </td>
                            <td> <?= $correo["fecha_creacion_correo"];?>      </td>
                        </tr>
                    <?php endforeach;?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>ID Correo</th>
                    <th>Direccion Correo</th>
                    <th>Fecha Creación</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row --> 
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script>
  $(function () {
    $('#tabla-entradas').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>
