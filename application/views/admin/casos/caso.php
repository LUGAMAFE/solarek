<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Casos Exito Admin <i class="nav-icon fas fa-images"></i> <?= $accion ?> </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/casosExito") ?>">Casos Exito Admin</a></li>
            <li class="breadcrumb-item active">Caso Exito <?= $accion ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-md-11">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Control Blog</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form-entrada" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form"> 
              <div class="card-body">

                <div class="row justify-content-center">
                  <div class="col-12">
                      <div class="form-group">
                          <label>Capacidad Instalada Wp</label>
                          <input type="number" class="form-control" min="0" name="capacidad-instalada" placeholder="Capacidad.." required value="<?= isset($datos_caso["capacidad_caso"]) ? $datos_caso["capacidad_caso"] : ""; ?>">
                      </div>
                      <div class="form-group">
                          <label>Producción Anual kWh</label>
                          <input type="number" class="form-control" min="0" name="produccion-anual" placeholder="Producción.." required value="<?= isset($datos_caso["produccion_caso"]) ? $datos_caso["produccion_caso"] : ""; ?>">
                      </div>
                      <div class="form-group">
                          <label>Imagen del Caso de Exito</label>
                          <div class="upload-area requerido" id="img-caso"></div>
                          <input type="hidden" name="img-caso-info">
                      </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group ">
                      <label class="text-primary">Idioma a modificar: <span class="text-danger act-text">Español</span> </label>
                      <ul class="nav nav-tabs" id="tabIdioma">
                        <li class="nav-item">
                          <a id="esp-tab" data-toggle="tab" href="#esp-option" role="tab" aria-controls="esp-option" class="nav-link active bg-light" aria-selected="true" > <i class="text-success mr-1 seleccionado fas fa-check"></i> Español</a>
                        </li>
                        <li class="nav-item">
                          <a id="eng-tab" data-toggle="tab" href="#eng-option" role="tab" aria-controls="eng-option" class="nav-link" aria-selected="false" > <i style="display: none;" class="mr-1 text-success seleccionado fas fa-check"></i> Inglés</a>
                        </li>
                      </ul>
                      <div class="tab-content pt-2 bg-light" id="myTabContent">
                        <div class="tab-pane fade show active" id="esp-option" role="tabpanel" aria-labelledby="esp-option">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                  <div class="form-group">
                                      <label>Titulo</label>
                                      <input type="text" class="form-control" name="titulo" placeholder="Titulo.." required value="<?= isset($datos_caso["titulo_caso"]) ? $datos_caso["titulo_caso"] : ""; ?>">
                                  </div>
                                  <div class="form-group">
                                      <label>Lugar</label>
                                      <input type="text" class="form-control" name="lugar" placeholder="Lugar.." required value="<?= isset($datos_caso["lugar_caso"]) ? $datos_caso["lugar_caso"] : ""; ?>">
                                  </div>
                                  <div class="form-group">
                                      <label>Información</label>
                                      <textarea class="form-control" rows="3" name="informacion" placeholder="Informacion Caso..." required><?= isset($datos_caso["informacion_caso"]) ? $datos_caso["informacion_caso"] : ""; ?></textarea>
                                  </div>
                                </div> 
                            </div> 
                        </div>
                        <div class="tab-pane fade" id="eng-option" role="tabpanel" aria-labelledby="eng-option">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                  <div class="form-group">
                                      <label>Titulo</label>
                                      <input type="text" class="form-control" name="titulo-ingles" placeholder="Titulo Inglés.." required value="<?= isset($datos_caso["titulo_ingles_caso"]) ? $datos_caso["titulo_ingles_caso"] : ""; ?>">
                                  </div>
                                  <div class="form-group">
                                      <label>Lugar</label>
                                      <input type="text" class="form-control" name="lugar-ingles" placeholder="Lugar Inglés.." required value="<?= isset($datos_caso["lugar_ingles_caso"]) ? $datos_caso["lugar_ingles_caso"] : ""; ?>">
                                  </div>
                                  <div class="form-group">
                                      <label>Información</label>
                                      <textarea class="form-control" rows="3" name="informacion-ingles" placeholder="Informacion Caso Inglés..." required><?= isset($datos_caso["informacion_ingles_caso"]) ? $datos_caso["informacion_ingles_caso"] : ""; ?></textarea>
                                  </div>
                                </div> 
                            </div> 
                        </div>
                      </div>
                    </div>

                  </div>
                </div>      
                
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <a href="<?= site_url("administracion/casosExito") ?>"  class="btn btn-danger btn-cancelar">Cancelar</a>
                <div id="submit-form" class="btn btn-info float-right">Guardar Caso</div>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>