<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Admin Banners Pagina</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item active">Admin Banners Pagina</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-md-11">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Editar Banners Pagina</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form-entrada" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form"> 
                <div class="card-body">
                  <div class="row justify-content-center">
                      <div class="col-12">
                        <input type="hidden" id="banner-type-home" name="banner-type-home" value="<?= $datos["banner_type_home"] ?>">
                          <div class="form-group">
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="check-home" class="custom-control-input" id="customCheck1" <?= (isset($datos["banner_home_activo"]) && $datos["banner_home_activo"] == 1) ? "checked": ""; ?> >
                                <label class="custom-control-label" for="customCheck1">Activar Banner Home Personalizado</label>
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="text-primary">Eligue el tipo de banner personalizado a mostrar: <span class="text-danger">&nbsp &nbsp Activo Ahora - </span>  <span class="text-danger act-text" class="text-danger"><?= ($datos["banner_type_home"] == "1") ? 'Imagen' : 'Video';?></span> </label>
                          <ul class="nav nav-tabs" id="tabTipoBanner">
                            <li class="nav-item">
                              <a id="image-tab" data-toggle="tab" href="#image-option" role="tab" aria-controls="image-option" class="nav-link <?= ($datos["banner_type_home"] == "1") ? 'active" aria-selected="true" > <i class="text-success mr-1 seleccionado fas fa-check"></i>' : '" aria-selected="false" > <i style="display: none;" class="mr-1 text-success seleccionado fas fa-check"></i>';?> Imagen</a>
                            </li>
                            <li class="nav-item">
                              <a id="video-tab" data-toggle="tab" href="#video-option" role="tab" aria-controls="video-option" class="nav-link <?= ($datos["banner_type_home"] == "2") ? 'active" aria-selected="true" > <i class="text-success mr-1 seleccionado fas fa-check"></i>' : '" aria-selected="false" > <i style="display: none;" class="mr-1 text-success seleccionado fas fa-check"></i>';?> Video Youtube</a>
                            </li>
                          </ul>
                          <div class="tab-content mt-2" id="myTabContent">
                              <div class="tab-pane fade <?= ($datos["banner_type_home"] == "1") ? 'show active' : '';?>" id="image-option" role="tabpanel" aria-labelledby="image-option">
                                <div class="row">
                                  <div class="col-6">
                                    <div class="form-group">
                                      <label>Banner Pagina Home Escritorio<small> Tamaño minimo sugerido 1920 x 800 px</small> </label>
                                      <div class="upload-area individual requerido" id="banner-img-home"></div>
                                      <input type="hidden" name="banner-img-home-info">
                                    </div>
                                  </div>
                                  <div class="col-6">
                                    <div class="form-group">
                                      <label>Banner Pagina Home Movil<small> Tamaño minimo sugerido 800 x 800 px</small> </label>
                                      <div class="upload-area individual requerido" id="banner-img-home-movil"></div>
                                      <input type="hidden" name="banner-img-home-movil-info">
                                    </div>
                                  </div>
                                  <div class="col-6">
                                    <div class="form-group">
                                      <label>Banner Pagina Home Escritorio Inglés<small> Tamaño minimo sugerido 1920 x 800 px</small> </label>
                                      <div class="upload-area individual requerido" id="banner-img-home-ingles"></div>
                                      <input type="hidden" name="banner-img-home-ingles-info">
                                    </div>
                                  </div>
                                  <div class="col-6">
                                    <div class="form-group">
                                      <label>Banner Pagina Home Movil Inglés<small> Tamaño minimo sugerido 800 x 800 px</small> </label>
                                      <div class="upload-area individual requerido" id="banner-img-home-movil-ingles"></div>
                                      <input type="hidden" name="banner-img-home-movil-ingles-info">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="tab-pane fade <?= ($datos["banner_type_home"] == "2") ? 'show active' : '';?>" id="video-option" role="tabpanel" aria-labelledby="video-option">
                                <div class="form-group urls-container">
                                    <label>Url Video Youtube</label>
                                    <div class="input-group">
                                      <input type="url" class="form-control" name="url" placeholder="Url Video Youtube" required value="<?= isset($datos["url_youtube_id_home"]) ? $datos["url_youtube_id_home"] : ""; ?>">
                                    </div>
                                </div>
                                <div class="form-group urls-container">
                                    <label>Url Video Youtube Inglés</label>
                                    <div class="input-group">
                                      <input type="url" class="form-control" name="url-ingles" placeholder="Url Video Youtube Inglés" required value="<?= isset($datos["url_youtube_id_home_ingles"]) ? $datos["url_youtube_id_home_ingles"] : ""; ?>">
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        
                      </div>   
                  </div>   
                </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <a href="<?= site_url("administracion") ?>"  class="btn btn-danger btn-cancelar">Cancelar</a>
                <div id="submit-form" class="btn btn-info float-right">Guardar Banners</div>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>
