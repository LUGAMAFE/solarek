<div class="animation position-absolute w-100 h-100" style="z-index:-1; overflow: hidden;">
    <div id="particles-js" class="w-100 h-100"></div>
</div> 
<div class="login-box m-0">
  <div class="login-logo rounded" style="background-color: #E9ECEF">
    <a href="#"><img class="img-fluid" src="assets/img/logo.png"></a>
  </div>
  <!-- /.login-logo -->
  <div class="card rounded">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Ingresa tus datos para poder iniciar sesión</p>

      <form action="<?= site_url('administracion/login');?>" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Email o Usuario..." name="usuario" required>
          <div class="input-group-append input-group-text">
              <span class="fas fa-user"></span>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Contraseña..." name="password" required>
          <div class="input-group-append input-group-text">
              <span class="fas fa-lock"></span>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember" name="recordar">
              <label for="remember">
                Recordar Sesión
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Iniciar</button>
          </div>
          <!-- /.col -->
        </div>
      </form>


      <p class="mb-1">
        <a>Olvide Mi Contraseña</a>
      </p>
      <!-- <p class="mb-0">
        <a href="assets/adminLTE/pages/examples/register.html" class="text-center">Registrarse</a>
      </p> -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<script>
  $( document ).ready(function() {
    <?php $mensaje = $this->session->flashdata("login_message"); ?>

    <?php if( isset( $mensaje) ):?>
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: '<?= $mensaje["error"]; ?>', 
        footer: '<?= $mensaje["errorExplicacion"]; ?>',
      })
    <?php $this->session->unset_userdata("login_message"); endif; ?>
  });
</script>