<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Cart extends CI_Cart {

 
	public function obtener_subtotal()
	{
		//$ci =& get_instance();
		return $this->format_number($this->total());
	}
	public function obtener_total()
	{
		//$ci =& get_instance();
		$total = ($this->total() * .16) + $this->total();
		//return $this->format_number($this->total());
		return $this->format_number($total);
	}
	public function obtener_subtotalSinFormato()
	{
		//$ci =& get_instance();
		return $this->total();
	}
	public function obtener_totalSinFormato()
	{
		//$ci =& get_instance();
		$total = ($this->total() * .16) + $this->total();
		return round($total,2);
		//return $total;
	}
	public function obtener_impuesto()
	{
		//$ci =& get_instance();
		$total_impuesto = ($this->total() * .16);
		//return $this->format_number($this->total());
		return $this->format_number($total_impuesto);
	}
	public function obtener_impuestoSinFormato()
	{
		//$ci =& get_instance();
		$total_impuesto = ($this->total() * .16);
		//return $this->format_number($this->total(),2,'.','');
		return round($total_impuesto,2);
    }
    
	public function obtener_numero_producro()
	{
        return $this->total_items();
	}

	public function guardar_carrito_session()
	{
		$ci =& get_instance();
		$ci->load->model('clientes/cliente');
		//print_m();
		if($ci->session->userdata('id_cliente')) {
			$id = $ci->session->userdata('id_cliente');
			/* validar si existe sesion de carrito */
			if($ci->cart->total_items() != 0) {
				$cliente = $ci->cliente->get($id);
				if($cliente->num_rows()) {
					$cliente = $cliente->row();
					if($ci->cart->contents()) {
						$carrito = base64_encode(json_encode($ci->cart->contents()));
					} else {
						$carrito = '';
					}
					$update = array(
						'carrito_cliente' => $carrito,
						'update_carrito' => date('Y-m-d')
					);
					//print_m($update);
					$ci->cliente->update($update, $id);
				}
			}
		}
	}
}