<?php 

$lang = array(
	'banners' => array(
		'btn-contacto' => 'CONTACTA A UN ASESOR',
	),
	'header' => array(
		'inicio' => 'INICIO',
		'beneficios' => 'BENEFICIOS',
		'casos_de_exito' => 'CASOS DE ÉXITO',
		'casos_de_exito' => 'CASOS DE ÉXITO',
		'credito' => 'CRÉDITO',
		'blog' => 'BLOG',
		'contacto' => 'CONTACTO',
	),
	'footer' => array(
		'titulo' => 'contacta a un asesor de energía renovable',
		'form' => array(
			'nombre' => 'Nombre',
			'email' => 'Email',
			'telefono' => 'Teléfono',
			'cuanto_paga' => '¿Cuánto paga de luz al bimestre?',
			'mensaje' => 'Mensaje',
			'enviar' => 'Enviar',
		),
		'calle' => 'Calle',
		'oficina' => 'Oficina de ventas:',
		'aviso' => '| SOLÁREK Energía Alternativas | TODOS LOS DERECHOS RESERVADOS | Términos y condiciones | Aviso de privacidad'
	),
	'beneficios' => array(
		'titulo' => 'BENEFICIOS DE CONTRATAR CON SOLÁREK',
		'subtitulo' => 'CONTÁCTA CON UN ASESOR',
		'lista' => array(
			'titulo1' => 'GARANTÍA',
			'texto1' => 'Los paneles solares que instalamos tienen certificación Tier 1 con garantía de producción al 80% de su capacidad durante 25 años.',
			'titulo2' => 'Instalación y puesta en marcha rápida y eficiente',
			'texto2' => 'Tenemos los mejores tiempos de instalación, al igual que 5 años de garantía en la misma.',
			'titulo3' => 'monitoreo',
			'texto3' => 'Nuestra área de monitoreo vigilará todos los días la producción de tu sistema y se comunicarán contigo en dado caso que exista alguna eventualidad para poder solucionarlo.',
			'titulo4' => 'Comprometidos con el medio ambiente',
			'texto4' => 'En promedio cada panel solar colocado por Solárek disminuye emisiones de 250 kg de CO2 al año y evita que se talen 6.5 árboles cada año.',
		)
	),
	'experiencia' => array(
		'titulo' => 'Con más de <b>5</b> años <span>de experiencia</span>',
		'texto' => 'CONTÁCTA CON UN ASESOR',
		'instalaciones' => 'INSTALACIONES',
		'anios' => 'AÑOS DE EXPERIENCIA',
		'paneles' => 'PANELES INSTALADOS'
	),
	'financiamiento_home' => array(
		'titulo' => 'FINANCIAMIENTO <br> PARA HOGAR Y PARA EMPRESAS',
		'asi' => 'Si eres dueño de una propiedad tienes la oportunidad de acceder a este crédito, con cargo en tu recibo de luz.',
		'fide' => 'Si eres una empresa o persona física con actividad empresarial en tarifas 2,3 u OM, el FIDE te ofrece financiamiento.',
		'ci_banco' => 'Es el crédito destinado para paneles solares para negocio propio o para casa habitación propia, con tarifa Doméstica de Alto Consumo (DAC) o 02.',
		'infonavit' => 'Utiliza el saldo en tu Subcuenta de Vivienda del INFONAVIT para tu proyecto de Energía Solar y ahorra sin invertir.',
		'btn_financiamiento' => 'SABER MÁS SOBRE LOS CRÉDITOS'
	),
	'consumo_basico' => array(
		'titulo' => 'CONOCE TU CONSUMO BÁSICO',
		'refrigerador' => '<span>Refrigerador</span> <br> Tarifa 1C: $6.7 <br> Tarifa DAC: $17.8',
		'television' => '<span>Televisión</span> <br> Tarifa 1C: $3.3 <br> Tarifa DAC: $8.8',
		'iluminacion' => '<span>Iluminación</span> <br> Tarifa 1C: $0.56 <br> Tarifa DAC: $1.52',
		'lavadora' => '<span>Lavadora</span> <br> Tarifa 1C: $6.7 <br> Tarifa DAC: $17.8',
		'laptop' => '<span>Laptop</span> <br> Tarifa 1C: $2 <br> Tarifa DAC: $5.3',
		'aire' => '<span>Aire acondicionado</span> <br> Tarifa 1C: $20.6 <br> Tarifa DAC: $54.94',
		'btn_consumo' => 'COMIENZA A AHORRAR'
	),
	'banner_invierte' => array(
		'titulo' => 'ES UN MOMENTO PERFECTO <br> <span>PARA INVERTIR</span> <br> EN PANELES SOLARES',
		'texto' => 'Si pagas más de 7 mil pesos en luz <br> Tienes tarifa DAC <br> Quieres ahorrar el 99% de comsumo',
		'btn_banner' => 'Contacto'
	),
	'casos_exito_home' => array(
		'titulo' => 'CASOS DE <br> <span>ÉXITO</span>',
		'btn_caso_exito' => 'ver más'
	),
	'decide_invertir' => array(
		'titulo' => '¡Decide invertir en los mejores paneles solares de mérida!',
		'soporte' => 'Soporte técnico 24/7',
		'deducible' => '100% deducibles de impuestos',
		'cfe' => 'Ahorra hasta un 97% en tu recibo de CFE'
	),
	'testimonios' => array(
		'titulo' => 'Testimonios SOLÁREK'
	),
	'colaboracion' => array(
		'titulo' => 'Nuestros certificados',
		'titulo_colaboracion' => 'En colaboración con las mejores marcas',
		'forma_pago' => 'Forma de pago',
	),
	'contacto' => array(
		'titulo' => '<p>Permítenos apoyarte en</p> <span>El desarollo de tu proyecto</span>',
		'subtitulo' => 'Solicita una cotización',
		'form' => array(
			'nombre' => 'Nombre',
			'email' => 'Email',
			'telefono' => 'Teléfono',
			'cuanto_paga' => '¿Cuánto paga de luz al bimestre?',
			'residencia' => array(
				'vacio' => 'Residencia o Negocio',
				'residencia' => 'Residencia',
				'negocio' => 'Negocio',
			),
			'mensaje' => 'Mensaje',
			'enviar' => 'Enviar',
		),
	),
	'casos_exito' => array(
		'titulo' => 'NUESTROS CLIENTES SOLÁREK',
		'btn_casos_exito' => 'CONTÁCTA CON UN ASESOR',
		'capacidad' => 'Capacidad instalada:',
		'produccion' => 'Producción anual:'
	),
	'financiamiento' => array(
		'titulo' => 'FINANCIAMIENTO <br> PARA HOGAR Y PARA EMPRESAS',
	),
	'blog' => array(
		'titulo' => 'BLOG SOLÁREK',
		'entradas_recientes' => 'Entradas Recientes',
		'btn_ver_mas' => 'Ver Más',
		'recomendados' => 'Recomendados',
		'descargables' => 'Archivos Descargables',
		'ahorro' => 'El momento de ahorrar es ahora',
		'btn_agendar' => 'Agendar Cita'
	),
	'descargables' => array(
		'titulo' => 'DESCARGABLES',
		'archivo' => 'Archivo Descargable',
		'descargar_ahora' => 'Descarga Ahora',
		'para_descargar' => 'Para descargar el archivo proporcionanos tu correo:',
	),
);	

?>