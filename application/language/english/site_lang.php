<?php 

$lang = array(
	'banners' => array(
		'btn-contacto' => 'CONTACT AN ADVISOR',
	),
	'header' => array(
		'inicio' => 'HOME',
		'beneficios' => 'BENEFITS SUCESS',
		'casos_de_exito' => 'CASES',
		'credito' => 'CREDIT',
		'blog' => 'BLOG',
		'contacto' => 'CONTACT',
	),
	'footer' => array(
		'titulo' => 'Contact a renewable energy advisor.',
		'form' => array(
			'nombre' => 'Name',
			'email' => 'E-mail',
			'telefono' => 'Phone Number',
			'cuanto_paga' => 'How much do you pay on your electricity bill per bimester aproximetely? ',
			'mensaje' => 'Message',
			'enviar' => 'Send',
		),
		'calle' => 'Street',
		'oficina' => 'Sales office:',
		'aviso' => 'Solárek ALTERNATIVE ENERGY | ALL RIGHTS RESERVED'
	),
	'beneficios' => array(
		'titulo' => 'Benefits of hiring Solárek',
		'subtitulo' => 'CONTACT AN ENERGY ADVISOR',
		'lista' => array(
			'titulo1' => 'Guarantee',
			'texto1' => 'Our panels has a Tier 1 certificate with a production guarantee of 80% of their capability for 25 years.',
			'titulo2' => 'Quick and efficient start up',
			'texto2' => 'We have the quickest installations times with a 5 years guarantee on it.',
			'titulo3' => 'Monitoring Area',
			'texto3' => 'Our monitoring area will verify the production of your system everyday and in case any eventuality happens we’ll communicate with you.',
			'titulo4' => 'Committed with the environment',
			'texto4' => 'On average every solar panel that Solárek install, reduces 250 kg of CO2 per year and prevents the chop of 6.5 trees every year.',
		)
	),
	'experiencia' => array(
		'titulo' => 'WE HAVE MORE THAN <b>5</b> YEARS <span>OF EXPERIENCE</span>',
		'texto' => 'CONTACT AN ENERGY ADVISOR',
		'instalaciones' => 'installations',
		'anios' => 'years experience',
		'paneles' => 'solar panels installed'
	),
	'financiamiento_home' => array(
		'titulo' => 'FINANCING <br> FOR HOMES AND BUSINESS COMPANIES',
		'asi' => 'If you own a property you have the opportunity of accesing to this credit with charge to your light bill.',
		'fide' => 'If you have a company or you are a person with business activities the FIDE offers you finincing programms. ',
		'ci_banco' => 'It’s the credit for the acquisition of solar panels for own business or for particular homes with a mortgage credit at your name or of a family member in direct line with domestic rate of high consumption DAC or 02.',
		'infonavit' => 'Use the balance in your INFONAVIT housing sub-account for your solar energy project and start saving without investing.',
		'btn_financiamiento' => 'LEARN MORE ABOUT THE CREDITS'
	),
	'consumo_basico' => array(
		'titulo' => 'KNOW YOUR BASIC CONSUMPTION',
		'refrigerador' => '<span>Refrigerator</span> <br> Rate 1C: $6.7 <br> Rate DAC: $17.8',
		'television' => '<span>Television</span> <br> Rate 1C: $3.3 <br> Rate DAC: $8.8',
		'iluminacion' => '<span>Lights</span> <br> Rate 1C: $0.56 <br> Rate DAC: $1.52',
		'lavadora' => '<span>Washing Machine</span> <br> Rate 1C: $6.7 <br> Rate DAC: $17.8',
		'laptop' => '<span>Laptop</span> <br> Rate 1C: $2 <br> Rate DAC: $5.3',
		'aire' => '<span>Air Conditioner</span> <br> Rate 1C: $20.6 <br> Rate DAC: $54.94',
		'btn_consumo' => 'START SAVING MONEY'
	),
	'banner_invierte' => array(
		'titulo' => 'IT IS THE PERFECT <br> <span>TIME TO INVEST</span> <br> IN SOLAR PANELS',
		'texto' => 'If you pay more than 7,000 Mexican pesos in energy you have a <br> If you have DAC tariff <br> If you want to save 97% of electricity consumption',
		'btn_banner' => 'Contact'
	),
	'casos_exito_home' => array(
		'titulo' => 'SUCESS<br> <span>CASES</span>',
		'btn_caso_exito' => 'SEE MORE'
	),
	'decide_invertir' => array(
		'titulo' => 'CHOOSE TO INVEST ON THE BEST SOLAR PANELS IN MÉRIDA!',
		'soporte' => 'Tech support 24/7',
		'deducible' => '100% tax deductible',
		'cfe' => 'Save up to 97% of your CFE check'
	),
	'testimonios' => array(
		'titulo' => 'Solárek Testimonials'
	),
	'colaboracion' => array(
		'titulo' => 'Our Certificates',
		'titulo_colaboracion' => 'In Collaboration with the best brands',
		'forma_pago' => 'Payments Methods',
	),
	'contacto' => array(
		'titulo' => '<p>LET US SUPPORT YOU</p> <span>THE DEVELOPMENT OF YOUR PROJECT</span>',
		'subtitulo' => 'Contact a renewable energy advisor',
		'form' => array(
			'nombre' => 'Name',
			'email' => 'E-mail',
			'telefono' => 'Phone Number',
			'cuanto_paga' => 'How much do you pay on your electricity bill per bimester aproximetely? ',
			'residencia' => array(
				'vacio' => 'Residence or Busness',
				'residencia' => 'Residence',
				'negocio' => 'Busness',
			),
			'mensaje' => 'Message',
			'enviar' => 'SUBMIT',
		),
	),
	'casos_exito' => array(
		'titulo' => 'OUR CLIENTS RECOMMEND US',
		'btn_casos_exito' => 'CONTACT AN ENERGY ADVISOR',
		'capacidad' => 'Installed capacity:',
		'produccion' => 'Annual production:'
	),
	'financiamiento' => array(
		'titulo' => 'FINANCING <br> FOR HOME AND FOR COMPANIES',
	),
	'blog' => array(
		'titulo' => 'Solárek BLOG',
		'entradas_recientes' => 'Recent Entries',
		'btn_ver_mas' => 'See more',
		'recomendados' => 'Recommended',
		'descargables' => 'Downloaded files',
		'ahorro' => 'ITS THE TIME TO SAVE NOW',
		'btn_agendar' => 'Schedule an appointment'
	),
	'descargables' => array(
		'titulo' => 'DOWNLOADABLE',
		'archivo' => 'Downloadable File',
		'descargar_ahora' => 'Download now',
		'para_descargar' => 'To download the file provide us with your email:',
	),
);

?>