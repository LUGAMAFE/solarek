<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comentario extends MY_Model {

	private   $salt_length = 10;
	protected $_table = 'comentarios';
	protected $_prefix = '_comentario';
	protected $_id         = '';
	protected $field_names = array();

	public function __construct()
	{
		parent::__construct();
		$this->_id = 'id'.$this->_prefix;
		foreach($this->db->list_fields($this->_table) as $key => $row) {
			if($this->_id != $row) {
				$this->field_names = array_merge($this->field_names, array($row));
			}
        }
       
    }
    

    
    public function datetime_creado($datos)
	{
		date_default_timezone_set('America/Mexico_City');
		$datos['creado'.$this->_prefix.''] = date('Y-m-d H:i:s');
		return $datos;
	}

	public function datetime_modificado($datos)
	{
		date_default_timezone_set('America/Mexico_City');
		$datos['actualizado'.$this->_prefix.''] = date('Y-m-d H:i:s');
		return $datos;
	}

}