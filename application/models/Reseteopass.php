<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reseteopass extends MY_Model {

  protected $_table = 'ReseteoPassAdmin';
  protected $_id         = 'id';
  protected $pre_insert  = array('datetime_creado');
  protected $pre_update  = array('datetime_modificado');
  protected $field_names = array(
    "id_usuario",
    "user",
    "token",
    "tipo_usuario",
    "status",
    "creado",
    "actualizado"
    );

  public function __construct()
  {
    parent::__construct();    
  } 

  public function datetime_creado($datos)
  {
    $datos['creado'] = date('Y-m-d H:i:s');
    return $datos;
  }

  public function datetime_modificado($datos)
  {
    $datos['actualizado'] = date('Y-m-d H:i:s');
    return $datos;
  }

}

/* End of file reseteopass.php */
/* Location: ./application/models/reseteopass.php */