<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Correos_Model extends MY_Model {

	public function __construct(){
        parent::__construct();	
        $this->load->database();
    }

    public function existeCorreo($correo, $id = NULL){
        $this->db->select('*');
        $this->db->from('correos');
        if(is_null($id)){
            $this->db->where("direccion_correo = '$correo'");
        }else{
            $this->db->where("id_correo !='$id' AND direccion_correo = '$correo'");
        }
        $query = $this->db->get();
        $existe = $query->num_rows();
        return $existe === 1 ? TRUE : FALSE;
    }

    public function obtenerCorreosTabla(){
        $this->db->select('id_correo, direccion_correo, fecha_creacion_correo');
        $this->db->from('correos');
        $this->db->order_by("id_correo", "ASC");
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function count(){
        $this->db->select('COUNT(*)');
        $this->db->from('correos');
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado['COUNT(*)'];
    }

    public function reset(){
        $query = $this->db->query("ALTER TABLE correos AUTO_INCREMENT =  1");
    }

    public function guardarCorreo($correo){
        if($this->existeCorreo($correo)){
            return;
        }
        $data = array(
            "direccion_correo" => $correo,
            "fecha_creacion_correo" => date('Y/m/d H:i:s'),
        );
        $resultado = $this->db->insert('correos', $data);
        return $resultado;
    }
}    