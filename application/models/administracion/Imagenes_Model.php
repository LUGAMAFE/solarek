<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imagenes_Model extends MY_Model {

	public function __construct(){
        parent::__construct();	
        $this->load->database();
    }

    public function guardarImagen($folder, $uuid, $name){
        $pathinfo = pathinfo($name);
        $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
        $filename = isset($pathinfo['filename']) ? $pathinfo['filename'] : '';
        $folder = str_replace( "../public/" , "", $folder);

        $data = array(
            "uuid_img" => $uuid,
            "name_img" => $filename,
            "folder_img" => $folder,
            "ext_img" => $ext,
            "dir_img" => $folder . "/" . $uuid . "/",
            "full_route_img" => $folder . "/" . $uuid . "/" . $name,
            "fecha_creacion_img" => date('Y/m/d H:i:s'),
            "fecha_modificacion_img" => date('Y/m/d H:i:s'),
        );
        $resultado = $this->db->insert('imagenes', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function eliminarCarpetaImagenPorId($idImg){
        $imagen = $this->obtenerImagenPorId($idImg);
        $directorioValido = isInaccessible($imagen["folder_img"]);
        if($directorioValido){
            return false;
        }
        $target = $imagen["folder_img"]. $imagen["uuid_img"];
        if (is_dir($target)){
            removeDir($target);
        }
        return true;
    }

    public function eliminarCarpetaImagenPorCarpetaUuid($folder , $uuid){
        $folder = str_replace( "../public/" , "", $folder);
        $directorioValido = isInaccessible($folder);
        if($directorioValido){
            return false;
        }
        $target = $folder. $uuid;
        if (is_dir($target)){
            removeDir($target);
        }
        return true;
    }

    public function eliminarImagenPorId($idImg){
        $this->db->where('id_img', $idImg);
        $resultado = $this->db->delete('imagenes');
        if($resultado && $this->countImagenes() <= 0){
            $this->db->query("ALTER TABLE imagenes AUTO_INCREMENT =  1");
        }
        return $resultado;
    }

    public function countImagenes(){
        $this->db->select('COUNT(*)');
        $this->db->from('imagenes');
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado['COUNT(*)'];
    }

    public function obtenerImagenPorId($idImg){
        $this->db->select('*');
        $this->db->from('imagenes');
        $this->db->where("id_img = '$idImg'");
        $query = $this->db->get();
        return $query->row_array();
    }

    public function existeImagen($uuid){
        $this->db->select('*');
        $this->db->from('imagenes');
        $this->db->where("uuid_img = '$uuid'");
        $query = $this->db->get();
        $existe = $query->num_rows();
        return $existe === 1 ? TRUE : FALSE;
    }
}    