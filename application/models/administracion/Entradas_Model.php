<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Entradas_Model extends MY_Model {

	public function __construct(){
        parent::__construct();	
        $this->load->database();
    }

    public function obtenerImagenesEntradaPorId($idEntrada){
        $this->db->select('*');
        $this->db->from('entradas_blog');
        $this->db->where("id_entrada = '$idEntrada'");
        $query = $this->db->get();
        $idImg = $query->row_array();
        return ["img_big" => $idImg["imagen_entrada_big"], "img_small" => $idImg["imagen_entrada_small"]];
    }

    public function eliminarEntradaPorId($id){
        $this->db->where('id_entrada', $id);
        $resultado = $this->db->delete('entradas_blog');
        return $resultado;
    }

    public function existeEntradaId($idEntrada){
        $this->db->select('*');
        $this->db->from('entradas_blog');
        $this->db->where("id_entrada = '$idEntrada'");
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function existeEntrada($datos, $id = NULL){
        $nombre_entrada = $datos["nombre"];
        $titulo_entrada = $datos["titulo"];
        $this->db->select('*');
        $this->db->from('entradas_blog');
        if(is_null($id)){
            $this->db->where("nombre_entrada = '$nombre_entrada' OR titulo_entrada = '$titulo_entrada'");
        }else{
            $this->db->where("id_entrada !='$id' AND (nombre_entrada = '$nombre_entrada' OR titulo_entrada = '$titulo_entrada')");
        }
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function obtenerEntradasTabla(){
        $this->db->select('*');
        $this->db->from('entradas_blog');
        $this->db->order_by("id_entrada", "ASC");
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function obtenerInfoEntradaModificarPorId($idEntrada){
        $this->db->select('*');
        $this->db->from('entradas_blog');
        $this->db->where("id_entrada = '$idEntrada'");
        $query = $this->db->get();  
        return $query->row_array();
    }

    public function countentradas(){
        $this->db->select('COUNT(*)');
        $this->db->from('entradas_blog');
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado['COUNT(*)'];
    }

    public function resetEntradas(){
        $query = $this->db->query("ALTER TABLE entradas_blog AUTO_INCREMENT =  1");
    }

    public function actualizarEntrada($datos, $idEntrada){
        $data = array(
            "nombre_entrada" => ucwords($datos["nombre"]),
            "autor_entrada" => $datos["autor"],
            "titulo_entrada" => $datos["titulo"],
            "subtitulo_entrada" =>$datos["subtitulo"],
            "resumen_entrada" => $datos["resumen"],
            "informacion_entrada" => $datos["informacion"],
            "titulo_ingles_entrada" => $datos["titulo_ingles"],
            "subtitulo_ingles_entrada" =>$datos["subtitulo_ingles"],
            "resumen_ingles_entrada" => $datos["resumen_ingles"],
            "informacion_ingles_entrada" => $datos["informacion_ingles"],
            "check_video_entrada" => $datos["check_video_entrada"],
            "url_youtube_id_entrada" => $datos["url_youtube_id_entrada"],
            "fecha_modificacion_entrada" => date('Y/m/d H:i:s'),
        );
        if(isset($datos["id_img_small"])){
            $data["imagen_entrada_small"] = $datos["id_img_small"];
        }
        if(isset($datos["id_img_big"])){
            $data["imagen_entrada_big"] = $datos["id_img_big"];
        }
        $this->db->where('id_entrada', $idEntrada);
        $resultado = $this->db->update('entradas_blog', $data);
        
        return $resultado;
    }

    public function guardarEntrada($datos){
        
        $data = array(
            "nombre_entrada" => ucwords(strtolower($datos["nombre"])),
            "autor_entrada" => $datos["autor"],
            "titulo_entrada" => $datos["titulo"],
            "subtitulo_entrada" =>$datos["subtitulo"],
            "resumen_entrada" => $datos["resumen"],
            "informacion_entrada" => $datos["informacion"],
            "titulo_ingles_entrada" => $datos["titulo_ingles"],
            "subtitulo_ingles_entrada" =>$datos["subtitulo_ingles"],
            "resumen_ingles_entrada" => $datos["resumen_ingles"],
            "informacion_ingles_entrada" => $datos["informacion_ingles"],
            "check_video_entrada" => $datos["check_video_entrada"],
            "url_youtube_id_entrada" => $datos["url_youtube_id_entrada"],
            "imagen_entrada_big" => $datos["id_img_big"],
            "imagen_entrada_small" => $datos["id_img_small"],
            "fecha_creacion_entrada" => date('Y/m/d H:i:s'),
            "fecha_modificacion_entrada" => date('Y/m/d H:i:s'),
        );
        $resultado = $this->db->insert('entradas_blog', $data);
        
        return $resultado;
    }

    //FUNCIONES PARA VISTA BLOG
    public function getEntradasBlog(){
        $this->db->select('
        id_entrada,
        nombre_entrada,
        autor_entrada,
        titulo_entrada,
        subtitulo_entrada,
        resumen_entrada,
        informacion_entrada,
        titulo_ingles_entrada,
        subtitulo_ingles_entrada,
        resumen_ingles_entrada,
        informacion_ingles_entrada,
        check_video_entrada,
        url_youtube_id_entrada,
        fecha_creacion_entrada,
        fecha_modificacion_entrada,
        imagen_grande.id_img AS big_id_img,
        imagen_grande.folder_img AS big_folder_img,
        imagen_grande.uuid_img AS big_uuid_img,
        imagen_grande.name_img AS big_name_img,
        imagen_grande.ext_img AS big_ext_img,
        imagen_grande.dir_img AS big_dir_img,
        imagen_grande.full_route_img AS big_full_route_img,
        imagen_pequena.id_img AS small_id_img AS,
        imagen_pequena.folder_img AS small_folder_img,
        imagen_pequena.uuid_img AS small_uuid_img,
        imagen_pequena.name_img AS small_name_img,
        imagen_pequena.ext_img AS small_ext_img,
        imagen_pequena.dir_img AS small_dir_img,
        imagen_pequena.full_route_img AS small_full_route_img,
        ');
        $this->db->from('entradas_blog');
        $this->db->join('imagenes as imagen_grande', 'entradas_blog.imagen_entrada_big = imagen_grande.id_img', 'left');
        $this->db->join('imagenes as imagen_pequena', ' entradas_blog.imagen_entrada_small = imagen_pequena.id_img', 'left');
        $this->db->order_by("fecha_modificacion_entrada", "desc");
        $query = $this->db->get();  
        return $query->result_array();
    }
    
    public function getEntrada($idEntrada){
        $this->db->select('
        id_entrada,
        nombre_entrada,
        autor_entrada,
        titulo_entrada,
        subtitulo_entrada,
        resumen_entrada,
        informacion_entrada,
        titulo_ingles_entrada,
        subtitulo_ingles_entrada,
        resumen_ingles_entrada,
        informacion_ingles_entrada,
        check_video_entrada, 
        url_youtube_id_entrada,
        fecha_creacion_entrada,
        fecha_modificacion_entrada,
        imagen_grande.id_img AS big_id_img,
        imagen_grande.folder_img AS big_folder_img,
        imagen_grande.uuid_img AS big_uuid_img,
        imagen_grande.name_img AS big_name_img,
        imagen_grande.ext_img AS big_ext_img,
        imagen_grande.dir_img AS big_dir_img,
        imagen_grande.full_route_img AS big_full_route_img,
        imagen_pequena.id_img AS small_id_img AS,
        imagen_pequena.folder_img AS small_folder_img,
        imagen_pequena.uuid_img AS small_uuid_img,
        imagen_pequena.name_img AS small_name_img,
        imagen_pequena.ext_img AS small_ext_img,
        imagen_pequena.dir_img AS small_dir_img,
        imagen_pequena.full_route_img AS small_full_route_img,
        ');
        $this->db->from('entradas_blog');
        $this->db->join('imagenes as imagen_grande', 'entradas_blog.imagen_entrada_big = imagen_grande.id_img', 'left');
        $this->db->join('imagenes as imagen_pequena', ' entradas_blog.imagen_entrada_small = imagen_pequena.id_img', 'left');
        $this->db->where("id_entrada = '$idEntrada'");
        $this->db->order_by("fecha_modificacion_entrada", "desc");
        $query = $this->db->get();  
        return $query->row_array();
    }

    public function getEntradasBlogLimit($offset, $limit){
        $this->db->select('imagen_grande.full_route_img as ruta_big, imagen_pequena.full_route_img as ruta_small, titulo_entrada, subtitulo_entrada, resumen_entrada, titulo_ingles_entrada, subtitulo_ingles_entrada, resumen_ingles_entrada, check_video_entrada, url_youtube_id_entrada, id_entrada');
        $this->db->from('entradas_blog');
        $this->db->join('imagenes as imagen_grande', 'entradas_blog.imagen_entrada_big = imagen_grande.id_img', 'left');
        $this->db->join('imagenes as imagen_pequena', ' entradas_blog.imagen_entrada_small = imagen_pequena.id_img', 'left');
        $this->db->order_by("fecha_modificacion_entrada", "desc");
        $this->db->limit($limit, $offset);
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function getEntradasBlogLimitRand($offset, $limit){
        $this->db->select('imagen_grande.full_route_img as ruta_big, imagen_pequena.full_route_img as ruta_small, titulo_entrada, subtitulo_entrada, resumen_entrada, titulo_ingles_entrada, subtitulo_ingles_entrada, resumen_ingles_entrada, check_video_entrada, url_youtube_id_entrada, id_entrada');
        $this->db->from('entradas_blog');
        $this->db->join('imagenes as imagen_grande', 'entradas_blog.imagen_entrada_big = imagen_grande.id_img', 'left');
        $this->db->join('imagenes as imagen_pequena', ' entradas_blog.imagen_entrada_small = imagen_pequena.id_img', 'left');
        $this->db->order_by("rand()");
        $this->db->limit($limit, $offset);
        $query = $this->db->get();  
        return $query->result_array();
    }
}    