<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonios_Model extends MY_Model {

	public function __construct(){
        parent::__construct();	
        $this->load->database();
    }

    public function obtenerImagenPorId($id){
        $this->db->select('img_testimonio');
        $this->db->from('testimonios');
        $this->db->where("id_testimonio = '$id'");
        $query = $this->db->get();
        $idImg = $query->row_array();
        return $idImg["img_testimonio"];
    }

    public function eliminarPorId($id){
        $this->db->where('id_testimonio', $id);
        $resultado = $this->db->delete('testimonios');
        return $resultado;
    }

    public function existeId($id){
        $this->db->select('*');
        $this->db->from('testimonios');
        $this->db->where("id_testimonio = '$id'");
        $query = $this->db->get();
        $existe = $query->num_rows();
        return $existe === 1 ? TRUE : FALSE;
    }

    public function existe($datos, $id = NULL){
        $cliente = $datos["cliente"];
        $this->db->select('*');
        $this->db->from('testimonios');
        if(is_null($id)){
            $this->db->where("cliente_testimonio = '$cliente'");
        }else{
            $this->db->where("id_testimonio !='$id' AND cliente_testimonio = '$cliente'");
        }
        $query = $this->db->get();
        $existe = $query->num_rows();
        return $existe === 1 ? TRUE : FALSE;
    }

    public function obtenerTestimonios(){
        $this->db->select('*');
        $this->db->from('testimonios');
        $this->db->order_by("id_testimonio", "ASC");
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function obtenerInfoPorId($id){
        $this->db->select('*');
        $this->db->from('testimonios');
        $this->db->where("id_testimonio = '$id'");
        $query = $this->db->get();  
        return $query->row_array();
    }

    public function count(){
        $this->db->select('COUNT(*)');
        $this->db->from('testimonios');
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado['COUNT(*)'];
    }

    public function reset(){
        $query = $this->db->query("ALTER TABLE testimonios AUTO_INCREMENT =  1");
    }

    public function actualizarTestimonio($datos, $id){
        $data = array(
            "cliente_testimonio" => $datos["cliente"],
            "descripcion_testimonio" => $datos["descripcion"],
            "descripcion_ingles_testimonio" => $datos["descripcion-ingles"],
            "fecha_modificacion_testimonio" => date('Y/m/d H:i:s'),
        );
        if(isset($datos["img_testimonio"])){
            $data["img_testimonio"] = $datos["img_testimonio"];
        }
        $this->db->where('id_testimonio', $id);
        $resultado = $this->db->update('testimonios', $data);
        
        return $resultado;
    }

    public function guardar($datos){
        
        $data = array(
            "cliente_testimonio" => $datos["cliente"],
            "img_testimonio" => $datos["img_testimonio"],
            "descripcion_testimonio" => $datos["descripcion"],
            "descripcion_ingles_testimonio" => $datos["descripcion-ingles"],
            "fecha_creacion_testimonio" => date('Y/m/d H:i:s'),
            "fecha_modificacion_testimonio" => date('Y/m/d H:i:s'),
        );
        $resultado = $this->db->insert('testimonios', $data);
        
        return $resultado;
    }

    //FUNCIONES PARA VISTA BLOG
    public function getTestimonios(){
        $this->db->select('
            cliente_testimonio,
            descripcion_testimonio,
            descripcion_ingles_testimonio,
            img.id_img,
            img.folder_img,
            img.uuid_img,
            img.name_img,
            img.ext_img,
            img.dir_img,
            img.full_route_img,
        ');
        
        $this->db->from('testimonios');
        $this->db->join('imagenes as img', 'testimonios.img_testimonio = img.id_img', 'left');
        $this->db->order_by("fecha_modificacion_testimonio", "desc");
        $query = $this->db->get();  
        return $query->result_array();
    }
}    