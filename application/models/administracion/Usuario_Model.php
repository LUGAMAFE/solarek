<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario_Model extends MY_Model {

	public function __construct(){
        parent::__construct();	
        $this->load->database();
    }

    private function usuarioSelect($usuario){
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where("nombre_usuario = '".strtolower($usuario)."' OR correo_usuario = '$usuario'");
        $query = $this->db->get();
        return $query;
    }

    public function existeUsuario($usuario){
        $query = $this->usuarioSelect($usuario);
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function getDatosUsuario($usuario){
        $query = $this->usuarioSelect($usuario);
        $row = $query->row_array();
        return $row;
    }
}    