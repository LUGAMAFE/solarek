<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Descargables_Model extends MY_Model {

	public function __construct(){
        parent::__construct();	
        $this->load->database();
    }

    public function obtenerArchivoDescargablePorId($id){
        $this->db->select('id_archivo_descargable');
        $this->db->from('descargables');
        $this->db->where("id_descargable = '$id'");
        $query = $this->db->get();
        $idImg = $query->row_array();
        return $idImg["id_archivo_descargable"];
    }

    public function eliminarDescargablePorId($id){
        $this->db->where('id_descargable', $id);
        $resultado = $this->db->delete('descargables');
        return $resultado;
    }

    public function existeDescargableId($id){
        $this->db->select('*');
        $this->db->from('descargables');
        $this->db->where("id_descargable = '$id'");
        $query = $this->db->get();
        $existe = $query->num_rows();
        return $existe === 1 ? TRUE : FALSE;
    }

    public function existeDescargable($datos, $id = NULL){
        $titulo = $datos["titulo"];
        $this->db->select('*');
        $this->db->from('descargables');
        if(is_null($id)){
            $this->db->where("titulo_descargable = '$titulo'");
        }else{
            $this->db->where("id_descargable !='$id' AND titulo_descargable = '$titulo'");
        }
        $query = $this->db->get();
        $existe = $query->num_rows();
        return $existe === 1 ? TRUE : FALSE;
    }

    public function obtenerDescargablesTabla(){
        $this->db->select('id_descargable, titulo_descargable, fecha_creacion_descargable, fecha_modificacion_descargable');
        $this->db->from('descargables');
        $this->db->order_by("id_descargable", "ASC");
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function obtenerInfoModificarPorId($id){
        $this->db->select('titulo_descargable, descripcion_descargable');
        $this->db->from('descargables');
        $this->db->where("id_descargable = '$id'");
        $query = $this->db->get();  
        return $query->row_array();
    }

    public function count(){
        $this->db->select('COUNT(*)');
        $this->db->from('descargables');
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado['COUNT(*)'];
    }

    public function reset(){
        $query = $this->db->query("ALTER TABLE descargables AUTO_INCREMENT =  1");
    }

    public function actualizar($datos, $id){
        $data = array(
            "titulo_descargable" => $datos["titulo"],
            "descripcion_descargable" => $datos["descripcion"],
            "fecha_modificacion_descargable" => date('Y/m/d H:i:s'),
        );
        if(isset($datos["id_archivo"])){
            $data["id_archivo_descargable"] = $datos["id_archivo"];
        }
        $this->db->where('id_descargable', $id);
        $resultado = $this->db->update('descargables', $data);
        
        return $resultado;
    }

    public function guardarDescargable($datos){
        $data = array(
            "titulo_descargable" => $datos["titulo"],
            "descripcion_descargable" => $datos["descripcion"],
            "id_archivo_descargable" => $datos["id_archivo"],
            "fecha_creacion_descargable" => date('Y/m/d H:i:s'),
            "fecha_modificacion_descargable" => date('Y/m/d H:i:s'),
        );
        $resultado = $this->db->insert('descargables', $data);
        return $resultado;
    }

    public function getDescargables(){
        $this->db->select('
        titulo_descargable,
        descripcion_descargable,
        archivo_descargable.id_img AS id_archivo,
        archivo_descargable.folder_img AS folder_archivo,
        archivo_descargable.uuid_img AS uuid_archivo,
        archivo_descargable.name_img AS name_archivo,
        archivo_descargable.ext_img AS ext_archivo,
        archivo_descargable.dir_img AS dir_archivo,
        archivo_descargable.full_route_img AS full_route_archivo,
        ');
        
        $this->db->from('descargables');
        $this->db->join('imagenes as archivo_descargable', 'descargables.id_archivo_descargable = archivo_descargable.id_img', 'left');
        $this->db->order_by("fecha_modificacion_descargable", "desc");
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function getDescargableTitulo($titulo){
        $this->db->select('
        descripcion_descargable,
        archivo_descargable.id_img AS id_archivo,
        archivo_descargable.folder_img AS folder_archivo,
        archivo_descargable.uuid_img AS uuid_archivo,
        archivo_descargable.name_img AS name_archivo,
        archivo_descargable.ext_img AS ext_archivo,
        archivo_descargable.dir_img AS dir_archivo,
        archivo_descargable.full_route_img AS full_route_archivo,
        ');
        
        $this->db->from('descargables');
        $this->db->join('imagenes as archivo_descargable', 'descargables.id_archivo_descargable = archivo_descargable.id_img', 'left');
        $this->db->where('titulo_descargable', $titulo);
        $query = $this->db->get();  
        return $query->row_array();
    }
}    