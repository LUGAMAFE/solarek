<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners_Model extends CI_Model {

	public function __construct(){
        parent::__construct();	
        $this->load->database();
    }

    public function get_banners(){
        $this->db->from('banners');
        $query = $this->db->get();  
        return $query->row_array();;
    }

    public function actualizar($data, $update){
        if($update){
            $this->db->where("id_banners", 1);
            $resultado = $this->db->update("banners", $data);
        }else{
            $resultado = $this->db->insert("banners", $data);
        }
        return $resultado;
    }

    public function obtenerIdsBanners(){
        $this->db->select('id_img_banner_home, id_img_banner_home_movil, id_img_banner_home_ingles, id_img_banner_home_movil_ingles');
        $this->db->from("banners");
        $this->db->order_by("id_banners", "DESC");
        $this->lastQuery = $this->db->get();  
        return $this->lastQuery->row_array();
    }

    /**
     * Consulta generica para recuperar el primer regstro de la tabla
     * @return array
     */
    public function getFirst() {
        $this->db->select('*');
        $this->db->from("banners");
        $this->db->order_by("id_banners", "DESC");
        $this->lastQuery = $this->db->get();  
        return $this->lastQuery->row_array();
    }
}    