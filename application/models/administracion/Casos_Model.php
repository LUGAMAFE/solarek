<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Casos_Model extends MY_Model {

	public function __construct(){
        parent::__construct();	
        $this->load->database();
    }

    public function obtenerImagenCasoPorId($idCaso){
        $this->db->select('img_caso');
        $this->db->from('casos_exito');
        $this->db->where("id_caso = '$idCaso'");
        $query = $this->db->get();
        $idImg = $query->row_array();
        return $idImg["img_caso"];
    }

    public function eliminarCasoPorId($id){
        $this->db->where('id_caso', $id);
        $resultado = $this->db->delete('casos_exito');
        return $resultado;
    }

    public function existeCasoId($id){
        $this->db->select('*');
        $this->db->from('casos_exito');
        $this->db->where("id_caso = '$id'");
        $query = $this->db->get();
        $existeCaso = $query->num_rows();
        return $existeCaso === 1 ? TRUE : FALSE;
    }

    public function existeCaso($datos, $id = NULL){
        $titulo = $datos["titulo"];
        $this->db->select('*');
        $this->db->from('casos_exito');
        if(is_null($id)){
            $this->db->where("titulo_caso = '$titulo'");
        }else{
            $this->db->where("id_caso !='$id' AND titulo_caso = '$titulo'");
        }
        $query = $this->db->get();
        $existeCaso = $query->num_rows();
        return $existeCaso === 1 ? TRUE : FALSE;
    }

    public function obtenerCasosTabla(){
        $this->db->select('*');
        $this->db->from('casos_exito');
        $this->db->order_by("id_caso", "ASC");
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function obtenerInfoCasoModificarPorId($id){
        $this->db->select('*');
        $this->db->from('casos_exito');
        $this->db->where("id_caso = '$id'");
        $query = $this->db->get();  
        return $query->row_array();
    }

    public function countCasos(){
        $this->db->select('COUNT(*)');
        $this->db->from('casos_exito');
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado['COUNT(*)'];
    }

    public function resetCasos(){
        $query = $this->db->query("ALTER TABLE casos_exito AUTO_INCREMENT =  1");
    }

    public function actualizarCaso($datos, $id){
        $data = array(
            "titulo_caso" => $datos["titulo"],
            "lugar_caso" => $datos["lugar"],
            "capacidad_caso" => $datos["capacidad-instalada"],
            "produccion_caso" => $datos["produccion-anual"],
            "informacion_caso" => $datos["informacion"],
            "titulo_ingles_caso" => $datos["titulo-ingles"],
            "lugar_ingles_caso" => $datos["lugar-ingles"],
            "informacion_ingles_caso" => $datos["informacion-ingles"],
            "fecha_modificacion_caso" => date('Y/m/d H:i:s'),
        );
        if(isset($datos["id_img_caso"])){
            $data["img_caso"] = $datos["id_img_caso"];
        }
        $this->db->where('id_caso', $id);
        $resultado = $this->db->update('casos_exito', $data);
        
        return $resultado;
    }

    public function guardarCaso($datos){
        
        $data = array(
            "titulo_caso" => $datos["titulo"],
            "lugar_caso" => $datos["lugar"],
            "capacidad_caso" => $datos["capacidad-instalada"],
            "produccion_caso" => $datos["produccion-anual"],
            "informacion_caso" => $datos["informacion"],
            "titulo_ingles_caso" => $datos["titulo-ingles"],
            "lugar_ingles_caso" => $datos["lugar-ingles"],
            "informacion_ingles_caso" => $datos["informacion-ingles"],
            "img_caso" => $datos["id_img_caso"],
            "fecha_creacion_caso" => date('Y/m/d H:i:s'),
            "fecha_modificacion_caso" => date('Y/m/d H:i:s'),
        );
        $resultado = $this->db->insert('casos_exito', $data);
        
        return $resultado;
    }

    //FUNCIONES PARA VISTA BLOG
    public function getCasosExito(){
        $this->db->select('
        titulo_caso,
        lugar_caso,
        capacidad_caso,
        produccion_caso,
        informacion_caso,
        titulo_ingles_caso,
        lugar_ingles_caso,
        informacion_ingles_caso,
        imagen_caso.id_img AS big_id_img,
        imagen_caso.folder_img AS big_folder_img,
        imagen_caso.uuid_img AS big_uuid_img,
        imagen_caso.name_img AS big_name_img,
        imagen_caso.ext_img AS big_ext_img,
        imagen_caso.dir_img AS big_dir_img,
        imagen_caso.full_route_img AS big_full_route_img,
        ');
        
        $this->db->from('casos_exito');
        $this->db->join('imagenes as imagen_caso', 'casos_exito.img_caso = imagen_caso.id_img', 'left');
        $this->db->order_by("fecha_modificacion_caso", "desc");
        $query = $this->db->get();  
        return $query->result_array();
    }
}    