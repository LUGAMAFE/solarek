<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categoria extends MY_Model {

	protected $_table = 'categorias';
	protected $_prefix = '_categoria';
	protected $pre_insert  = array('url_str', 'datetime_creado');
	protected $pre_update  = array('url_str', 'datetime_modificado');
	protected $_id         = '';
	protected $field_names = array();

	public function __construct()
	{
		parent::__construct();
		$this->_id = 'id'.$this->_prefix;
		foreach($this->db->list_fields($this->_table) as $key => $row) {
			if($this->_id != $row) {
				$this->field_names = array_merge($this->field_names, array($row));
			}
        }
    }

    public function url_str($datos)
	{
		$datos['nombre_categoriai_slug'] = create_slug($datos['nombre_categoriai']);
		$datos['nombre_categoriae_slug'] = create_slug($datos['nombre_categoriae']);
		return $datos;
	}

	public function datetime_creado($datos)
	{
		date_default_timezone_set('America/Mexico_City');
		$datos['creado'.$this->_prefix] = date('Y-m-d H:i:s');
		return $datos;
	}

	public function datetime_modificado($datos)
	{
		date_default_timezone_set('America/Mexico_City');
		$datos['actualizado'.$this->_prefix] = date('Y-m-d H:i:s');
		return $datos;
	}

	public function Obtener_menu_full() 
	{
		$result = $this->db->order_by('orden_categoria', 'ASC')->get($this->_table)->result();

		foreach($result as $key => $row) {
			$subresult = $this->db->get_where('subcategorias', array('id_categoria' => $row->id_categoria));
			$aux = $subresult->result();
			if(sizeof($aux) > 0) {
				$result[$key]->result = $aux;
			}
		}
		return $result;
	}

	public function view_categoria_home()
	{
		$result = $this->db->order_by('orden_categoria', 'ASC')->get($this->_table);
		$num_total = $result->num_rows();
		$result = $result->result();
		$banner_1 = array();
		$banner_2 = array();
		if($num_total > 0){
			foreach($result as $key => $row) {
				if($row->tipo_banner_principal == 1) {
					array_push($banner_1, $row);
				} elseif($row->tipo_banner_principal == 2) {
					array_push($banner_2, $row);
				}
			}
			$num_total_banner_1 = count($banner_1);
			$num_total_banner_2 = count($banner_2);
			$num = count($banner_2) / 2;
			$num = round($num, 0, PHP_ROUND_HALF_UP);
			//print_m($banner_2);
			$aux_banner_2 = array(); 
			$aux_banner = array();
			if($banner_2 % 2 == 0) {
				$count = 1;
				foreach($banner_2 as $row) {
					$aux = $row;
					if($count == 2) {
						$aux->nivel = 2;
						$count = 0;
						array_push($aux_banner, $aux);
						array_push($aux_banner_2, $aux_banner);
						$aux_banner = array();
					} else {
						$aux->nivel = 1;
						array_push($aux_banner, $aux);
					}
					$count++;
				}
			} else {
				$count = 1;
				$count_items = 1;
				foreach($banner_2 as $row) {
					$aux = $row;
					if($count == 2) {
						if($num_total_banner_2 == $count_items) {
							$aux->nivel = 2;
							$count = 0;
						} else {
							$aux->nivel = 2;
							$count = 0;
						}
						array_push($aux_banner, $aux);
						array_push($aux_banner_2, $aux_banner);
						$aux_banner = array();
					} else {
						$aux->nivel = 1;
						array_push($aux_banner, $aux);
					}
					
					$count++;
				}
			}
			//agrupar el array
			/*echo $num_total_banner_1.'<br>';
			echo $num_total_banner_1.'<br>';*/
			//$total_div = $num_total_banner_1 + $num;
			//echo $total_div; 
			//print_m($banner_1);
			//print_m($aux_banner_2);
			$new_array_banner = array_merge($banner_1, $aux_banner_2);
			return shuffle_assoc($new_array_banner);
		} else {
			return array();
		}
	}

	public function Actualizarfotocategoria($datos, $id)
	{	
		$this->db->where('id_categoria', $id);
		$this->db->update('categorias', $datos);
	}
	
}