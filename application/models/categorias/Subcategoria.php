<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subcategoria extends MY_Model {

	private   $salt_length = 10;
	protected $_table = 'subcategorias';
	protected $_prefix = '_subcategoria';
	protected $pre_insert  = array('url_str', 'datetime_creado');
	protected $pre_update  = array('url_str', 'datetime_modificado');
	protected $_id         = '';
	protected $field_names = array();

	public function __construct()
	{
		parent::__construct();
		$this->_id = 'id'.$this->_prefix;
		foreach($this->db->list_fields($this->_table) as $key => $row) {
			if($this->_id != $row) {
				$this->field_names = array_merge($this->field_names, array($row));
			}
        }
    }

    public function url_str($datos)
	{
		$datos['nombre_subcategoriae_slug'] = create_slug($datos['nombre_subcategoriae']);
		$datos['nombre_subcategoriai_slug'] = create_slug($datos['nombre_subcategoriai']);
		return $datos;
	}

	public function datetime_creado($datos)
	{
		date_default_timezone_set('America/Mexico_City');
		$datos['creado'.$this->_prefix] = date('Y-m-d H:i:s');
		return $datos;
	}

	public function datetime_modificado($datos)
	{
		date_default_timezone_set('America/Mexico_City');
		$datos['actualizado'.$this->_prefix] = date('Y-m-d H:i:s');
		return $datos;
	}
}