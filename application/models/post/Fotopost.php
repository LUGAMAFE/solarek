<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fotopost extends MY_Model {

	protected $_table = 'FotoPost';
	protected $_prefix = '_foto_post';
	protected $_id         = '';
	protected $pre_insert  = array('datetime_creado');
	protected $pre_update  = array('datetime_modificado');
	protected $field_names = array();

	public function __construct()
	{
		parent::__construct();
		$this->_id = 'id'.$this->_prefix;
		foreach($this->db->list_fields($this->_table) as $key => $row) {
			if($this->_id != $row) {
				$this->field_names = array_merge($this->field_names, array($row));
			}
		}		
	}

}

/* End of file fotografia.php */
/* Location: ./application/models/fotografia.php */