<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends MY_Model {

	protected $_table = 'Posts';
	protected $_prefix = '_post';
	protected $_id         = '';
	protected $pre_insert  = array('url_str', 'datetime_creado');
	protected $pre_update  = array('url_str', 'datetime_modificado');
	protected $field_names = array();

	public function __construct()
	{
		parent::__construct();	
		$this->_id = 'id'.$this->_prefix;
		foreach($this->db->list_fields($this->_table) as $key => $row) {
			if($this->_id != $row) {
				$this->field_names = array_merge($this->field_names, array($row));
			}
		}		
	}

	public function url_str($datos)
	{
		$datos['titulo'.$this->_prefix.'_slug_es'] = create_slug($datos['titulo'.$this->_prefix.'_es']);
		$datos['titulo'.$this->_prefix.'_slug_en'] = create_slug($datos['titulo'.$this->_prefix.'_en']);
		//$datos['titulo'.$this->_prefix.'_slug_fr'] = create_slug($datos['titulo'.$this->_prefix.'_fr']);
		return $datos;
	}

	public function get_post($id = '')
	{
		//echo $id;
		$this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
        $this->db->where('id_post', $id);
        $post = $this->db->get($this->_table)->row();
        //print_m($post);
		$img = $this->db->where('idRel_post', $id)->get('FotoPost')->result();
		return array_merge(
			(array) $post, 
			array(
				'img' => $img
			)
		);
	}

	public function get_post_ramdom()
	{
		$this->db->select('SQL_CALC_FOUND_ROWS id_post, titulo_post_es, creado_post, titulo_post_slug_es, titulo_post_slug_en, titulo_post_en, tipo_post, descripcion_breve_en,
			(select path_foto_post from FotoPost where idRel_post = '.$this->_table.'.id_post order by principal_foto_post desc limit 1) as path,
			(select ext_foto_post from FotoPost where idRel_post = '.$this->_table.'.id_post order by principal_foto_post desc limit 1) as ext,
			(select file_name_foto_post from FotoPost where idRel_post = '.$this->_table.'.id_post order by principal_foto_post desc limit 1) as file,
		', FALSE);
        $this->db->order_by('rand()');
        $this->db->limit(4);

		return $this->db->get($this->_table);
	}

	public function get_post_last()
	{
		$this->db->select('SQL_CALC_FOUND_ROWS id_post, titulo_post_es, creado_post, titulo_post_slug_es, titulo_post_slug_en, titulo_post_en, tipo_post, descripcion_breve_en,
			(select path_foto_post from FotoPost where idRel_post = '.$this->_table.'.id_post order by principal_foto_post desc limit 1) as path,
			(select ext_foto_post from FotoPost where idRel_post = '.$this->_table.'.id_post order by principal_foto_post desc limit 1) as ext,
			(select file_name_foto_post from FotoPost where idRel_post = '.$this->_table.'.id_post order by principal_foto_post desc limit 1) as file,
		', FALSE);
        $this->db->order_by('id_post', 'DESC');
        $this->db->limit(3);

		return $this->db->get($this->_table);
	}

	public function get_posts($url = '')
	{
		$this->db->select('SQL_CALC_FOUND_ROWS id_post, titulo_post_es, creado_post, titulo_post_slug_es, titulo_post_slug_en, titulo_post_en, tipo_post, descripcion_breve_en,
			(select path_foto_post from FotoPost where idRel_post = '.$this->_table.'.id_post order by principal_foto_post desc limit 1) as path,
			(select ext_foto_post from FotoPost where idRel_post = '.$this->_table.'.id_post order by principal_foto_post desc limit 1) as ext,
			(select file_name_foto_post from FotoPost where idRel_post = '.$this->_table.'.id_post order by principal_foto_post desc limit 1) as file,
		', FALSE);
		if($this->session->userdata('busqueda_post') && !empty($this->session->userdata('busqueda_post'))){
			$this->db->like("CONCAT(titulo_post_es)", $this->session->userdata('busqueda_post'), 'both', FALSE);
		}
		$this->db->where('estatus'.$this->_prefix, 1);
		$this->db->order_by('id_post', 'DESC');
		
		return $this->db->get($this->_table);
	}

	public function busqueda($buscar = '', $offset = 0, $limit = 15)
	{
		$this->db->select('SQL_CALC_FOUND_ROWS *,
			(select path_foto_post from FotoPost where idRel_post = '.$this->_table.'.id_post order by principal_foto_post desc limit 1) as path,
			(select ext_foto_post from FotoPost where idRel_post = '.$this->_table.'.id_post order by principal_foto_post desc limit 1) as ext,
			(select file_name_foto_post from FotoPost where idRel_post = '.$this->_table.'.id_post order by principal_foto_post desc limit 1) as file,
		', FALSE);
		//$this->db->join('Menus', 'id_menu = idRel_menu');
		if (!empty($buscar)) {
			$this->db->like("CONCAT(nombre_proveedor, ' ', nombre_pds)", $buscar, 'both', FALSE);
		}

		$this->db->order_by('id'.$this->_prefix, 'DESC');
		$limit  = (is_numeric($limit)) ? $limit:15;

		if ($limit != 0) {
			$offset = (is_numeric($offset)) ? $offset:0;
			$this->db->limit($limit, $offset);
		}

		return $this->db->get($this->_table);
	}
}