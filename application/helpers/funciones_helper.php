<?php 
	if ( ! function_exists('formatDateToString'))
	{
		/**
		 * Formatea un date de sql a la fecha en lenguaje natural dependiendo la confiuracion de setLocale().
		 * @param string $date
		 * @param string $lenguaje
		 */
		function formatDateToString($date, $lenguaje){
			switch ($lenguaje) {
				case "english":
					$fecha = utf8_encode(strftime("%A, %d %B %Y", strtotime($date)));
					break;
				default:
					$fecha = utf8_encode(strftime("%A, %d de %B del %Y", strtotime($date)));
					break;
			}
			return $fecha;
		}
	}

	if ( ! function_exists('removeDir'))
	{
		/**
		 * Removes a directory and all files contained inside
		 * @param string $dir
		 */
		function removeDir($dir){
			foreach (scandir($dir) as $item){
				if ($item == "." || $item == "..")
					continue;

				if (is_dir($item)){
					removeDir($item);
				} else {
					unlink(join(DIRECTORY_SEPARATOR, array($dir, $item)));
				}

			}
			rmdir($dir);
		}
	}
    
	if ( ! function_exists('isWindows'))
	{
		/**
		 * Determines is the OS is Windows or not
		 *
		 * @return boolean
		 */
		function isWindows() {
			$isWin = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN');
			return $isWin;
		}
	}

	if ( ! function_exists('isInaccessible'))
	{
		/**
		 * Determines whether a directory can be accessed.
		 *
		 * is_executable() is not reliable on Windows prior PHP 5.0.0
		 *  (http://www.php.net/manual/en/function.is-executable.php)
		 * The following tests if the current OS is Windows and if so, merely
		 * checks if the folder is writable;
		 * otherwise, it checks additionally for executable status (like before).
		 *
		 * @param string $directory The target directory to test access
		 */
		function isInaccessible($directory) {
			$isWin = isWindows();
			$folderInaccessible = ($isWin) ? !is_writable($directory) : ( !is_writable($directory) && !is_executable($directory) );
			return $folderInaccessible;
		}
	}

	if ( ! function_exists('redirect_back'))
	{
		function redirect_back()
		{
			if(isset($_SERVER['HTTP_REFERER']))
			{
				header('Location: '.$_SERVER['HTTP_REFERER']);
			}
			else
			{
				header('Location: http://'.$_SERVER['SERVER_NAME']);
			}
			exit;
		}
	}
	
	if(! function_exists('get_request_method')) 
	{
		// This will retrieve the "intended" request method.  Normally, this is the
        // actual method of the request.  Sometimes, though, the intended request method
        // must be hidden in the parameters of the request.  For example, when attempting to
        // delete a file using a POST request. In that case, "DELETE" will be sent along with
        // the request in a "_method" parameter.
        function get_request_method() {
            global $HTTP_RAW_POST_DATA;

            if(isset($HTTP_RAW_POST_DATA)) {
                parse_str($HTTP_RAW_POST_DATA, $_POST);
            }

            if (isset($_POST["_method"]) && $_POST["_method"] != null) {
                return $_POST["_method"];
            }

            return $_SERVER["REQUEST_METHOD"];
        }
	}

	if(! function_exists('url_link')) 
	{
		function url_link($uri = '', $protocol = NULL)
		{
			$ci = get_instance();
			$url = '';
			if($ci->session->userdata('idioma') == 'en') {
				$url = 'en/';
			} else {
				$url = 'es/';
			}
			return $ci->config->site_url($url.$uri, $protocol);
		}
	}

	if(!function_exists('active')) {
		function active($a, $b)  
		{
			$result = '';
			if($a == $b) {
				$result = 'class="active"';
			}
			return $result;
		}
	}

	if(!function_exists('active_idioma')) {
		function active_idioma($a, $b)  
		{
			$b = (!empty($b)) ? $b : 'es';
			$result = '';
			if($a == $b) {
				$result = 'class="active"';
			}
			echo $result;
		}
	}

	if(!function_exists('print_m')) {
		function print_m($array) {
			echo '<pre>';
			print_r($array);
			echo '</pre>';
		}
	}

	if(!function_exists('shuffle_assoc')) {
		function shuffle_assoc($list) { 
			if (!is_array($list)) return $list; 
			$keys = array_keys($list); 
			shuffle($keys); 
			$random = array(); 
			foreach ($keys as $key) {
				$random[$key] = $list[$key];
			} 
			return $random; 
		} 
	}

	if(!function_exists('url_img')) {
		function url_img($path = '', $file_name = '', $ext = '')  
		{
			if(!empty($path)) {
				$foto = $path.'/'.$file_name.$ext;
				$foto = str_replace('//', '/', $foto);
				$foto = str_replace('./', '/', $foto);
				$foto = site_url($foto);
			return $foto;
			} else {
				return site_url('assets/img/default.png');
			}
			
		}
	}

	if (! function_exists('create_slug'))
	{
		function create_slug($slug)
		{
			return strtolower(url_title(convert_accented_characters($slug), '-', TRUE));
		}
	}
?>