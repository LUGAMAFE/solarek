<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
    protected $folder_set = '/admin/partials/';
    protected $folder_archivos = '../public/assets/archivos/banners/';

	public function __construct(){
		parent::__construct();
        $this->class = strtolower(get_class());
        $this->load->model("administracion/Banners_Model");
        $this->load->model("administracion/Imagenes_Model");
    }

    public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Banners');
        $this->loadTemplatesComunes($datos);
        $this->template->asset_js('banners.js?v=2');
        $datos["accion"] = "MODIFICAR";
        $datos["datos"] = $this->Banners_Model->getFirst();
        $datos["action"] = site_url("administracion/banners/form");
        $this->template->write_view('content', $this->folder.'/banners/list', $datos);
        $this->template->render();
    }

    public function modificar(){
        $url_youtube = $this->input->post("url");
        $url_youtube_ingles = $this->input->post("url-ingles");
        $banner_type_home = $this->input->post("banner-type-home");
        $banner = $this->input->post("banner-img-home-info");
        $banner_movil = $this->input->post("banner-img-home-movil-info");
        $banner_ingles = $this->input->post("banner-img-home-ingles-info");
        $banner_ingles_movil = $this->input->post("banner-img-home-movil-ingles-info");
        $checkHome = $this->input->post("check-home");

        if(empty($banner_type_home)){
            $this->alertError("Error al guardar el banner.");
            redirect_back();
            return;
        }

        $datos["banner_type_home"] = $banner_type_home;

        preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url_youtube, $matches);
        
        if(isset($matches[1])){
            $datos["url_youtube_id_home"] = $url_youtube;
        }else{
            $this->alertError("Url Youtube Invalida. No se ha podido identificar el id del video de youtube seleccionado para el banner home.");
            redirect_back();
            return;
        }

        preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url_youtube_ingles, $matches);
        
        if(isset($matches[1])){
            $datos["url_youtube_id_home_ingles"] = $url_youtube_ingles;
        }else{
            $this->alertError("Url Youtube Inglés Invalida. No se ha podido identificar el id del video de youtube seleccionado para el banner home inglés.");
            redirect_back();
            return;
        }

        if($checkHome === "on"){
            $datos["banner_home_activo"] = 1;
        }else{
            $datos["banner_home_activo"] = 0;
        }

        //$nuevaImagenSma = false;
        $ids = $this->Banners_Model->obtenerIdsBanners();
        $update = true;
		if(!$ids){
			$update = false;
		}

        if(!empty($banner)){
            $banner = json_decode($banner)[0];
            //$nuevaImagen = true;
            if($this->Imagenes_Model->existeImagen($banner->uuid)){
                $this->alertError("Ya existe el archivo seleccionado que se intenta subir");
                redirect_back();
                return;
            }
            $idArchivoGuardado = $this->Imagenes_Model->guardarImagen($this->folder_archivos, $banner->uuid, $banner->name);
            if(!$idArchivoGuardado){
                $this->alertError("No se ha podido guardar el banner");
                redirect_back();
                return;
            }
            $datos["id_img_banner_home"] = $idArchivoGuardado;
        }

        if(!empty($banner_movil)){
            $banner_movil = json_decode($banner_movil)[0];
            //$nuevaImagen = true;
            if($this->Imagenes_Model->existeImagen($banner_movil->uuid)){
                $this->alertError("Ya existe el archivo seleccionado que se intenta subir");
                redirect_back();
                return;
            }
            $idArchivoGuardado = $this->Imagenes_Model->guardarImagen($this->folder_archivos, $banner_movil->uuid, $banner_movil->name);
            if(!$idArchivoGuardado){
                $this->alertError("No se ha podido guardar el banner");
                redirect_back();
                return;
            }
            $datos["id_img_banner_home_movil"] = $idArchivoGuardado;
        }

        if(!empty($banner_ingles)){
            $banner_ingles = json_decode($banner_ingles)[0];
            //$nuevaImagen = true;
            if($this->Imagenes_Model->existeImagen($banner_ingles->uuid)){
                $this->alertError("Ya existe el archivo seleccionado que se intenta subir");
                redirect_back();
                return;
            }
            $idArchivoGuardado = $this->Imagenes_Model->guardarImagen($this->folder_archivos, $banner_ingles->uuid, $banner_ingles->name);
            if(!$idArchivoGuardado){
                $this->alertError("No se ha podido guardar el banner");
                redirect_back();
                return;
            }
            $datos["id_img_banner_home_ingles"] = $idArchivoGuardado;
        }

        if(!empty($banner_ingles_movil)){
            $banner_ingles_movil = json_decode($banner_ingles_movil)[0];
            //$nuevaImagen = true;
            if($this->Imagenes_Model->existeImagen($banner_ingles_movil->uuid)){
                $this->alertError("Ya existe el archivo seleccionado que se intenta subir");
                redirect_back();
                return;
            }
            $idArchivoGuardado = $this->Imagenes_Model->guardarImagen($this->folder_archivos, $banner_ingles_movil->uuid, $banner_ingles_movil->name);
            if(!$idArchivoGuardado){
                $this->alertError("No se ha podido guardar el banner");
                redirect_back();
                return;
            }
            $datos["id_img_banner_home_movil_ingles"] = $idArchivoGuardado;
        }

        if(isset($datos)){
            $actualizado = $this->Banners_Model->actualizar($datos, $update);
            if(!$actualizado){
                $this->alertError("No se ha podido modificar los datos de los banners");
                redirect_back();
                return;
            }
        }

        if(isset($datos["id_img_banner_home"])){
            $this->Imagenes_Model->eliminarCarpetaImagenPorId($ids["id_img_banner_home"]);
            $this->Imagenes_Model->eliminarImagenPorId($ids["id_img_banner_home"]);
        }
        if(isset($datos["id_img_banner_home_movil"])){
            $this->Imagenes_Model->eliminarCarpetaImagenPorId($ids["id_img_banner_home_movil"]);
            $this->Imagenes_Model->eliminarImagenPorId($ids["id_img_banner_home_movil"]);
        }
        if(isset($datos["id_img_banner_home_ingles"])){
            $this->Imagenes_Model->eliminarCarpetaImagenPorId($ids["id_img_banner_home_ingles"]);
            $this->Imagenes_Model->eliminarImagenPorId($ids["id_img_banner_home_ingles"]);
        }
        if(isset($datos["id_img_banner_home_movil_ingles"])){
            $this->Imagenes_Model->eliminarCarpetaImagenPorId($ids["id_img_banner_home_movil_ingles"]);
            $this->Imagenes_Model->eliminarImagenPorId($ids["id_img_banner_home_movil_ingles"]);
        }
        
        $this->alertSuccess("Banners Modificados Correctamente");
        redirect('administracion');
    }

    public function imagenesEntrada(){
        $ids = $this->Banners_Model->obtenerIdsBanners();
        $uploaderId = $this->input->get("uploaderId");
        if($ids){
            if($uploaderId === "banner-img-home"){
                $archivo = $this->Imagenes_Model->obtenerImagenPorId($ids["id_img_banner_home"]);
            }else if($uploaderId === "banner-img-home-movil"){
                $archivo = $this->Imagenes_Model->obtenerImagenPorId($ids["id_img_banner_home_movil"]);
            }else if($uploaderId === "banner-img-home-ingles"){
                $archivo = $this->Imagenes_Model->obtenerImagenPorId($ids["id_img_banner_home_ingles"]);
            }else if($uploaderId === "banner-img-home-movil-ingles"){
                $archivo = $this->Imagenes_Model->obtenerImagenPorId($ids["id_img_banner_home_movil_ingles"]);
            }
            if(!is_null($archivo)){
                $ruta = $archivo["dir_img"] . $archivo["name_img"] . ".". $archivo["ext_img"];
                $archivo = (object) ["name" => $archivo["name_img"], "uuid" => $archivo["uuid_img"], "thumbnailUrl" => $ruta];
                $output = [$archivo];
            }else{
                $output = [];
            }
        }else{
            $output = [];
        }
        
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    public function sinImagenes(){
        $output = [];
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    protected function loadFineUploaderHelper(){
        $this->load->library('handlerfineuploader');
        $uploader = new Handlerfineuploader();
        // Specify max file size in bytes.
        $uploader->sizeLimit =  1 * 1024 * 1024 * 8;
        // If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
        $uploader->chunksFolder = $this->folder_archivos.'chunks';
        $this->uploader = $uploader;
    }
    
    public function subirImagen(){
        $this->loadFineUploaderHelper();

        $method = get_request_method();

        if ($method == "POST") {
            header("Content-Type: text/plain");

            // Assumes you have a chunking.success.endpoint set to point here with a query parameter of "done".
            // For example: /myserver/handlers/endpoint.php?done
            if (isset($_GET["done"])) {
                $result = $this->uploader->combineChunks($this->folder_archivos);
            }
            // Handles upload requests
            else {
                // Call handleUpload() with the name of the folder, relative to PHP's getcwd()
                $result = $this->uploader->handleUpload($this->folder_archivos);

                // To return a name used for uploaded file you can use the following line.
                $result["uploadName"] = $this->uploader->getUploadName();

                if($result["success"]){
                    $this->handleSubidaImagenEntrada($result["uuid"], $result["uploadName"]);
                }
            }

            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        // for delete file requests
        else if ($method == "DELETE") {
            $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $tokens = explode('/', $url);
            $uuid = $tokens[sizeof($tokens)-1];
            if($this->Imagenes_Model->existeImagen($uuid)){
                $result = array("success" => false,
                    "error" => "File not found! Unable to delete.".$url,
                    "path" => $uuid
                );
            }else{
                $result = $this->uploader->handleDelete($this->folder_archivos);
            }
            
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        else {
            header("HTTP/1.0 405 Method Not Allowed");
        }
    }

    public function handleSubidaImagenEntrada($uuid, $uploadName){

    }
}
