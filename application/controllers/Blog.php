<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
    protected $folder_set = '/admin/partials/';
    protected $folder_img_entradas = '../public/assets/img/entradas/';

	public function __construct(){
		parent::__construct();
        $this->class = strtolower(get_class());
        $this->load->model("administracion/Entradas_Model");
        $this->load->model("administracion/Imagenes_Model");
    }

    public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Blog');
		$this->loadTemplatesComunes($datos);
        $this->loadDataTables();
        $datos['entradas'] = $this->Entradas_Model->obtenerEntradasTabla();
        if($datos['entradas'] === FALSE || is_null($datos['entradas'])){
            $this->alertError("Error del servidor al tratar de recibir los datos de las entradas");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/blog/list', $datos);
		$this->template->render();
    }

    public function crear(){
        $datos['class'] = $this->class;
		$this->template->write('title', 'Admin Blog Modificar');
        $this->loadTemplatesComunes($datos);
        //Summernote 
        $this->template->asset_css('summernote/summernote-bs4.css');
        $this->template->asset_js('summernote/summernote-bs4.min.js');
        $this->template->asset_js('summernote/lang/summernote-es-ES.js');
        $this->template->asset_js('blog-entrada.js');
        $datos["accion"] = "CREAR";
        $datos["action"] = site_url("administracion/blog/crear-entrada/formInfoEntrada");
        $this->template->write_view('content', $this->folder.'/blog/entrada', $datos);
        $this->template->render();
    }

    public function eliminarEntrada($id){
        if(!$this->Entradas_Model->existeEntradaId($id)){
            $this->alertError("La entrada que tratas de eliminar no existe");
            redirect_back();
            return;
        }

        $idImagenes = $this->Entradas_Model->obtenerImagenesEntradaPorId($id);

        $eliminacionEntrada = $this->Entradas_Model->eliminarEntradaPorId($id);

        if(!$eliminacionEntrada){
            $this->alertError("No se pudo borrar la entrada $id del servidor");
            redirect_back();
            return;
        }

        $this->Imagenes_Model->eliminarCarpetaImagenPorId($idImagenes["img_big"]);
        $this->Imagenes_Model->eliminarCarpetaImagenPorId($idImagenes["img_small"]);

        $this->Imagenes_Model->eliminarImagenPorId($idImagenes["img_big"]);
        $this->Imagenes_Model->eliminarImagenPorId($idImagenes["img_small"]);

        if($this->Entradas_Model->countEntradas() <= 0){
            $this->Entradas_Model->resetEntradas();
        }

        $this->alertSuccess("Entrada {$id} Eliminada Correctamente");

        redirect('administracion/blog');
    }

    protected function eliminarImagenesEntradaDeServidor($smallUuid, $bigUuid){
        $this->Imagenes_Model->eliminarCarpetaImagenPorCarpetaUuid($this->folder_img_entradas,  $smallUuid);
        $this->Imagenes_Model->eliminarCarpetaImagenPorCarpetaUuid($this->folder_img_entradas,  $bigUuid);
    }

    public function crearEntrada(){
        $check_video = $this->input->post('check-video');
        $video_youtube = $this->input->post('video-youtube');
        $datos["nombre"] = $this->input->post('nombre');
        $datos["autor"] = $this->input->post('autor');
        $datos["titulo"] = $this->input->post('titulo');
        $datos["subtitulo"] = $this->input->post('subtitulo');
        $datos["resumen"] = $this->input->post('resumen');
        $datos["informacion"] = trim($this->input->post('informacion'));
        $datos["titulo_ingles"] = $this->input->post('titulo-ingles');
        $datos["subtitulo_ingles"] = $this->input->post('subtitulo-ingles');
        $datos["resumen_ingles"] = $this->input->post('resumen-ingles');
        $datos["informacion_ingles"] = trim($this->input->post('informacion-ingles'));
        $imagen_entrada_small = json_decode($this->input->post("imagen-entrada-small-info"));
        $imagen_entrada_big = json_decode($this->input->post("imagen-entrada-big-info"));

        $datos["check_video_entrada"] = ($check_video === "on") ? 1 : 0 ;

        if( $video_youtube === ""){
            $datos["url_youtube_id_entrada"] = "";
        }else{
            preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $video_youtube, $matches);
        
            if(isset($matches[1])){
                $datos["url_youtube_id_entrada"] = $video_youtube;
            }else{
                $this->alertError("Url Youtube Invalida. No se ha podido identificar el id del video de youtube seleccionado para el banner home.");
                redirect_back();
                return;
            }
        }


        if($this->Entradas_Model->existeEntrada($datos)){
            $this->eliminarImagenesEntradaDeServidor($imagen_entrada_small->uuid, $imagen_entrada_big->uuid);
            $this->alertError("Ya existe la entrada con el nombre o titulo seleccionado");
            redirect_back();
            return;
        }

        $idImagenGuardadaSmall = $this->Imagenes_Model->guardarImagen($this->folder_img_entradas, $imagen_entrada_small->uuid, $imagen_entrada_small->name);
        if(!$idImagenGuardadaSmall){
            $this->eliminarImagenesEntradaDeServidor($imagen_entrada_small->uuid, $imagen_entrada_big->uuid);
            $this->alertError("No se ha podido guardar la imagen de entrada pequeña");
            redirect_back();
            return;
        }

        $idImagenGuardadaBig = $this->Imagenes_Model->guardarImagen($this->folder_img_entradas, $imagen_entrada_big->uuid, $imagen_entrada_big->name);
        if(!$idImagenGuardadaBig){
            $this->eliminarImagenesEntradaDeServidor($imagen_entrada_small->uuid, $imagen_entrada_big->uuid);
            $this->alertError("No se ha podido guardar la imagen de entrada grande");
            redirect_back();
            return;
        }

        $datos["id_img_big"] = $idImagenGuardadaBig;
        $datos["id_img_small"] = $idImagenGuardadaSmall;
        $entradaGuardada = $this->Entradas_Model->guardarEntrada($datos);

        if(!$entradaGuardada){
            $this->Imagenes_Model->eliminarImagenPorId($datos["id_img_big"]);
            $this->Imagenes_Model->eliminarImagenPorId($datos["id_img_small"]);
            $this->eliminarImagenesEntradaDeServidor($imagen_entrada_small->uuid, $imagen_entrada_big->uuid);
            $this->alertError("No se ha podido guardar la entrada correctamente en el servidor");
            redirect_back();
            return;
        }
        $this->alertSuccess("Entrada {$datos["titulo"]} Guardada Correctamente");
        redirect('administracion/blog');
    }

    public function modificarEntrada($idEntrada){

        if(!$this->Entradas_Model->existeEntradaId($idEntrada)){
            $this->alertError("No existe la entrada con el id $idEntrada");
            redirect_back();
            return;
        }

        $check_video = $this->input->post('check-video');
        $video_youtube = $this->input->post('video-youtube');
        $datos["nombre"] = $this->input->post('nombre');
        $datos["autor"] = $this->input->post('autor');
        $datos["titulo"] = $this->input->post('titulo');
        $datos["subtitulo"] = $this->input->post('subtitulo');
        $datos["resumen"] = $this->input->post('resumen');
        $datos["informacion"] = trim($this->input->post('informacion'));
        $datos["titulo_ingles"] = $this->input->post('titulo-ingles');
        $datos["subtitulo_ingles"] = $this->input->post('subtitulo-ingles');
        $datos["resumen_ingles"] = $this->input->post('resumen-ingles');
        $datos["informacion_ingles"] = trim($this->input->post('informacion-ingles'));
        $imagen_entrada_small = $this->input->post("imagen-entrada-small-info");
        $imagen_entrada_big = $this->input->post("imagen-entrada-big-info");

        $datos["check_video_entrada"] = ($check_video === "on") ? 1 : 0 ;

        if( $video_youtube === ""){
            $datos["url_youtube_id_entrada"] = "";
        }else{
            preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $video_youtube, $matches);
        
            if(isset($matches[1])){
                $datos["url_youtube_id_entrada"] = $video_youtube;
            }else{
                $this->alertError("Url Youtube Invalida. No se ha podido identificar el id del video de youtube seleccionado para el banner home.");
                redirect_back();
                return;
            }
        }

        if($this->Entradas_Model->existeEntrada($datos, $idEntrada)){
            $this->alertError("Ya existe la entrada con el nombre o titulo seleccionado");
            //redirect_back();
            return false;
        }

        //$nuevaImagenSma = false;
        $idImagenes = $this->Entradas_Model->obtenerImagenesEntradaPorId($idEntrada);

        if(!empty($imagen_entrada_small)){
            $imagen_entrada_small = json_decode($imagen_entrada_small);
            //$nuevaImagen = true;
            if($this->Imagenes_Model->existeImagen($imagen_entrada_small->uuid)){
                $this->alertError("Ya existe la imagen seleecionada que se intenta subir");
                redirect_back();
                return;
            }
            $idImagenGuardadaSmall = $this->Imagenes_Model->guardarImagen($this->folder_img_entradas, $imagen_entrada_small->uuid, $imagen_entrada_small->name);
            if(!$idImagenGuardadaSmall){
                $this->alertError("No se ha podido guardar la imagen de entrada pequeña");
                redirect_back();
                return;
            }
            $datos["id_img_small"] = $idImagenGuardadaSmall;
        }

        if(!empty($imagen_entrada_big)){
            $imagen_entrada_big = json_decode($imagen_entrada_big);
            //$nuevaImagen = true;
            if($this->Imagenes_Model->existeImagen($imagen_entrada_big->uuid)){
                $this->alertError("Ya existe la imagen seleecionada que se intenta subir");
                redirect_back();
                return;
            }
            $idImagenGuardadaBig = $this->Imagenes_Model->guardarImagen($this->folder_img_entradas, $imagen_entrada_big->uuid, $imagen_entrada_big->name);
            if(!$idImagenGuardadaBig){
                $this->alertError("No se ha podido guardar la imagen de entrada pequeña");
                redirect_back();
                return;
            }
            $datos["id_img_big"] = $idImagenGuardadaBig;
        }

        $entradaActualizada = $this->Entradas_Model->actualizarEntrada($datos, $idEntrada);

        if(!$entradaActualizada){
            $this->alertError("No se ha podido modificar los datos de la entrada $idEntrada");
            redirect_back();
            return;
        }

        if(isset($datos["id_img_small"])){
            $this->Imagenes_Model->eliminarCarpetaImagenPorId($idImagenes["img_small"]);
            $this->Imagenes_Model->eliminarImagenPorId($idImagenes["img_small"]);
        }
        if(isset($datos["id_img_big"])){
            $this->Imagenes_Model->eliminarCarpetaImagenPorId($idImagenes["img_big"]);
            $this->Imagenes_Model->eliminarImagenPorId($idImagenes["img_big"]);
        }
        
        $this->alertSuccess("Entrada {$datos["titulo"]} Modificada Correctamente");
        redirect('administracion/blog');
    }

    public function imagenesEntrada($idEntrada){
        $idImagenes = $this->Entradas_Model->obtenerImagenesEntradaPorId($idEntrada);
        if($this->input->get("uploaderId") === "imagen-entrada-small"){
            $imagen = $this->Imagenes_Model->obtenerImagenPorId($idImagenes["img_small"]);
        }else{
            $imagen = $this->Imagenes_Model->obtenerImagenPorId($idImagenes["img_big"]);
        }
    
        $ruta = $imagen["dir_img"] . $imagen["name_img"] . " (medium).". $imagen["ext_img"];

        $imagen = (object) ["name" => $imagen["name_img"], "uuid" => $imagen["uuid_img"], "thumbnailUrl" => $ruta/*"thumbnailUrl" => $imagen["full_route_img"]*/];
        $output = [$imagen];
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    public function sinImagenes(){
        $output = [];
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }
    
    public function modificar($idEntrada){
        
        if(!$this->Entradas_Model->existeEntradaId($idEntrada)){
            $this->alertError("No existe la entrada con el id $idEntrada");
            redirect_back();
            return;
        }
        $datos['class'] = $this->class;
		$this->template->write('title', 'Admin Blog Modificar');
        $this->loadTemplatesComunes($datos);
        //Summernote 
        $this->template->asset_css('summernote/summernote-bs4.css');
        $this->template->asset_js('summernote/summernote-bs4.min.js');
        $this->template->asset_js('summernote/lang/summernote-es-ES.js');
        $this->template->asset_js('blog-entrada.js');
        $datos["accion"] = "MODIFICAR";
        $datos["action"] = site_url("administracion/blog/$idEntrada/formInfoEntrada");
        $datos["datos_entrada"] = $this->Entradas_Model->obtenerInfoEntradaModificarPorId($idEntrada);
        if($datos["datos_entrada"] === FALSE || is_null($datos["datos_entrada"])){
            $this->alertError("Error del servidor al tratar de recibir los datos de la entrada $idEntrada");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/blog/entrada', $datos);
        $this->template->render();
    }

    protected function loadFineUploaderHelper(){
        $this->load->library('handlerfineuploader');
        $uploader = new Handlerfineuploader();
        // Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $uploader->allowedExtensions = array('jpeg', 'jpg', 'png'); // all files types allowed by default
        // Specify max file size in bytes.
        $uploader->sizeLimit =  1 * 1024 * 1024 * 8;
        // If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
        $uploader->chunksFolder = $this->folder_img_entradas.'chunks';
        $this->uploader = $uploader;
    }
    
    public function subirImagen(){
        $this->loadFineUploaderHelper();

        $method = get_request_method();

        if ($method == "POST") {
            header("Content-Type: text/plain");

            // Assumes you have a chunking.success.endpoint set to point here with a query parameter of "done".
            // For example: /myserver/handlers/endpoint.php?done
            if (isset($_GET["done"])) {
                $result = $this->uploader->combineChunks($this->folder_img_entradas);
            }
            // Handles upload requests
            else {
                // Call handleUpload() with the name of the folder, relative to PHP's getcwd()
                $result = $this->uploader->handleUpload($this->folder_img_entradas);

                // To return a name used for uploaded file you can use the following line.
                $result["uploadName"] = $this->uploader->getUploadName();

                if($result["success"]){
                    $this->handleSubidaImagenEntrada($result["uuid"], $result["uploadName"]);
                }
            }

            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        // for delete file requests
        else if ($method == "DELETE") {
            $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $tokens = explode('/', $url);
            $uuid = $tokens[sizeof($tokens)-1];
            if($this->Imagenes_Model->existeImagen($uuid)){
                $result = array("success" => false,
                    "error" => "File not found! Unable to delete.".$url,
                    "path" => $uuid
                );
            }else{
                $result = $this->uploader->handleDelete($this->folder_img_entradas);
            }
            
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        else {
            header("HTTP/1.0 405 Method Not Allowed");
        }
    }

    public function handleSubidaImagenEntrada($uuid, $uploadName){

    }

}
