<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	protected $class = '';
	protected $folder = '/site';
	protected $folder_set = '/site/partials/';
	protected $object;
	protected $objectCliente;
	protected $objectD;
	protected $objectpedido;
	protected $url_recuperar;
	protected $principal = 1;
	protected $objectCat;
	protected $objectSubCat;

	protected $entradasPorPaginaBlog= 4;

	public function __construct(){
		parent::__construct();
		$this->class = strtolower(get_class());
	}


	public function index($datito = null){
		$datos['class'] = $this->class;
		$datos['banner'] = 1;
		$datos['active'] = 'inicio';
		$datos['datito'] = $datito;
		$this->template->write('title', 'Bienvenido');
		$this->template->asset_js('slick-1.8.0/slick/slick.min.js');
		$this->template->asset_js('numscroller/numscroller-1.0.js');
		$this->template->asset_js('tilt.js-master/src/tilt.jquery.js');
		$this->template->asset_js('tilter.js');
		$this->template->asset_js('youtubeBackground/src/jquery.youtubebackground.js');

		$this->template->asset_js('slider.js');
		$this->load->model("administracion/Banners_Model");
		$this->load->model("administracion/Imagenes_Model");
		$this->load->model("administracion/Testimonios_Model");
		$datos['testimonios'] = $this->Testimonios_Model->getTestimonios();

		$datos['datosBanner'] = $this->Banners_Model->getFirst();
		$home_activo = $datos['datosBanner']["banner_home_activo"];
		if($home_activo == 1){
			$banners = $this->Banners_Model->get_banners();
			$datos["banners"] = array(
				"img_banner_home" => $this->Imagenes_Model->obtenerImagenPorId($banners["id_img_banner_home"]),
				"img_banner_home_movil" => $this->Imagenes_Model->obtenerImagenPorId($banners["id_img_banner_home_movil"]),
				"img_banner_home_ingles" => $this->Imagenes_Model->obtenerImagenPorId($banners["id_img_banner_home_ingles"]),
				"img_banner_home_movil_ingles" => $this->Imagenes_Model->obtenerImagenPorId($banners["id_img_banner_home_movil_ingles"]),
			);
		}
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('banner', $this->folder_set.'banner', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/home/list', $datos);
		$this->template->render();
	}

	public function casosExito(){
		$datos['class'] = $this->class;
		$datos['banner'] = 1;
		$datos['active'] = 'casos-exito';
		$this->template->write('title', 'Casos Exito');
		$this->template->asset_js('slick-1.8.0/slick/slick.min.js');
		$this->template->asset_js('numscroller/numscroller-1.0.js');
		$this->template->asset_css('fancybox/dist/jquery.fancybox.min.css');
		$this->template->asset_js('fancybox/dist/jquery.fancybox.min.js');
		$this->template->asset_js('fancyConf.js');
		$this->template->asset_js('tilt.js-master/src/tilt.jquery.js');
		$this->template->asset_js('tilter.js');
		
		$this->load->model("administracion/Casos_Model");
		$this->load->model("administracion/Testimonios_Model");
		$datos['testimonios'] = $this->Testimonios_Model->getTestimonios();
		$datos['casos'] = $this->Casos_Model->getCasosExito();
		
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('banner', $this->folder_set.'banner', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/casos_Exito/list', $datos);
		$this->template->render();
	}

	public function financiamiento(){
		$datos['class'] = $this->class;
		$datos['banner'] = 1;
		$datos['active'] = 'credito';
		$this->template->write('title', 'Financiamiento');
		$this->template->asset_js('tilt.js-master/src/tilt.jquery.js');
		$this->template->asset_js('tilter.js');
		
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('banner', $this->folder_set.'banner', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/financiamiento/list', $datos);
		$this->template->render();
	}

	public function idioma($tipo = '') 
	{
		if($tipo == 'es') {
			$tipo = 'spanish';
		} else {
			$tipo = 'english';
		}
		$idioma = array(
			'language' => $tipo
		);
		$this->session->set_userdata($idioma);
		redirect($this->agent->referrer());
	}

	public function contacto(){
		$datos['class'] = $this->class;
		$datos['banner'] = 1;
		$datos['active'] = 'contacto';
		$this->template->write('title', 'Contacto');
		$this->template->asset_js('tilt.js-master/src/tilt.jquery.js');
		$this->template->asset_js('tilter.js');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('content', $this->folder.'/contacto/list', $datos);
		$this->template->render();
	}

	public function blog($entrada = null){
		$this->load->model("administracion/Entradas_Model");
		$this->load->model("administracion/Imagenes_Model");
		$this->template->asset_js('jquery.bootpag.min.js');
		$datos['class'] = $this->class;
		$datos['banner'] = 1;
		$datos['active'] = 'blog';
		$this->template->write('title', 'Blog');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		if(is_null($entrada)){
			$this->template->asset_js('paginador.js');
			$datos['entradas'] = $this->Entradas_Model->getEntradasBlogLimit(0, $this->entradasPorPaginaBlog);
			$datos['entradasRec'] = $this->Entradas_Model->getEntradasBlogLimitRand(0, 8);
			$datos['totalPaginas'] = ceil($this->Entradas_Model->countEntradas() / $this->entradasPorPaginaBlog);
			$this->template->write_view('content', $this->folder.'/blog/list', $datos);
		}else{
			$datos['entradas'] = $this->Entradas_Model->getEntradasBlogLimitRand(0, 8);
			$datos['entradaBlog'] = $this->Entradas_Model->getEntrada($entrada);
			$datos['description'] = $datos["entradaBlog"]["titulo_entrada"];
			$datos['imagePrev'] = site_url($datos["entradaBlog"]["big_full_route_img"]);
			$this->template->write_view('content', $this->folder.'/blog/entrada', $datos);
		}
		$this->template->render();
	}

	public function correoContacto(){
		$this->load->model("administracion/Correos_Model");
		$this->load->library('email');

        $data = array(
            'nombre' => $this->input->post('nombre'),
			'correo' => $this->input->post('correo'),
			'telefono' => $this->input->post('telefono'),
			'pago' => $this->input->post('pago-bimestre'),
			'residencia' => $this->input->post('residencia'),
			'mensaje' => $this->input->post('mensaje'),
		);
		
        $this->Correos_Model->guardarCorreo($data['correo']);

        $vista = $this->load->view($this->folder.'/template/correo_contacto', $data, true);
        //echo $vista;
        $sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $email
            //->addTo('info@grupobari.com.mx')
            ->addTo(["luisjavier004@hotmail.com", $this->config->item('correo')])
            ->setFrom($data['correo'])
            //->setReplyTo($this->config->item('correo'))
            ->setSubject('Mensaje enviado desde: '.$this->config->item('name_site'))
            ->setHtml($vista);
        $sendgrid->send($email);
        redirect_back();
	}

	public function descargables(){
		$datos['class'] = $this->class;
		$datos['banner'] = 1;
		$datos['active'] = 'descargables';
		$this->template->write('title', 'Descargables');
		$this->template->asset_js('modalDescargas.js');

		$this->load->model("administracion/Descargables_Model");
		$datos['descargas'] = $this->Descargables_Model->getDescargables();

		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('content', $this->folder.'/descargables/list', $datos);
		$this->template->render();
	}

	public function descarga(){
		$this->load->model("administracion/Descargables_Model");
		$this->load->model("administracion/Correos_Model");
		$titulo = $this->input->post('tituloArchivo');
		$correo = $this->input->post('correo');
		$archivo = $this->Descargables_Model->getDescargableTitulo($titulo);
		$this->Correos_Model->guardarCorreo($correo);
		$output = ["error" => false, "archivo" => $archivo["full_route_archivo"], "nombre" => $archivo["name_archivo"].".".$archivo["ext_archivo"]];
        $json = json_encode($output);
        echo $json;
	}

	public function blogPage($page){
		$this->load->model("administracion/Entradas_Model");
		$totalEntradas = $this->Entradas_Model->countEntradas();
		$numeroPaginas = ceil($totalEntradas/$this->entradasPorPaginaBlog);
		$limite = $page * $this->entradasPorPaginaBlog;
		$offset = $limite - $this->entradasPorPaginaBlog;
		$entradas = $this->Entradas_Model->getEntradasBlogLimit($offset, $this->entradasPorPaginaBlog);
		for ($i=0; $i < count($entradas); $i++) { 
			$entradas[$i]["ref_entrada"]  =  site_url('blog/'.create_slug($entradas[$i]["titulo_entrada"])."/".$entradas[$i]["id_entrada"]);
		}
		$output = ["error" => false, "mensaje" => $entradas];
        $json = json_encode($output);
        echo $json;
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
