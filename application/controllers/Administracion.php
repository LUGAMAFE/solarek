<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administracion extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';

	public function __construct(){
		parent::__construct();
		$this->class = strtolower(get_class());
	}

	public function cerrarSesion(){
		$this->session->sess_destroy();
		redirect('administracion/login');
	}

	public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Home');
		$this->loadTemplatesComunes($datos);
		$this->template->write_view('content', $this->folder.'/home/list', $datos);
		$this->template->render();
	}

	public function validateUserData($usuario, $password){
		if(is_null($usuario) || is_null($password) ){
			return true;
		}
		if(empty($usuario)){
			$this->setFlashData("login_message" , array("error" => 'Campo Usuario Vacio.', "errorExplicacion" => "Revisa que el campo de usuario este llenado correctamente"));
			return true;
		}
		if(empty($password)){
			$this->setFlashData("login_message" , array("error" => 'Campo Contraseña Vacio.', "errorExplicacion" => "Revisa que el campo de contraseña este llenado correctamente"));
			return true;
		} 
		return false;
	}
	public function login(){

		if($this->session->userdata('logged_in') ) {
            redirect('administracion');
		} 
		
		$usuario = $this->input->post('usuario');
		$password = $this->input->post('password');
		$recordar = $this->input->post('recordar');
		
		if( $this->validateUserData($usuario, $password) ){
			$this->cargarVistaLogin();
			return;
		}

		$this->load->model("administracion/Usuario_Model");
		$existe = $this->Usuario_Model->existeUsuario($usuario);

		if(!$existe){
			$this->setFlashData("login_message" , array("error" => 'Usuario Incorrecto.', "errorExplicacion" => "Revisa que el campo de usuario este llenado con datos correctos"));
			$this->cargarVistaLogin();
			return;
		}

		$userData = $this->Usuario_Model->getDatosUsuario($usuario);
		if($userData["contrasena_usuario"] !== $password){
			$this->setFlashData("login_message" , array("error" => 'Contraseña Invalida.', "errorExplicacion" => "Revisa que la contraseña coincida con el usuario."));
			$this->cargarVistaLogin();
			return;
		}

		if($recordar === "on"){
			$newdata = array(
				'username'  => $userData["nombre_usuario"],
				'email'     => $userData["correo_usuario"],
				'logged_in' => TRUE,
			);
		}else{
			$newdata = array(
				'username'  => $userData["nombre_usuario"],
				'email'     => $userData["correo_usuario"],
				'logged_in' => TRUE,
			);
		}
		$this->session->set_userdata($newdata);
		redirect('administracion');
	}

	protected function cargarVistaLogin(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Login');
		$this->template->asset_js('particlesjs/particles.js');
		$this->template->asset_js('particlesjs/app.js');
		$this->template->write('bodyclasses', 'vh-100 login-page d-flex justify-content-center align-items-center');
		$this->template->write_view('content', $this->folder.'/login/list', $datos);
		$this->template->render();
	}
}


// // ChartJS
// $this->template->asset_js('chart.js/Chart.min.js');
// //Sparkline
// $this->template->asset_js('sparklines/sparkline.js');
// // JQVMap
// $this->template->asset_css('jqvmap/jqvmap.min.css');
// $this->template->asset_js('jqvmap/jquery.vmap.min.js');
// //jQuery Knob Chart
// $this->template->asset_js('jqvmap/maps/jquery.vmap.world.js');
// $this->template->asset_js('jquery-knob/jquery.knob.min.js');
// //daterangepicker 
// $this->template->asset_css('daterangepicker/daterangepicker.css');
// $this->template->asset_js('moment/moment.min.js');
// $this->template->asset_js('daterangepicker/daterangepicker.js');
// //Tempusdominus Bootstrap 4
// $this->template->asset_css('tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css');
// $this->template->asset_js('tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js');
// //Summernote 
// $this->template->asset_css('summernote/summernote-bs4.css');
// $this->template->asset_js('summernote/summernote-bs4.min.js');
//AdminLTE dashboard demo (This is only for demo purposes)