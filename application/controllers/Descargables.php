<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Descargables extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
    protected $folder_set = '/admin/partials/';
    protected $folder_archivos = '../public/assets/archivos/descargables/';

	public function __construct(){
		parent::__construct();
        $this->class = strtolower(get_class());
        $this->load->model("administracion/Descargables_Model");
        $this->load->model("administracion/Imagenes_Model");
    }

    public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Descargables');
		$this->loadTemplatesComunes($datos);
        $this->loadDataTables();
        $datos['descargables'] = $this->Descargables_Model->obtenerDescargablesTabla();
        if($datos['descargables'] === FALSE || is_null($datos['descargables'])){
            $this->alertError("Error del servidor al tratar de recibir los datos de los descargables");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/descargables/list', $datos);
		$this->template->render();
    }

    public function crearVista(){
        $datos['class'] = $this->class;
		$this->template->write('title', 'Admin Descargables Modificar');
        $this->loadTemplatesComunes($datos);
        $this->template->asset_js('descargables.js');
        $datos["accion"] = "CREAR";
        $datos["action"] = site_url("administracion/descargables/crear/form");
        $this->template->write_view('content', $this->folder.'/descargables/descargable', $datos);
        $this->template->render();
    }

    public function modificarVista($id){
        
        if(!$this->Descargables_Model->existeDescargableId($id)){
            $this->alertError("No existe El descargable con el id $id");
            redirect_back();
            return;
        }
        $datos['class'] = $this->class;
		$this->template->write('title', 'Admin Descargables Modificar');
        $this->loadTemplatesComunes($datos);
        $this->template->asset_js('descargables.js');
        $datos["accion"] = "MODIFICAR";
        $datos["action"] = site_url("administracion/descargables/$id/form");
        $datos["datos"] = $this->Descargables_Model->obtenerInfoModificarPorId($id);
        if($datos["datos"] === FALSE || is_null($datos["datos"])){
            $this->alertError("Error del servidor al tratar de recibir los datos del descargable $id");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/descargables/descargable', $datos);
        $this->template->render();
    }

    public function eliminar($id){
        if(!$this->Descargables_Model->existeDescargableId($id)){
            $this->alertError("El descargable que tratas de eliminar no existe");
            redirect_back();
            return;
        }

        $idArchivo = $this->Descargables_Model->obtenerArchivoDescargablePorId($id);

        $eliminacion = $this->Descargables_Model->eliminarDescargablePorId($id);

        if(!$eliminacion){
            $this->alertError("No se pudo borrar el descargable $id del servidor");
            redirect_back();
            return;
        }

        $this->Imagenes_Model->eliminarCarpetaImagenPorId($idArchivo);
        $this->Imagenes_Model->eliminarImagenPorId($idArchivo);

        if($this->Descargables_Model->count() <= 0){
            $this->Descargables_Model->reset();
        }

        $this->alertSuccess("Descargable {$id} Eliminado Correctamente");

        redirect('administracion/descargables');
    }

    public function crear(){
        $datos["titulo"] = $this->input->post('titulo');
        $datos["descripcion"] = $this->input->post('descripcion');
        $descargable = json_decode($this->input->post("archivo-descargable-info"))[0];

        if($this->Descargables_Model->existeDescargable($datos)){
            $this->Imagenes_Model->eliminarCarpetaImagenPorCarpetaUuid($this->folder_archivos, $descargable->uuid);
            $this->alertError("Ya existe el descargable con el titulo seleccionado");
            redirect_back();
            return;
        }

        $idArchivoGuardado = $this->Imagenes_Model->guardarImagen($this->folder_archivos, $descargable->uuid, $descargable->name);
        if(!$idArchivoGuardado){
            $this->Imagenes_Model->eliminarCarpetaImagenPorCarpetaUuid($this->folder_archivos, $descargable->uuid);
            $this->alertError("No se ha podido guardar el archivo descargable");
            redirect_back();
            return;
        }

        $datos["id_archivo"] = $idArchivoGuardado;

        $descargableGuardado = $this->Descargables_Model->guardarDescargable($datos);

        if(!$descargableGuardado){
            $this->Imagenes_Model->eliminarImagenPorId($datos["id_archivo"]);
            $this->Imagenes_Model->eliminarCarpetaImagenPorCarpetaUuid($this->folder_archivos, $descargable->uuid);
            $this->alertError("No se ha podido guardar el descargable correctamente en el servidor");
            redirect_back();
            return;
        }
        $this->alertSuccess("Descargable {$datos["titulo"]} Guardado Correctamente");
        redirect('administracion/descargables');
    }

    public function modificar($id){

        if(!$this->Descargables_Model->existeDescargableId($id)){
            $this->alertError("No existe el descargable con el id $id");
            redirect_back();
            return;
        }

        $datos["titulo"] = $this->input->post('titulo');
        $datos["descripcion"] = $this->input->post('descripcion');
        $descargable = $this->input->post("archivo-descargable-info");

        if($this->Descargables_Model->existeDescargable($datos, $id)){
            $this->alertError("Ya existe el descargable con el titulo seleccionado");
            redirect_back();
            return;
        }

        //$nuevaImagenSma = false;
        $idArchivo = $this->Descargables_Model->obtenerArchivoDescargablePorId($id);

        if(!empty($descargable)){
            $descargable = json_decode($descargable)[0];
            //$nuevaImagen = true;
            if($this->Imagenes_Model->existeImagen($descargable->uuid)){
                $this->alertError("Ya existe el archivo seleccionado que se intenta subir");
                redirect_back();
                return;
            }
            $idArchivoGuardado = $this->Imagenes_Model->guardarImagen($this->folder_archivos, $descargable->uuid, $descargable->name);
            if(!$$idArchivoGuardado){
                $this->alertError("No se ha podido guardar el archivo descargable");
                redirect_back();
                return;
            }
            $datos["id_archivo"] = $idArchivoGuardado;
        }

        $actualizado = $this->Descargables_Model->actualizar($datos, $id);

        if(!$actualizado){
            $this->alertError("No se ha podido modificar los datos del descargable $id");
            redirect_back();
            return;
        }

        if(isset($datos["id_archivo"])){
            $this->Imagenes_Model->eliminarCarpetaImagenPorId($idArchivo);
            $this->Imagenes_Model->eliminarImagenPorId($idArchivo);
        }
        
        $this->alertSuccess("Descargable {$datos["titulo"]} Modificado Correctamente");
        redirect('administracion/descargables');
    }

    public function imagenesEntrada($id){
        $idDescargable = $this->Descargables_Model->obtenerArchivoDescargablePorId($id);
        if($this->input->get("uploaderId") === "archivo-descargable"){
            $archivo = $this->Imagenes_Model->obtenerImagenPorId($idDescargable);
        }
    
        $ruta = $archivo["dir_img"] . $archivo["name_img"] . ".". $archivo["ext_img"];

        $archivo = (object) ["name" => $archivo["name_img"], "uuid" => $archivo["uuid_img"]];
        $output = [$archivo];
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    public function sinImagenes(){
        $output = [];
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    protected function loadFineUploaderHelper(){
        $this->load->library('handlerfineuploader');
        $uploader = new Handlerfineuploader();
        // Specify max file size in bytes.
        $uploader->sizeLimit =  1 * 1024 * 1024 * 8;
        // If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
        $uploader->chunksFolder = $this->folder_archivos.'chunks';
        $this->uploader = $uploader;
    }
    
    public function subirImagen(){
        $this->loadFineUploaderHelper();

        $method = get_request_method();

        if ($method == "POST") {
            header("Content-Type: text/plain");

            // Assumes you have a chunking.success.endpoint set to point here with a query parameter of "done".
            // For example: /myserver/handlers/endpoint.php?done
            if (isset($_GET["done"])) {
                $result = $this->uploader->combineChunks($this->folder_archivos);
            }
            // Handles upload requests
            else {
                // Call handleUpload() with the name of the folder, relative to PHP's getcwd()
                $result = $this->uploader->handleUpload($this->folder_archivos);

                // To return a name used for uploaded file you can use the following line.
                $result["uploadName"] = $this->uploader->getUploadName();

                if($result["success"]){
                    $this->handleSubidaImagenEntrada($result["uuid"], $result["uploadName"]);
                }
            }

            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        // for delete file requests
        else if ($method == "DELETE") {
            $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $tokens = explode('/', $url);
            $uuid = $tokens[sizeof($tokens)-1];
            if($this->Imagenes_Model->existeImagen($uuid)){
                $result = array("success" => false,
                    "error" => "File not found! Unable to delete.".$url,
                    "path" => $uuid
                );
            }else{
                $result = $this->uploader->handleDelete($this->folder_archivos);
            }
            
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        else {
            header("HTTP/1.0 405 Method Not Allowed");
        }
    }

    public function handleSubidaImagenEntrada($uuid, $uploadName){

    }
}
