<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CasosExito extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
    protected $folder_set = '/admin/partials/';
    protected $folder_img_casos = '../public/assets/img/casos/';

	public function __construct(){
		parent::__construct();
        $this->class = strtolower(get_class());
        $this->load->model("administracion/Casos_Model");
        $this->load->model("administracion/Imagenes_Model");
    }

    public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Casos Exito');
		$this->loadTemplatesComunes($datos);
        $this->loadDataTables();
        $datos['casos'] = $this->Casos_Model->obtenerCasosTabla();
        if($datos['casos'] === FALSE || is_null($datos['casos'])){
            $this->alertError("Error del servidor al tratar de recibir los casos de exito");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/casos/list', $datos);
		$this->template->render();
	}

    public function crear(){
        $datos['class'] = $this->class;
		$this->template->write('title', 'Admin Casos Exito Crear');
        $this->loadTemplatesComunes($datos);
        $this->template->asset_js('galeria-caso.js');
        $datos["accion"] = "CREAR";
        $datos["action"] = site_url("administracion/casosExito/crear/form");
        $this->template->write_view('content', $this->folder.'/casos/caso', $datos);
        $this->template->render();
    }

    public function eliminar($id){
        if(!$this->Casos_Model->existeCasoId($id)){
            $this->alertError("El caso de exito que tratas de eliminar no existe");
            redirect_back();
            return;
        }

        $idImagen = $this->Casos_Model->obtenerImagenCasoPorId($id);

        $eliminacionCaso = $this->Casos_Model->eliminarCasoPorId($id);

        if(!$eliminacionCaso){
            $this->alertError("No se pudo borrar el caso $id del servidor");
            redirect_back();
            return;
        }


        $this->Imagenes_Model->eliminarCarpetaImagenPorId($idImagen); 

        $this->Imagenes_Model->eliminarImagenPorId($idImagen); 

        if($this->Casos_Model->countCasos() <= 0){
            $this->Casos_Model->resetCasos();
        }

        $this->alertSuccess("Caso {$id} Eliminado Correctamente");

        redirect('administracion/casosExito');
    }

    public function crearCaso(){
        $datos["titulo"] = $this->input->post('titulo');
        $datos["lugar"] = $this->input->post('lugar');
        $datos["informacion"] = $this->input->post('informacion');
        $datos["capacidad-instalada"] = $this->input->post('capacidad-instalada');
        $datos["produccion-anual"] = $this->input->post('produccion-anual');
        $datos["titulo-ingles"] = $this->input->post('titulo-ingles');
        $datos["lugar-ingles"] = $this->input->post('lugar-ingles');
        $datos["informacion-ingles"] = $this->input->post('informacion-ingles');
        $imagen_caso = json_decode($this->input->post("img-caso-info"))[0];

        if($this->Casos_Model->existeCaso($datos)){
            $this->Imagenes_Model->eliminarCarpetaImagenPorCarpetaUuid($this->folder_img_casos, $imagen_caso->uuid);
            $this->alertError("Ya existe el caso con el titulo seleccionado");
            redirect_back();
            return;
        }

        $idImagenGuardada = $this->Imagenes_Model->guardarImagen($this->folder_img_casos, $imagen_caso->uuid, $imagen_caso->name);
        if(!$idImagenGuardada){
            $this->Imagenes_Model->eliminarCarpetaImagenPorCarpetaUuid($this->folder_img_casos, $imagen_caso->uuid);
            $this->alertError("No se ha podido guardar la imagen del caso de exito");
            redirect_back();
            return;
        }

        $datos["id_img_caso"] = $idImagenGuardada;
        $casoGuardado = $this->Casos_Model->guardarCaso($datos);

        if(!$casoGuardado){
            $this->Imagenes_Model->eliminarImagenPorId($datos["id_img_caso"]);
            $this->Imagenes_Model->eliminarCarpetaImagenPorCarpetaUuid($this->folder_img_casos, $imagen_caso->uuid);
            $this->alertError("No se ha podido guardar el caso correctamente en el servidor");
            redirect_back();
            return;
        }
        $this->alertSuccess("Caso {$datos["titulo"]} Guardado Correctamente");
        redirect('administracion/casosExito');
    }

    public function modificarCaso($idCaso){

        if(!$this->Casos_Model->existeCasoId($idCaso)){
            $this->alertError("No existe el caso con el id $idCaso");
            redirect_back();
            return;
        }

        $datos["capacidad-instalada"] = $this->input->post('capacidad-instalada');
        $datos["produccion-anual"] = $this->input->post('produccion-anual');
        $datos["titulo"] = $this->input->post('titulo');
        $datos["lugar"] = $this->input->post('lugar');
        $datos["informacion"] = $this->input->post('informacion');
        $datos["titulo-ingles"] = $this->input->post('titulo-ingles');
        $datos["lugar-ingles"] = $this->input->post('lugar-ingles');
        $datos["informacion-ingles"] = $this->input->post('informacion-ingles');
        $imagen_caso = $this->input->post("img-caso-info");

        if($this->Casos_Model->existeCaso($datos, $idCaso)){
            $this->alertError("Ya existe el caso con el titulo seleccionado");
            //redirect_back();
            return false;
        }

        //$nuevaImagenSma = false;
        $idImagenCaso = $this->Casos_Model->obtenerImagenCasoPorId($idCaso);

        if(!empty($imagen_caso)){
            $imagen_caso = json_decode($imagen_caso)[0];
            //$nuevaImagen = true;
            if($this->Imagenes_Model->existeImagen($imagen_caso->uuid)){
                $this->alertError("Ya existe la imagen seleecionada que se intenta subir");
                redirect_back();
                return;
            }
            $idImagen_caso = $this->Imagenes_Model->guardarImagen($this->folder_img_casos, $imagen_caso->uuid, $imagen_caso->name);
            if(!$idImagen_caso){
                $this->alertError("No se ha podido guardar la imagen de entrada pequeña");
                redirect_back();
                return;
            }
            $datos["id_img_caso"] = $idImagen_caso;
        }

        $casoActualizado = $this->Casos_Model->actualizarCaso($datos, $idCaso);

        if(!$casoActualizado){
            $this->alertError("No se ha podido modificar los datos del caso $idCaso");
            redirect_back();
            return;
        }

        if(isset($datos["id_img_caso"])){
            $this->Imagenes_Model->eliminarCarpetaImagenPorId($idImagenCaso);
            $this->Imagenes_Model->eliminarImagenPorId($idImagenCaso);
        }
        
        $this->alertSuccess("Caso {$datos["titulo"]} Modificado Correctamente");
        redirect('administracion/casosExito');
    }

    public function imagenesEntrada($idCaso){
        $idImagenCaso = $this->Casos_Model->obtenerImagenCasoPorId($idCaso);
        if($this->input->get("uploaderId") === "img-caso"){
            $imagen = $this->Imagenes_Model->obtenerImagenPorId($idImagenCaso);
        }
    
        $ruta = $imagen["dir_img"] . $imagen["name_img"] . " (small).". $imagen["ext_img"];

        $imagen = (object) ["name" => $imagen["name_img"], "uuid" => $imagen["uuid_img"], "thumbnailUrl" => $ruta/*"thumbnailUrl" => $imagen["full_route_img"]*/];
        $output = [$imagen];
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    public function sinImagenes(){
        $output = [];
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }
    
    public function modificar($idCaso){
        if(!$this->Casos_Model->existeCasoId($idCaso)){
            $this->alertError("No existe el caso con el id $idCaso");
            redirect_back();
            return;
        }
        $datos['class'] = $this->class;
		$this->template->write('title', 'Admin Casos Exito Modificar');
        $this->loadTemplatesComunes($datos);
        $this->template->asset_js('galeria-caso.js');
        $datos["accion"] = "MODIFICAR";
        $datos["action"] = site_url("administracion/casosExito/$idCaso/form");
        $datos["datos_caso"] = $this->Casos_Model->obtenerInfoCasoModificarPorId($idCaso);
        if($datos["datos_caso"] === FALSE || is_null($datos["datos_caso"])){
            $this->alertError("Error del servidor al tratar de recibir los datos del caso $idCaso");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/casos/caso', $datos);
        $this->template->render();
    }

    protected function loadFineUploaderHelper(){
        $this->load->library('handlerfineuploader');
        $uploader = new Handlerfineuploader();
        // Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $uploader->allowedExtensions = array('jpeg', 'jpg', 'png'); // all files types allowed by default
        // Specify max file size in bytes.
        $uploader->sizeLimit =  1 * 1024 * 1024 * 8;
        // If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
        $uploader->chunksFolder = $this->folder_img_casos.'chunks';
        $this->uploader = $uploader;
    }
    
    public function subirImagen(){
        $this->loadFineUploaderHelper();

        $method = get_request_method();

        if ($method == "POST") {
            header("Content-Type: text/plain");

            // Assumes you have a chunking.success.endpoint set to point here with a query parameter of "done".
            // For example: /myserver/handlers/endpoint.php?done
            if (isset($_GET["done"])) {
                $result = $this->uploader->combineChunks($this->folder_img_casos);
            }
            // Handles upload requests
            else {
                // Call handleUpload() with the name of the folder, relative to PHP's getcwd()
                $result = $this->uploader->handleUpload($this->folder_img_casos);

                // To return a name used for uploaded file you can use the following line.
                $result["uploadName"] = $this->uploader->getUploadName();

                if($result["success"]){
                    $this->handleSubidaImagenEntrada($result["uuid"], $result["uploadName"]);
                }
            }

            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        // for delete file requests
        else if ($method == "DELETE") {
            $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $tokens = explode('/', $url);
            $uuid = $tokens[sizeof($tokens)-1];
            if($this->Imagenes_Model->existeImagen($uuid)){
                $result = array("success" => false,
                    "error" => "File not found! Unable to delete.".$url,
                    "path" => $uuid
                );
            }else{
                $result = $this->uploader->handleDelete($this->folder_img_casos);
            }
            
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        else {
            header("HTTP/1.0 405 Method Not Allowed");
        }
    }

    public function handleSubidaImagenEntrada($uuid, $uploadName){

    }

}
