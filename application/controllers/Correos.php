<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Correos extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
    protected $folder_set = '/admin/partials/';

	public function __construct(){
		parent::__construct();
        $this->class = strtolower(get_class());
        $this->load->model("administracion/Correos_Model");
    }

    public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Correos');
		$this->loadTemplatesComunes($datos);
        $this->loadDataTables();
        $datos['correos'] = $this->Correos_Model->obtenerCorreosTabla();
        if($datos['correos'] === FALSE || is_null($datos['correos'])){
            $this->alertError("Error del servidor al tratar de recibir los datos de los correos");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/correos/list', $datos);
		$this->template->render();
    }
}
