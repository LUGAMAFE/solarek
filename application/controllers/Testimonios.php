<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonios extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
    protected $folder_set = '/admin/partials/';
    protected $folder_img_casos = '../public/assets/img/testimonios/';

	public function __construct(){
		parent::__construct();
        $this->class = strtolower(get_class());
        $this->load->model("administracion/Testimonios_Model");
        $this->load->model("administracion/Imagenes_Model");
    }

    public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Testimonios');
		$this->loadTemplatesComunes($datos);
        $this->loadDataTables();
        $datos['testimonios'] = $this->Testimonios_Model->obtenerTestimonios();
        if($datos['testimonios'] === FALSE || is_null($datos['testimonios'])){
            $this->alertError("Error del servidor al tratar de recibir los testimonios");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/testimonios/list', $datos);
		$this->template->render();
	}

    public function crear(){
        $datos['class'] = $this->class;
		$this->template->write('title', 'Admin Testimonios Crear');
        $this->loadTemplatesComunes($datos);
        $this->template->asset_js('testimonios.js');
        $datos["accion"] = "CREAR";
        $datos["action"] = site_url("administracion/testimonios/crear/form");
        $this->template->write_view('content', $this->folder.'/testimonios/testimonio', $datos);
        $this->template->render();
    }

    public function eliminar($id){
        if(!$this->Testimonios_Model->existeId($id)){
            $this->alertError("El testimonio que tratas de eliminar no existe");
            redirect_back();
            return;
        }

        $idImagen = $this->Testimonios_Model->obtenerImagenPorId($id);

        $eliminacion = $this->Testimonios_Model->eliminarPorId($id);

        if(!$eliminacion){
            $this->alertError("No se pudo borrar el testimonio $id del servidor");
            redirect_back();
            return;
        }


        $this->Imagenes_Model->eliminarCarpetaImagenPorId($idImagen); 

        $this->Imagenes_Model->eliminarImagenPorId($idImagen); 

        if($this->Testimonios_Model->count() <= 0){
            $this->Testimonios_Model->reset();
        }

        $this->alertSuccess("Caso {$id} Eliminado Correctamente");

        redirect('administracion/testimonios');
    }

    public function crearTestimonio(){
        $datos["cliente"] = $this->input->post('cliente');
        $datos["descripcion"] = $this->input->post('descripcion');
        $datos["descripcion-ingles"] = $this->input->post('descripcion-ingles');

        $imagen = json_decode($this->input->post("img-testimonio-info"))[0];

        if($this->Testimonios_Model->existe($datos)){
            $this->Imagenes_Model->eliminarCarpetaImagenPorCarpetaUuid($this->folder_img_casos, $imagen->uuid);
            $this->alertError("Ya existe el testimonio con el cliente seleccionado");
            redirect_back();
            return;
        }

        $idImagenGuardada = $this->Imagenes_Model->guardarImagen($this->folder_img_casos, $imagen->uuid, $imagen->name);
        if(!$idImagenGuardada){
            $this->Imagenes_Model->eliminarCarpetaImagenPorCarpetaUuid($this->folder_img_casos, $imagen->uuid);
            $this->alertError("No se ha podido guardar la imagen de el testimonio");
            redirect_back();
            return;
        }

        $datos["img_testimonio"] = $idImagenGuardada;
        $guardado = $this->Testimonios_Model->guardar($datos);

        if(!$guardado){
            $this->Imagenes_Model->eliminarImagenPorId($datos["img_testimonio"]);
            $this->Imagenes_Model->eliminarCarpetaImagenPorCarpetaUuid($this->folder_img_casos, $imagen->uuid);
            $this->alertError("No se ha podido guardar el testimonio correctamente en el servidor");
            redirect_back();
            return;
        }
        $this->alertSuccess("Caso {$datos["cliente"]} Guardado Correctamente");
        redirect('administracion/testimonios');
    }

    public function modificarTestimonio($id){

        if(!$this->Testimonios_Model->existeId($id)){
            $this->alertError("No existe el testimonio con el id $id");
            redirect_back();
            return;
        }

        $datos["cliente"] = $this->input->post('cliente');
        $datos["descripcion"] = $this->input->post('descripcion');
        $datos["descripcion-ingles"] = $this->input->post('descripcion-ingles');

        $imagen = $this->input->post("img-testimonio-info");

        if($this->Testimonios_Model->existe($datos, $id)){
            $this->alertError("Ya existe el testimonio con el cliente seleccionado");
            redirect_back();
            return;
        }

        $idImagen = $this->Testimonios_Model->obtenerImagenPorId($id);

        if(!empty($imagen)){
            $imagen = json_decode($imagen)[0];
            //$nuevaImagen = true;
            if($this->Imagenes_Model->existeImagen($imagen->uuid)){
                $this->alertError("Ya existe la imagen seleecionada que se intenta subir");
                redirect_back();
                return;
            }
            $idImagenTestimonio = $this->Imagenes_Model->guardarImagen($this->folder_img_casos, $imagen->uuid, $imagen->name);
            if(!$idImagenTestimonio){
                $this->alertError("No se ha podido guardar la imagen");
                redirect_back();
                return;
            }
            $datos["img_testimonio"] = $idImagenTestimonio;
        }

        $actualizado = $this->Testimonios_Model->actualizarTestimonio($datos, $id);

        if(!$actualizado){
            $this->alertError("No se ha podido modificar los datos de el testimonio $id");
            redirect_back();
            return;
        }

        if(isset($datos["img_testimonio"])){
            $this->Imagenes_Model->eliminarCarpetaImagenPorId($idImagen);
            $this->Imagenes_Model->eliminarImagenPorId($idImagen);
        }
        
        $this->alertSuccess("Caso {$datos["titulo"]} Modificado Correctamente");
        redirect('administracion/testimonios');
    }

    public function imagenesEntrada($id){
        $idImagen = $this->Testimonios_Model->obtenerImagenPorId($id);
        if($this->input->get("uploaderId") === "img-testimonio"){
            $imagen = $this->Imagenes_Model->obtenerImagenPorId($idImagen);
        }
    
        $ruta = $imagen["dir_img"] . $imagen["name_img"] . " (small).". $imagen["ext_img"];

        $imagen = (object) ["name" => $imagen["name_img"], "uuid" => $imagen["uuid_img"], "thumbnailUrl" => $ruta/*"thumbnailUrl" => $imagen["full_route_img"]*/];
        $output = [$imagen];
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    public function sinImagenes(){
        $output = [];
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }
    
    public function modificar($id){
        if(!$this->Testimonios_Model->existeId($id)){
            $this->alertError("No existe el testimonio con el id $id");
            redirect_back();
            return;
        }
        $datos['class'] = $this->class;
		$this->template->write('title', 'Admin Testimonios Modificar');
        $this->loadTemplatesComunes($datos);
        $this->template->asset_js('testimonios.js');
        $datos["accion"] = "MODIFICAR";
        $datos["action"] = site_url("administracion/testimonios/$id/form");
        $datos["datos"] = $this->Testimonios_Model->obtenerInfoPorId($id);
        if($datos["datos"] === FALSE || is_null($datos["datos"])){
            $this->alertError("Error del servidor al tratar de recibir los datos del caso $id");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/testimonios/testimonio', $datos);
        $this->template->render();
    }

    protected function loadFineUploaderHelper(){
        $this->load->library('handlerfineuploader');
        $uploader = new Handlerfineuploader();
        // Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $uploader->allowedExtensions = array('jpeg', 'jpg', 'png'); // all files types allowed by default
        // Specify max file size in bytes.
        $uploader->sizeLimit =  1 * 1024 * 1024 * 8;
        // If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
        $uploader->chunksFolder = $this->folder_img_casos.'chunks';
        $this->uploader = $uploader;
    }
    
    public function subirImagen(){
        $this->loadFineUploaderHelper();

        $method = get_request_method();

        if ($method == "POST") {
            header("Content-Type: text/plain");

            // Assumes you have a chunking.success.endpoint set to point here with a query parameter of "done".
            // For example: /myserver/handlers/endpoint.php?done
            if (isset($_GET["done"])) {
                $result = $this->uploader->combineChunks($this->folder_img_casos);
            }
            // Handles upload requests
            else {
                // Call handleUpload() with the name of the folder, relative to PHP's getcwd()
                $result = $this->uploader->handleUpload($this->folder_img_casos);

                // To return a name used for uploaded file you can use the following line.
                $result["uploadName"] = $this->uploader->getUploadName();

                if($result["success"]){
                    $this->handleSubidaImagenEntrada($result["uuid"], $result["uploadName"]);
                }
            }

            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        // for delete file requests
        else if ($method == "DELETE") {
            $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $tokens = explode('/', $url);
            $uuid = $tokens[sizeof($tokens)-1];
            if($this->Imagenes_Model->existeImagen($uuid)){
                $result = array("success" => false,
                    "error" => "File not found! Unable to delete.".$url,
                    "path" => $uuid
                );
            }else{
                $result = $this->uploader->handleDelete($this->folder_img_casos);
            }
            
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        else {
            header("HTTP/1.0 405 Method Not Allowed");
        }
    }

    public function handleSubidaImagenEntrada($uuid, $uploadName){

    }

}
