<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Facebook\\' => array($vendorDir . '/facebook/graph-sdk/src/Facebook'),
    'Dotenv\\' => array($vendorDir . '/vlucas/phpdotenv/src'),
);
