$(document)
	.foundation()
	.ready(function() {
		/*$('.slider').slick({
			dots: true,
			infinite: true,
			speed: 500,
			fade: true,
			cssEase: 'linear',
			autoplay: true,
  			autoplaySpeed: 4000
		});*/
		if(typeof jQuery.fn.bpHS == 'function') {
			$('.bp-hs').bpHS({
				autoPlay: true,
				showControls: false,
				showBullets: false
			});
		}
	});
var resultAjax;
// The date picker (read the docs)
if(typeof jQuery.fn.pickadate != "undefined") { 
	$('.fecha').pickadate({
		max: new Date(),
		closeOnSelect: true,
		container: 'body',
		selectYears: true,
		selectMonths: true,
		selectYears: 100
	});
}


function formAjax(e, url_function) {
	resultAjax = {};
	var form = e.data('form');
	var datas = $(form).serialize();
	resultAjax = $.post(baseURL + url_function, datas, function(data) {
		//$('.load').show();
	});
}

function dateToYMD(date) {
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
}

function inputs(id, data, input)
{
	input.append(' <i class="fa fa-spinner fa-pulse loader"></i>');
	ajax_input(data, id);
}