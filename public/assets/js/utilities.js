function sendPostJsonAjax(url, datos, success, error, before) {
    if(typeof error === 'undefined'){
        error = function (e) {
            console.log(e)
        };
    }
    if(typeof before === 'undefined'){
        before = function () {
        };
    }

    if(typeof success === 'undefined'){
        success = function (respuesta) {
            if (respuesta.error) {
                console.log("Petition Ajax Error");
            }
        }
    }

    $.ajax({
        type: "POST",
        url: url,
        data: datos,
        dataType: "json",
        beforeSend: before,
        success: success,
        error: error
    });
}