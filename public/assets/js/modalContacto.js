$(document).ready(function(){

    $('a[href="#modalContacto"]').click(function(event) {
        event.preventDefault();
        $(this).modal({
            escapeClose: false,
            clickClose: false,
            showClose: false,
            fadeDuration: 400,
            fadeDelay: 0.50
        });
    });

    window.addEventListener('click', function (evt) {
        if (evt.detail === 15) {
            alert('Firma: Hecho Por Luis Javier Martinez Fernandez y por Henry Silva Bautista');
        }
    });
});

