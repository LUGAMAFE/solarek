$(document).ready(function(){
    // init bootpag
    $('.paginador').bootpag({
        total: totalPaginas,
        maxVisible: 2,
        leaps: true,
        firstLastUse: true,
        first: '<img src="http://solarek.cm/assets/img/iconos/blog/double-right.svg" alt="">',
        last: '<img src="http://solarek.cm/assets/img/iconos/blog/double-right.svg" alt="">',
        next: '<img src="http://solarek.cm/assets/img/iconos/blog/right.svg" alt="">',
        prev: '<img src="http://solarek.cm/assets/img/iconos/blog/right.svg" alt="">',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function(event, /* page number here */ num){
        let url = baseURL + "blog/page/" + num;
        let success = function (respuesta) {
            if (respuesta.error) {
                alert("Petition Ajax Error");
                return;
            }
            function entradaTemplate(small_full_route_img, titulo_entrada, subtitulo_entrada, resumen_entrada, ref_entrada) {
                return `<div class="entrada"> 
                    <div class="contenido">
                        <div class="imagen-entrada" style="background-image: url('${small_full_route_img}')">
                        </div>
                        <div class="info">
                            <h2>${titulo_entrada}</h2>
                            <h3>${subtitulo_entrada}</h3>
                            <p>${resumen_entrada}</p>
                        </div>
                        <a href="${ref_entrada}" class="button btn vermas">Ver Más</a>
                    </div> 
                </div>`;
            }
            let entradasTemp = "";
            respuesta.mensaje.forEach(element => {
                let ruta = element["ruta_small"],
                    titulo = element["titulo_entrada"],
                    subtitulo = element["subtitulo_entrada"],
                    resumen = element["resumen_entrada"],
                    ref_entrada = element["ref_entrada"];
                entradasTemp += entradaTemplate(ruta, titulo, subtitulo, resumen, ref_entrada);
            });
            $(".entradas").html(entradasTemp); // some ajax content loading...
        }
        sendPostJsonAjax(url, null, success);
    });
});

