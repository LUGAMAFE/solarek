$(document).ready(servicios);

function servicios() {

    var servicios = $(".item");

    var serviciosSize = servicios.length;
    $(servicios).hide();

    $(servicios[0]).show();

    var contadorMostrados = 1;

    $(servicios).on('click touch', siguienteServicio);

    setInterval(siguienteServicio, 5000);

    function siguienteServicio() {
        if(contadorMostrados >= serviciosSize){
            contadorMostrados = 0
        }
        $(servicios).hide();
        $(servicios[contadorMostrados]).fadeIn();
        contadorMostrados++;
    }

}

