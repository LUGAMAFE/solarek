$(document).ready(function(){

    $('a[href="#modalDescargas"]').click(function(event) {
        event.preventDefault();
        let $parent = $(this).parent();
        let titulo = $parent.find(".titulo").text();
        let descripcion = $parent.find(".descripcion").text();

        let $tituloModal = $("#modalDescargas").find(".titulo");
        let $descModal = $("#modalDescargas").find(".descripcion");
        $tituloModal.text(titulo);
        $descModal.text(descripcion);

        //console.log(titulo, descripcion, $tituloModal.text(), $descModal.text());

        $(this).modal({
            escapeClose: false,
            clickClose: false,
            showClose: false,
            fadeDuration: 400,
            fadeDelay: 0.50
        });
    });

    $(".obtener-archivo").click(function(e){
        if(checkInformFormValidation(document.getElementById("formDescargar"))){
            return;
        }
        let tituloModal = $("#modalDescargas").find(".titulo").text();
        let correoModal = $("#modalDescargas").find("#correo").val();
        let base_url = window.location.origin;
        let url = base_url + "/descargables/descarga";
        let datos = {
            tituloArchivo: tituloModal, 
            correo: correoModal,
        };

        let errorReload = function(){
            location.reload();
        };

        let success = function(respuesta){
            if(respuesta.error === true){
                errorReload();
            }else{
                downloadURI(respuesta.archivo);
            }
        };
        sendPostJsonAjax(url, datos, success, errorReload);
    });

    window.addEventListener('click', function (evt) {
        if (evt.detail === 15) {
            alert('Firma: Hecho Por Luis Javier Martinez Fernandez y por Henry Silva Bautista');
        }
    });
});

function downloadURI(uri) 
{
    var link = document.createElement("a");
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    link.remove();
}

function checkInformFormValidation(form){
    if (!form.checkValidity()) {
    // Create the temporary button, click and remove it
    var tmpSubmit = document.createElement('button')
    form.appendChild(tmpSubmit)
    tmpSubmit.click()
    form.removeChild(tmpSubmit)
    return true;
    } 
    return false;
}

function sendPostJsonAjax(url, datos, success, error, before) {
    if(typeof error === 'undefined'){
        error = function (e) {
            console.log(e)
        };
    }
    if(typeof before === 'undefined'){
        before = function () {
        };
    }

    if(typeof success === 'undefined'){
        success = function (respuesta) {
            if (respuesta.error) {
                alert("Error");
                location.reload();
            }
        }
    }

    $.ajax({
        type: "POST",
        url: url,
        data: datos,
        dataType: "json",
        beforeSend: before,
        success: success,
        error: error
    });
}

