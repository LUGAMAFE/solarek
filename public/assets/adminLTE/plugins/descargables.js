//FINE UPLOADERS LOGIC
let uploadersManager = new FineUploadersManager;

uploadersManager.createUploaders(".upload-area", {
    template: "qq-template-archivos",
    validation: {
        itemLimit: 1,
        sizeLimit: 1 * 1024 * 1024 * 8,  
    },
    messages: {
        onLeave: 'Los Archivos estan siendo subidos, si abandonas ahora las subidas seran canceladas.',
        typeError: 'El archivo {file} Tiene una extension invalida. </br>Extensiones Valida(s): {extensions}.',
        sizeError: 'El archivo {file} es demasiado grande. </br> El tamaño maximo por archivo es de {sizeLimit}.',
        tooManyItemsError: 'Demasiados archivos Seleccionados. </br> Solo Puedes subir uno.',
        //confirmMessage: 'Estas Seguro que quieres eliminar la imagen {filename} del servidor?',
    },
    scaling: {},
});

uploadersManager.esperarPromesas();

$('#submit-form').click(function(e) {
    let form = $('#form-entrada').eq(0)[0];
    let error = checkInformFormValidation(form);
    if(error){
        return;
    }
    let focusElement = null;

    function validarUploader(uploaderValidar, elemento){
        let uploadspasubir = uploaderValidar.getUploads({
            status: [qq.status.SUBMITTED]
        }).length;
    
        let uploadsyasubidas = uploaderValidar.getUploads({
            status: [qq.status.UPLOAD_SUCCESSFUL]
        }).length;
    
        if(uploadspasubir <= 0 && uploadsyasubidas <= 0){
            let closestRequerido = $("#"+elemento).tooltip({
                title: "Falta Un archivo para la entrada",
                trigger: "manual",
                placement: "auto"
            });
            closestRequerido.tooltip('show');
            error = true;
            focusElement = closestRequerido;
            return true;
        }
        return false;
    }
    let uploaders = uploadersManager.fineUploaders;

    uploaders.forEach(element => {
        validarUploader(element.uploader, element.asociatedId);
    });

    if(!error){
        uploaders.forEach(element => {
            let uploadspasubir = element.uploader.getUploads({
                status: [qq.status.SUBMITTED]
            }).length;
            let uploadsyasubidas = element.uploader.getUploads({
                status: [qq.status.UPLOAD_SUCCESSFUL]
            }).length;
            if(uploadsyasubidas > 0 && uploadspasubir <= 0){
                element.deferredAsociado.resolve();
            }else{
                element.uploader.uploadStoredFiles();
            }
        });
    }

    $(".requerido").off("focusin", hideToolTips);

    $(focusElement).focus();

    $(".requerido").on("focusin", hideToolTips);
});