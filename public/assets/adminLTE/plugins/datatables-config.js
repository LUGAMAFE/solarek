$.extend( true, $.fn.dataTable.defaults, {
    "language": {
        "emptyTable":       "No hay datos por mostrar en la tabla",
        "lengthMenu":       "Mostrar _MENU_ Registros por pagina.",
        "zeroRecords":      "Ningun registro encontrado - Lo sentimos!",
        "info":             "Mostrando Pagina _PAGE_ de _PAGES_",
        "infoEmpty":        "No hay registros para mostrar.",
        "infoFiltered":     "(Filtrado de _MAX_ registros totales)",
        "loadingRecords":   "Cargando...",
        "processing":       "Procesando...",
        "search":           "Buscar:",
        "paginate": {
            "first":        "First",
            "last":         "Last",
            "next":         "Siguiente",
            "previous":     "Anterior"
        },
        "select": {
            "rows": {
                _: "   %d Filas seleccionadas",
                0: "",
                1: "   Una fila seleccioanda"
            }
        }
    },
} );