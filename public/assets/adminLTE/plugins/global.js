$(".requerido").attr('tabindex', '0');

function hideToolTips() {
    $(this).tooltip('hide');
}

function checkInformFormValidation(form){
    if (!form.checkValidity()) {
    // Create the temporary button, click and remove it
    var tmpSubmit = document.createElement('button')
    form.appendChild(tmpSubmit)
    tmpSubmit.click()
    form.removeChild(tmpSubmit)
    return true;
    } 
    return false;
}

$(".cerrar-sesion").click( function(){  
    new jBox('Confirm', {
        content: 'Estas seguro que realmente deseas cerrar la sesion?',
        confirmButton: 'Si Cerrar',
        cancelButton: 'No Cancelar',
        confirm: function(){
            window.location.replace(baseURL + "administracion/cerrarSesion");
        },
    }).open();
});

// $(".pantalla-completa").click( function(){  
//     if(fullscreen){

//     }else{
//         openFullscreen();
//     }
// });

// var elem = document.documentElement;
// let fullscreen = false;


// /* View in fullscreen */
// function openFullscreen() {
//     fullscreen = true;
//   if (elem.requestFullscreen) {
//     elem.requestFullscreen();
//   } else if (elem.mozRequestFullScreen) { /* Firefox */
//     elem.mozRequestFullScreen();
//   } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
//     elem.webkitRequestFullscreen();
//   } else if (elem.msRequestFullscreen) { /* IE/Edge */
//     elem.msRequestFullscreen();
//   }
// }

// /* Close fullscreen */
// function closeFullscreen() {
//   if (document.exitFullscreen) {
//     document.exitFullscreen();
//   } else if (document.mozCancelFullScreen) { /* Firefox */
//     document.mozCancelFullScreen();
//   } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
//     document.webkitExitFullscreen();
//   } else if (document.msExitFullscreen) { /* IE/Edge */
//     document.msExitFullscreen();
//   }
// }

// $(document).on("ready", function(){
//     openFullscreen();
// });


const FineUploadersManager = function(){
    this.defferreds = [];
    this.fineUploaders = [];
    this.addUploader = function(uploader,uploaderId, deferredObject){
        this.addPromesa(deferredObject);
        this.fineUploaders.unshift({
            "uploader" : uploader,
            "asociatedId" : uploaderId,
            "deferredAsociado" : deferredObject,
        });
    };
    this.addPromesa = function(promesa) {
        this.defferreds.push(promesa);
    };
    this.esperarPromesas = function(){
        $.when.apply($, this.defferreds).done(function(){
            $('#form-entrada').submit();
        });
    };
    this.createUploaders = function(selector, config = {}){
        if(typeof selector !== 'string'){
            throw "El paramentro selector otorgado para crear los elementos FineUploaders es invalido.";
        }
        if(typeof config !== 'object'){
            throw "La paramentro de configuracion para crear los elementos FineUploaders es invalido.";
        }
        $(selector).each(function(index, element) {
            let uploaderId = $(this).attr('id');
            if(!uploaderId){
                throw "El elemento seleccionado no contiene un id para asociar a un objecto FineUploader";
            }
            let deferredObject = $.Deferred();
            let primeramodificacion = true;
            let defaults = new FineUploaderDefaults(uploaderId, element, deferredObject, primeramodificacion);
            let configuracion = {...defaults, ...config};
            let uploader = new qq.FineUploader(configuracion);
            uploadersManager.addUploader(uploader,uploaderId, deferredObject);
        });
    };
};

const FineUploaderDefaults = function(uploaderId, element, deferredObject, primeramodificacion){
    this.template = "qq-template";
    this.debug = false;
    this.element = element;
    this.autoUpload = false;
    this.session = {
        endpoint: siteURL + "imagenesEntrada",
        params: {
            uploaderId: uploaderId,
        },
    };
    this.multiple = false;
    this.warnBeforeUnload = true;
    this.request = {
        endpoint: siteURL + "subirImagen",
        params: {
            uploaderId: uploaderId,
        },
    };
    this.deleteFile = {
        enabled: true,
        //forceConfirm: true,
        endpoint: siteURL + "subirImagen",
        params: {
            uploaderId: uploaderId,
        },
    };
    this.resume = {
        enabled: true,
    };
    this.scaling = {
        hideScaled: true,
        //includeExif: true,
        sizes: [
            {name: "small", maxSize: 200},
            {name: "medium", maxSize: 600},
            {name: "large", maxSize: 1200}
        ]
    };
    this.validation = {
        itemLimit: 4,
        allowedExtensions: ['jpeg', 'jpg', 'png'],
        sizeLimit: 1 * 1024 * 1024 * 8,  
    };
    this.text =  {
        defaultResponseError: 'Un error desconocido a sucedido.'
    };
    this.messages = {
        onLeave: 'Los Archivos estan siendo subidos, si abandonas ahora las subidas seran canceladas.',
        typeError: 'El archivo {file} Tiene una extension invalida. </br>Extensiones Valida(s): {extensions}.',
        sizeError: 'El archivo {file} es demasiado grande. </br> El tamaño maximo por imagen es de {sizeLimit}.',
        tooManyItemsError: 'Demasiadas Imagenes Seleccioandas. </br> Solo Puedes subir una imagen.',
        //confirmMessage: 'Estas Seguro que quieres eliminar la imagen {filename} del servidor?',
    };
    this.onComplete = function(id, fileName, responseJSON){
        if( responseJSON.success ){
            //do something 
            $.log(responseJSON.filename, responseJSON.filesize);
        }
    };
    this.callbacks = {
        onSessionRequestComplete : function(response, success){
            if(response.length <= 0){
                primeramodificacion = false;
            }
        },
        onAllComplete : function(succeeded, failed){
            if(primeramodificacion === true){
                primeramodificacion = false;
                return;
            }
            if(failed.length <= 0 && succeeded.length > 0){
                let parentId;
                let jsonImgInfo = [];
                while(succeeded.length > 0){
                    parentId = this.getParentId(succeeded[0]);
                    if(parentId == null){
                        parentId = succeeded[0];
                    }
                    let deleteItems = [];
                    for(let j = 0; j < succeeded.length; j++){
                        let actualId = this.getParentId(succeeded[j]);
                        if(actualId == null){
                            actualId = succeeded[j];
                        }
                        if(parentId == actualId){
                            deleteItems.push(succeeded[j]);
                        }
                    }

                    for (let x = 0; x < deleteItems.length; x++) {
                        var indexDelete = succeeded.indexOf(deleteItems[x]);
                        if (indexDelete !== -1) succeeded.splice(indexDelete, 1);
                    }

                    jsonImgInfo.push({ uuid: this.getUuid(parentId), name: this.getName(parentId)});
                }
                if(parentId === null){
                    return;
                }
                let data = JSON.stringify(jsonImgInfo);
                $('input[name="' + uploaderId + '-info"]').val(data);
                deferredObject.resolve();
            }else if(succeeded.length > 0) {
                succeeded.forEach(element => {
                    this.deleteFile(element);
                });
                this.cancelAll();
            }    
        },
        onCancel : function(id, name){
            let idElement = id;
            let parent = this.getParentId(idElement);
            if(parent === null){
                let uploads = this.getUploads({
                    status: [qq.status.SUBMITTED]
                })
                for(let i = 0; i < uploads.length; i++){
                    let actualId = this.getParentId(uploads[i]["id"]);
                    if(actualId == idElement){
                        this.cancel(uploads[i]["id"]);
                    }
                }
            }
        },
        onDeleteComplete: function (id) {
            let i = --id,
            limit = id-2;
            for(i; (i >= limit) && (i >= 0); i--){
                this.cancel(i);
            }
        },
        onError: function(id, name, errorReason, xhrOrXdr) {
            if(xhrOrXdr){
                let modal = new jBox('Modal', {
                    // width: 300,
                    // height: 100,
                    showCountdown: true,
                    autoClose: 10000,
                    delayOnHover: true,
                    title: 'Error!',
                    content: qq.format("Error en archivo numero {} - {}. </br>Razon: {}", id, name, errorReason), 
                    onClose: function() {
                        setTimeout(() => {
                            modal.destroy();
                        }, 500);
                    }
                }).open();
                this.cancelAll();
            }
        }
    };
    this.showMessage = function(message) {
        let modal = new jBox('Modal', {
            // width: 300,
            // height: 100,
            showCountdown: true,
            autoClose: 10000,
            delayOnHover: true,
            title: 'Error!',
            content: message, 
            onClose: function() {
                setTimeout(() => {
                    modal.destroy();
                }, 500);
            }
        }).open();
    };
};
