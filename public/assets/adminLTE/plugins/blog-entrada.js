$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    e.target // newly activated tab
    e.relatedTarget // previous active tab
    let $target = $(e.target),
    targetId = $target.attr("id");
    $('a[data-toggle="tab"]').toggleClass("bg-light");
    $(".seleccionado").hide();
    switch (targetId) {
        case "esp-tab":
            $(".act-text").text("Español");
            break;
        case "eng-tab":
            $(".act-text").text("Inglés");
            break;    
        default:
            break;
    }
    $target.find(".seleccionado").show();
})

$('.summer').summernote({
    height: "540",                 // set editor height
    minHeight: 100,             // set minimum height of editor
    toolbar: [
        ['style', ['style', 'clear']],
        ['font', ['bold', 'underline', 'italic']],
        //['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link', /*'picture',*/ 'video', 'hr']],
        ['misc', ['fullscreen', 'codeview', 'undo', 'redo']],
    ],
    codemirror: { // codemirror options
        theme: 'monokai'
    },
    fontNames: ['Arial', 'Raleway'],
    fontNamesIgnoreCheck: ['Raleway'],
    lang: 'es-ES',
    // onKeyup: function(e) {
    //     $('textarea[name="informacion"]').val(this.code());
    // },
    placeholder: 'Escribe la informacion de la entrada aqui!',
    callbacks: {
        onPaste: function(e) {
            let clipboard = ((e.originalEvent || e).clipboardData || window.clipboardData),
            bufferText = clipboard.getData('text/html');
            if(bufferText.length > 0){
                bufferText = $(bufferText);
            }else{
                bufferText = clipboard.getData('text');
            }
            e.preventDefault();
            var div = $('<div />');
            div.append(bufferText);
            div.find('*').removeAttr('style').remove("style");
            document.execCommand('insertHtml', false, div.html());
        }
      }
});

$('.note-editable').css('font-family','Raleway');


//FINE UPLOADERS LOGIC
let fineUploaders = [];

let promesasSubidaImagenes = {
    defferreds : [],
    addPromesa: function(promesa) {
        this.defferreds.push(promesa);
    },
    esperarPromesas : function(){
        $.when.apply($, this.defferreds).done(function(){
            $('#form-entrada').submit();
        });
    }
}; 

$('#imagen-entrada-small, #imagen-entrada-big').each(function(index, element) {
    let uploaderId = $(this).attr('id');
    let deferredObject = $.Deferred();
    promesasSubidaImagenes.addPromesa(deferredObject);
    let uploader = new qq.FineUploader({
        //debug: true,
        element: element,
        autoUpload: false,
        session: {
            endpoint: siteURL + "imagenesEntrada",
            params: {
                uploaderId: uploaderId,
            },
        },
        multiple: false,
        warnBeforeUnload: true,
        request: {
            endpoint: siteURL + "subirImagen",
            params: {
                uploaderId: uploaderId,
            },
        },
        deleteFile: {
            enabled: true,
            //forceConfirm: true,
            endpoint: siteURL + "subirImagen",
        },
        resume: {
            enabled: true,
        },
        scaling: {
            hideScaled: true,
            //includeExif: true,
            sizes: [
                {name: "small", maxSize: 100},
                {name: "medium", maxSize: 300},
                {name: "large", maxSize: 1200}
            ]
        },
        validation: {
            itemLimit: 4,
            allowedExtensions: ['jpeg', 'jpg', 'png'],
            sizeLimit: 1 * 1024 * 1024 * 8,  
        },
        onComplete: function(id, fileName, responseJSON){
            if( responseJSON.success ){
                //do something 
                $.log(responseJSON.filename, responseJSON.filesize);
            }
        },
        callbacks: {
            onAllComplete : function(succeeded, failed){
                if(failed.length <= 0 && succeeded.length > 0){
                    let parentId;
                    for(let i = 0; i <= succeeded.length; i++){
                        let actualId = this.getParentId(succeeded[i]);
                        if(i == 0){
                            parentId = actualId;
                        }else if( actualId != parentId){
                        }
                    }
                    if(parentId === null){
                        return;
                    }
                    let jsonImgInfo = { uuid: this.getUuid(parentId), name: this.getName(parentId)};
                    let data = JSON.stringify(jsonImgInfo);
                    $('input[name="' + uploaderId + '-info"]').val(data);
                    deferredObject.resolve();
                }else if(succeeded.length > 0) {
                    succeeded.forEach(element => {
                        this.deleteFile(element);
                    });
                    this.cancelAll();
                }    
            },
            onCancel : function(id, name){
                let idElement = id;
                let parent = this.getParentId(idElement);
                if(parent === null){
                    let uploads = uploader.getUploads({
                        status: [qq.status.SUBMITTED]
                    })
                    for(let i = 0; i < uploads.length; i++){
                        let actualId = this.getParentId(uploads[i]["id"]);
                        if(actualId == idElement){
                            this.cancel(uploads[i]["id"]);
                        }
                    }
                }
            },
            onDeleteComplete: function (id) {
                let i = --id,
                limit = id-2;
                for(i; (i >= limit) && (i >= 0); i--){
                    this.cancel(i);
                }
            },
            onError: function(id, name, errorReason, xhrOrXdr) {
                if(xhrOrXdr){
                    let modal = new jBox('Modal', {
                        // width: 300,
                        // height: 100,
                        showCountdown: true,
                        autoClose: 10000,
                        delayOnHover: true,
                        title: 'Error!',
                        content: qq.format("Error en archivo numero {} - {}. </br>Razon: {}", id, name, errorReason), 
                        onClose: function() {
                            setTimeout(() => {
                                modal.destroy();
                            }, 500);
                        }
                    }).open();
                    this.cancelAll();
                }
            }
        },
        text: {
            defaultResponseError: 'Un error desconocido a sucedido.'
        },
        messages: {
            onLeave: 'Los Archivos estan siendo subidos, si abandonas ahora las subidas seran canceladas.',
            typeError: 'El archivo {file} Tiene una extension invalida. </br>Extensiones Valida(s): {extensions}.',
            sizeError: 'El archivo {file} es demasiado grande. </br> El tamaño maximo por imagen es de {sizeLimit}.',
            tooManyItemsError: 'Demasiadas Imagenes Seleccioandas. </br> Solo Puedes subir una imagen.',
            //confirmMessage: 'Estas Seguro que quieres eliminar la imagen {filename} del servidor?',
        },
        showMessage: function(message) {
            let modal = new jBox('Modal', {
                // width: 300,
                // height: 100,
                showCountdown: true,
                autoClose: 10000,
                delayOnHover: true,
                title: 'Error!',
                content: message, 
                onClose: function() {
                    setTimeout(() => {
                        modal.destroy();
                    }, 500);
                }
            }).open();
        }
    });

    fineUploaders.unshift({
        "uploader" : uploader,
        "asociatedId" : uploaderId,
        "deferredAsociado" : deferredObject,
    });
});

promesasSubidaImagenes.esperarPromesas();

$('#submit-form').click(function(e) {
    let form = $('#form-entrada').eq(0)[0];
    let error = checkInformFormValidation(form);
    if(error){
        return;
    }
    let focusElement = null;
    
    function validarSummer(selector){
        let summerempty = $(selector).summernote('isEmpty') || $(selector).summernote('code').trim() === "";

        if(summerempty){
            let closestRequerido = $(selector).closest(".requerido").tooltip({
                title: "El Campo Esta Incompleto",
                trigger: "manual",
                placement: "auto"
            });
            closestRequerido.tooltip('show');
            error = true;
            focusElement = closestRequerido;
        }
    }

    validarSummer(".summer.espanol");

    validarSummer(".summer.ingles");

    function validarUploader(uploaderValidar, elemento){
        let uploadspasubir = uploaderValidar.getUploads({
            status: [qq.status.SUBMITTED]
        }).length;
    
        let uploadsyasubidas = uploaderValidar.getUploads({
            status: [qq.status.UPLOAD_SUCCESSFUL]
        }).length;
    
        if(uploadspasubir <= 0 && uploadsyasubidas <= 0){
            let closestRequerido = $("#"+elemento).tooltip({
                title: "Falta Una Imagen para la entrada",
                trigger: "manual",
                placement: "auto"
            });
            closestRequerido.tooltip('show');
            error = true;
            focusElement = closestRequerido;
            return true;
        }
        return false;
    }

    fineUploaders.forEach(element => {
        validarUploader(element.uploader, element.asociatedId);
    });

    if(!error){
        fineUploaders.forEach(element => {
            let uploadspasubir = element.uploader.getUploads({
                status: [qq.status.SUBMITTED]
            }).length;
            let uploadsyasubidas = element.uploader.getUploads({
                status: [qq.status.UPLOAD_SUCCESSFUL]
            }).length;
            if(uploadsyasubidas > 0 && uploadspasubir <= 0){
                element.deferredAsociado.resolve();
            }else{
                element.uploader.uploadStoredFiles();
            }
        });
    }

    $(".requerido").off("focusin", hideToolTips);

    $(focusElement).focus();

    $(".requerido").on("focusin", hideToolTips);
});
