$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    e.target // newly activated tab
    e.relatedTarget // previous active tab
    let $target = $(e.target),
    targetId = $target.attr("id");
    $('a[data-toggle="tab"]').toggleClass("bg-light");
    $(".seleccionado").hide();
    switch (targetId) {
        case "esp-tab":
            $(".act-text").text("Español");
            break;
        case "eng-tab":
            $(".act-text").text("Inglés");
            break;    
        default:
            break;
    }
    $target.find(".seleccionado").show();
})

//FINE UPLOADERS LOGIC
let uploadersManager = new FineUploadersManager;

uploadersManager.createUploaders(".upload-area");

uploadersManager.esperarPromesas();

$('#submit-form').click(function(e) {
    let form = $('#form-entrada').eq(0)[0];
    let error = checkInformFormValidation(form);
    if(error){
        return;
    }
    let focusElement = null;

    function validarUploader(uploaderValidar, elemento){
        let uploadspasubir = uploaderValidar.getUploads({
            status: [qq.status.SUBMITTED]
        }).length;
    
        let uploadsyasubidas = uploaderValidar.getUploads({
            status: [qq.status.UPLOAD_SUCCESSFUL]
        }).length;
    
        if(uploadspasubir <= 0 && uploadsyasubidas <= 0){
            let closestRequerido = $("#"+elemento).tooltip({
                title: "Falta Una Imagen para la entrada",
                trigger: "manual",
                placement: "auto"
            });
            closestRequerido.tooltip('show');
            error = true;
            focusElement = closestRequerido;
            return true;
        }
        return false;
    }
    let uploaders = uploadersManager.fineUploaders;

    uploaders.forEach(element => {
        validarUploader(element.uploader, element.asociatedId);
    });

    if(!error){
        uploaders.forEach(element => {
            let uploadspasubir = element.uploader.getUploads({
                status: [qq.status.SUBMITTED]
            }).length;
            let uploadsyasubidas = element.uploader.getUploads({
                status: [qq.status.UPLOAD_SUCCESSFUL]
            }).length;
            if(uploadsyasubidas > 0 && uploadspasubir <= 0){
                element.deferredAsociado.resolve();
            }else{
                element.uploader.uploadStoredFiles();
            }
        });
    }

    $(".requerido").off("focusin", hideToolTips);

    $(focusElement).focus();

    $(".requerido").on("focusin", hideToolTips);
});