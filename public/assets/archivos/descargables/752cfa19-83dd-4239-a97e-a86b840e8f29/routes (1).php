<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['casos-exito'] = 'home/casosExito';
$route['financiamiento'] = 'home/financiamiento';
$route['blog'] = 'home/blog';
$route['blog/page/(:any)'] = 'home/blogPage/$1';
$route['blog/(:any)/(:any)'] = 'home/blog/$2';
$route['contacto'] = 'home/contacto';
$route['descargables'] = 'home/descargables';
$route['beneficios'] = 'home/index/1';

$route['administracion/blog'] = "blog";
$route['administracion/blog/eliminar/(:any)'] = "blog/eliminarEntrada/$1";

$route['administracion/blog/crear-entrada'] = "blog/crear";
$route['administracion/blog/crear-entrada/formInfoEntrada'] = "blog/crearEntrada";
$route['administracion/blog/crear-entrada/imagenesEntrada'] = "blog/sinImagenes";
$route['administracion/blog/crear-entrada/subirImagen'] = "blog/subirImagen";
$route['administracion/blog/crear-entrada/subirImagen/(:any)'] = "blog/subirImagen";

$route['administracion/blog/(:any)'] = "blog/modificar/$1";
$route['administracion/blog/(:any)/formInfoEntrada'] = "blog/modificarEntrada/$1";
$route['administracion/blog/(:any)/imagenesEntrada'] = "blog/imagenesEntrada/$1";
$route['administracion/blog/(:any)/subirImagen'] = "blog/subirImagen";
$route['administracion/blog/(:any)/subirImagen/(:any)'] = "blog/subirImagen";

$route['administracion/casosExito'] = "CasosExito";
$route['administracion/casosExito/eliminar/(:any)'] = "casosExito/eliminar/$1";

$route['administracion/casosExito/crear'] = "casosExito/crear";
$route['administracion/casosExito/crear/form'] = "casosExito/crearCaso";
$route['administracion/casosExito/crear/imagenesEntrada'] = "casosExito/sinImagenes";
$route['administracion/casosExito/crear/subirImagen'] = "casosExito/subirImagen";
$route['administracion/casosExito/crear/subirImagen/(:any)'] = "casosExito/subirImagen";

$route['administracion/casosExito/(:any)'] = "casosExito/modificar/$1";
$route['administracion/casosExito/(:any)/form'] = "casosExito/modificarCaso/$1";
$route['administracion/casosExito/(:any)/imagenesEntrada'] = "casosExito/imagenesEntrada/$1";
$route['administracion/casosExito/(:any)/subirImagen'] = "casosExito/subirImagen";
$route['administracion/casosExito/(:any)/subirImagen/(:any)'] = "casosExito/subirImagen";


$route['administracion/descargables'] = "descargables";
$route['administracion/descargables/eliminar/(:any)'] = "descargables/eliminar/$1";

$route['administracion/descargables/crear'] = "descargables/crear";
$route['administracion/descargables/crear/form'] = "descargables/crearVista";
$route['administracion/descargables/crear/imagenesEntrada'] = "descargables/sinImagenes";
$route['administracion/descargables/crear/subirImagen'] = "descargables/subirImagen";
$route['administracion/descargables/crear/subirImagen/(:any)'] = "descargables/subirImagen";

$route['administracion/descargables/(:any)'] = "descargables/modificar/$1";
$route['administracion/descargables/(:any)/form'] = "descargables/modificarVista/$1";
$route['administracion/descargables/(:any)/imagenesEntrada'] = "descargables/imagenesEntrada/$1";
$route['administracion/descargables/(:any)/subirImagen'] = "descargables/subirImagen";
$route['administracion/descargables/(:any)/subirImagen/(:any)'] = "descargables/subirImagen";

/* Administrador */
// $route['administracion'] = 'administracion/usuarios/login';
// $route['administracion/login'] = 'administracion/usuarios/login';
// $route['administracion/recuperar'] = 'administracion/usuarios/recuperar';
// $route['administracion/verificar/(:any)/(:any)'] = 'administracion/usuarios/verficacion_token';



/* Site */
