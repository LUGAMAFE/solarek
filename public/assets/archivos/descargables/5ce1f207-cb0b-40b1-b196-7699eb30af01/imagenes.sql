-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 02-08-2019 a las 01:01:26
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `solarek`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

DROP TABLE IF EXISTS `imagenes`;
CREATE TABLE IF NOT EXISTS `imagenes` (
  `id_img` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `folder_img` varchar(300) NOT NULL,
  `uuid_img` varchar(100) NOT NULL,
  `name_img` varchar(150) NOT NULL,
  `ext_img` varchar(10) NOT NULL,
  `dir_img` varchar(200) NOT NULL,
  `full_route_img` varchar(400) NOT NULL,
  `fecha_creacion_img` datetime NOT NULL,
  `fecha_modificacion_img` datetime NOT NULL,
  PRIMARY KEY (`id_img`),
  UNIQUE KEY `uuid_img_unique` (`uuid_img`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `imagenes`
--

INSERT INTO `imagenes` (`id_img`, `folder_img`, `uuid_img`, `name_img`, `ext_img`, `dir_img`, `full_route_img`, `fecha_creacion_img`, `fecha_modificacion_img`) VALUES
(1, 'assets/img/entradas/', 'c9038edb-f143-4813-ac32-9af947119751', 'Banner1 small', 'png', 'assets/img/entradas//c9038edb-f143-4813-ac32-9af947119751/', 'assets/img/entradas//c9038edb-f143-4813-ac32-9af947119751/Banner1 small.png', '2019-07-25 18:09:06', '2019-07-25 18:09:06'),
(2, 'assets/img/entradas/', '652159a7-fcb6-424d-8b7c-b06d8b0ef63a', 'Banner1', 'png', 'assets/img/entradas//652159a7-fcb6-424d-8b7c-b06d8b0ef63a/', 'assets/img/entradas//652159a7-fcb6-424d-8b7c-b06d8b0ef63a/Banner1.png', '2019-07-25 18:09:06', '2019-07-25 18:09:06'),
(3, 'assets/img/entradas/', '2cbf46b5-4e18-4745-9d4b-da2b1329ef84', 'Banner2 small', 'png', 'assets/img/entradas//2cbf46b5-4e18-4745-9d4b-da2b1329ef84/', 'assets/img/entradas//2cbf46b5-4e18-4745-9d4b-da2b1329ef84/Banner2 small.png', '2019-07-25 18:11:20', '2019-07-25 18:11:20'),
(4, 'assets/img/entradas/', 'd3762fc5-09b9-47c6-930b-5594b8886d85', 'Banner2', 'png', 'assets/img/entradas//d3762fc5-09b9-47c6-930b-5594b8886d85/', 'assets/img/entradas//d3762fc5-09b9-47c6-930b-5594b8886d85/Banner2.png', '2019-07-25 18:11:20', '2019-07-25 18:11:20');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
