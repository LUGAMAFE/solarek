<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	protected $class = '';
	protected $folder = '/site';
	protected $folder_set = '/site/partials/';
	protected $object;
	protected $objectCliente;
	protected $objectD;
	protected $objectpedido;
	protected $url_recuperar;
	protected $principal = 1;
	protected $objectCat;
	protected $objectSubCat;

	protected $entradasPorPaginaBlog= 4;

	public function __construct(){
		parent::__construct();
		$this->class = strtolower(get_class());
	}


	public function index($datito = null){
		$datos['class'] = $this->class;
		$datos['banner'] = 1;
		$datos['active'] = 'inicio';
		$datos['datito'] = $datito;
		$this->template->write('title', 'Bienvenido');
		$this->template->asset_js('slick-1.8.0/slick/slick.min.js');
		$this->template->asset_js('numscroller/numscroller-1.0.js');
		$this->template->asset_js('tilt.js-master/src/tilt.jquery.js');
		$this->template->asset_js('tilter.js');
		
		$this->template->asset_js('slider.js');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('banner', $this->folder_set.'banner', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/home/list', $datos);
		$this->template->render();
	}

	public function casosExito(){
		$datos['class'] = $this->class;
		$datos['banner'] = 1;
		$datos['active'] = 'casos-exito';
		$this->template->write('title', 'Casos Exito');
		$this->template->asset_js('slick-1.8.0/slick/slick.min.js');
		$this->template->asset_js('numscroller/numscroller-1.0.js');
		$this->template->asset_css('fancybox/dist/jquery.fancybox.min.css');
		$this->template->asset_js('fancybox/dist/jquery.fancybox.min.js');
		$this->template->asset_js('fancyConf.js');
		$this->template->asset_js('tilt.js-master/src/tilt.jquery.js');
		$this->template->asset_js('tilter.js');
		
		$this->load->model("administracion/Casos_Model");
		$datos['casos'] = $this->Casos_Model->getCasosExito();
		
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('banner', $this->folder_set.'banner', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/casos_Exito/list', $datos);
		$this->template->render();
	}

	public function financiamiento(){
		$datos['class'] = $this->class;
		$datos['banner'] = 1;
		$datos['active'] = 'credito';
		$this->template->write('title', 'Financiamiento');
		$this->template->asset_js('tilt.js-master/src/tilt.jquery.js');
		$this->template->asset_js('tilter.js');
		
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('banner', $this->folder_set.'banner', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/financiamiento/list', $datos);
		$this->template->render();
	}

	public function contacto(){
		$datos['class'] = $this->class;
		$datos['banner'] = 1;
		$datos['active'] = 'contacto';
		$this->template->write('title', 'Contacto');
		$this->template->asset_js('tilt.js-master/src/tilt.jquery.js');
		$this->template->asset_js('tilter.js');
		$this->template->asset_css('jquerymodal/jquery.modal.min.css');
		$this->template->asset_js('jquerymodal/jquery.modal.min.js');
		$this->template->asset_js('modalContacto.js');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('content', $this->folder.'/contacto/list', $datos);
		$this->template->render();
	}

	public function blog($entrada = null){
		$this->load->model("administracion/Entradas_Model");
		$this->load->model("administracion/Imagenes_Model");
		$this->template->asset_js('jquery.bootpag.min.js');
		$this->template->asset_js('paginador.js');
		$datos['class'] = $this->class;
		$datos['banner'] = 1;
		$datos['active'] = 'blog';
		$this->template->write('title', 'Blog');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		if(is_null($entrada)){
			$datos['entradas'] = $this->Entradas_Model->getEntradasBlogLimit(0, $this->entradasPorPaginaBlog);
			$datos['entradasRec'] = $this->Entradas_Model->getEntradasBlogLimitRand(0, 8);
			$datos['totalPaginas'] = ceil($this->Entradas_Model->countEntradas() / $this->entradasPorPaginaBlog);
			$this->template->write_view('content', $this->folder.'/blog/list', $datos);
		}else{
			$datos['entradas'] = $this->Entradas_Model->getEntradasBlogLimitRand(0, 8);
			$datos['entradaBlog'] = $this->Entradas_Model->getEntrada($entrada);
			$datos['description'] = $datos["entradaBlog"]["titulo_entrada"];
			$datos['imagePrev'] = site_url($datos["entradaBlog"]["big_full_route_img"]);
			$this->template->write_view('content', $this->folder.'/blog/entrada', $datos);
		}
		$this->template->render();
	}

	public function descargables(){
		$datos['class'] = $this->class;
		$datos['banner'] = 1;
		$datos['active'] = 'descargables';
		$this->template->write('title', 'Descargables');
		$this->template->asset_css('jquerymodal/jquery.modal.min.css');
		$this->template->asset_js('jquerymodal/jquery.modal.min.js');
		$this->template->asset_js('modalDescargas.js');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('content', $this->folder.'/descargables/list', $datos);
		$this->template->render();
	}

	public function blogPage($page){
		$this->load->model("administracion/Entradas_Model");
		$totalEntradas = $this->Entradas_Model->countEntradas();
		$numeroPaginas = ceil($totalEntradas/$this->entradasPorPaginaBlog);
		$limite = $page * $this->entradasPorPaginaBlog;
		$offset = $limite - $this->entradasPorPaginaBlog;
		$entradas = $this->Entradas_Model->getEntradasBlogLimit($offset, $this->entradasPorPaginaBlog);
		for ($i=0; $i < count($entradas); $i++) { 
			$entradas[$i]["ref_entrada"]  =  site_url('blog/'.create_slug($entradas[$i]["titulo_entrada"])."/".$entradas[$i]["id_entrada"]);
		}
		$output = ["error" => false, "mensaje" => $entradas];
        $json = json_encode($output);
        echo $json;
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
